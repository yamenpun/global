<?php

namespace App\HelperClass;

class Country {

    protected static $countries = [

        "Australia"   => "au",
        "Bangladesh"  => "bd",
        "India"       => "in",
        "Japan"       => "jp",
        "New Zealand" => "nz",
        "UK"          => "gb",
        "USA"         => "us",
    ];

    public static function all()
    {
        return static::$countries;
    }
}