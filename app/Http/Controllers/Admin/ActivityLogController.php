<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Log\AddFormValidation;
use App\Models\ActivityLog;
use Auth;
use Gate;
use DB;
use AppHelper;
use App\Http\Requests;


class ActivityLogController extends AdminBaseController {

    protected $view_path  = 'admin.log';
    protected $base_route = 'admin.log';
    protected $model;

    public function index()
    {
        $user = Auth::user();
        $data = [];
        $data['rows'] = DB::table('activity_log')
            ->select('activity_log.*', 'users.fullname')
            ->join('users', 'activity_log.user_id', '=', 'users.id')
            ->join('branch', 'users.branch_id', '=', 'branch.id')
            ->where('branch.id', '=', $user->branch_id )
            ->where('users.id', '=', $user->id)
            ->orderBy('activity_log.id', 'DESC')
            ->get();

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function deletePermanent($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route.'.list');
        }

        ActivityLog::destroy($id);

        AppHelper::flash('success', 'Record permanently deleted from database.');

        return redirect()->route($this->base_route.'.list');
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = ActivityLog::find($id);

        return $this->model;
    }
}
