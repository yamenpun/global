<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Gate;
use DB;
use App\Models\Branch;
use PDF;
use Carbon;
use Excel;
use AppHelper;
use App\Http\Requests;
use App\Http\Requests\Admin\Branch\AddFormValidation;
use App\Http\Requests\Admin\Branch\UpdateFormValidation;


class AdminBranchController extends AdminBaseController {

    protected $view_path  = 'admin.branch';
    protected $base_route = 'admin.branch';
    protected $model;
    
    public function index()
    {
        $data = [];
        $data['rows'] = DB::select( DB::raw(" SELECT * FROM branch WHERE is_deleted = '0' ORDER BY id ASC "));

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function isDeletedList()
    {
        $data = [];
        $data['rows'] = DB::select( DB::raw(" SELECT * FROM branch WHERE is_deleted = '1' ORDER BY id ASC "));

        return view(parent::loadDefaultVars($this->view_path . '.deletedBranchList'), compact('data'));
    }

    public function exportAsPrint()
    {
        $data = [];
        $data['rows'] = Branch::select('id', 'branch_name', 'branch_address', 'branch_phone', 'branch_email')->where('is_deleted', '0')->get();

        return view(parent::loadDefaultVars($this->view_path . '.branchPrint'), compact('data'));
    }

    public function exportAsPdf()
    {
        $data = [];
        $data['rows'] = Branch::select('id', 'branch_name', 'branch_address', 'branch_phone', 'branch_email')->where('is_deleted', '0')->get();

        $pdf = PDF::loadView($this->view_path. '.branchPdf', compact('data'));

        return $pdf->download('branchPDF.pdf');
    }

    public function exportAsExcel()
    {
        $branches = Branch::select('id', 'branch_name', 'branch_address', 'branch_phone', 'branch_email', 'created_at')->where('is_deleted', '0')->get();

        $branchArray = [];

        // Define the Excel spreadsheet headers
        $branchArray[] = ['S.N.', 'Branch Name', 'Branch Address', 'Branch Phone', 'Branch Email', 'Created Date'];

        // Convert each member of the returned collection into an array,
        // and append it to the branch array.
        foreach ($branches as $branch) {
            $branchArray[] = $branch->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('Branch Information', function($excel) use ($branchArray) {

            // Build the spreadsheet, passing in the branch array
            $excel->sheet('sheet1', function($sheet) use ($branchArray) {
                $sheet->fromArray($branchArray, null, 'A1', false, false);
            });

        })->export('xls');
    }

    public function deletedExportAsPrint()
    {
        $data = [];
        $data['rows'] = Branch::select('id', 'branch_name', 'branch_address', 'branch_phone', 'branch_email')->where('is_deleted', '1')->get();

        return view(parent::loadDefaultVars($this->view_path . '.deletedBranchPrint'), compact('data'));
    }

    public function deletedExportAsPdf()
    {
        $data = [];
        $data['rows'] = Branch::select('id', 'branch_name', 'branch_address', 'branch_phone', 'branch_email')->where('is_deleted', '1')->get();

        $pdf = PDF::loadView($this->view_path. '.deletedBranchPdf', compact('data'));

        return $pdf->download('deletedBranchPDF.pdf');
    }

    public function deletedExportAsExcel()
    {
        $branches = Branch::select('id', 'branch_name', 'branch_address', 'branch_phone', 'branch_email', 'created_at')->where('is_deleted', '1')->get();

        $branchArray = [];

        // Define the Excel spreadsheet headers
        $branchArray[] = ['S.N.', 'Branch Name', 'Branch Address', 'Branch Phone', 'Branch Email', 'Created Date'];

        // Convert each member of the returned collection into an array,
        // and append it to the branch array.
        foreach ($branches as $branch) {
            $branchArray[] = $branch->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('Deleted Branch Information', function($excel) use ($branchArray) {

            // Build the spreadsheet, passing in the branch array
            $excel->sheet('sheet1', function($sheet) use ($branchArray) {
                $sheet->fromArray($branchArray, null, 'A1', false, false);
            });

        })->export('xls');
    }

    public function create()
    {
        return view(parent::loadDefaultVars($this->view_path . '.create'));
    }

    public function store(AddFormValidation $request)
    {
        Branch::create([
            'branch_name'           => $request->get('branch_name'),
            'branch_address'        => $request->get('branch_address'),
            'branch_phone'          => $request->get('branch_phone'),
            'branch_email'          => $request->get('branch_email'),
            'created_at'            => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'            => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        AppHelper::flash('success', 'Record created Successfully.');

        return redirect()->route($this->base_route.'.list');
    }

    public function edit($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['row'] = $this->model;

        return view(parent::loadDefaultVars($this->view_path . '.edit'), compact('data'));
    }

    public function update(UpdateFormValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        $user = Auth::user();
        $data = $this->model;

        $data->update([
            'branch_name'           => $request->get('branch_name'),
            'branch_address'        => $request->get('branch_address'),
            'branch_phone'          => $request->get('branch_phone'),
            'branch_email'          => $request->get('branch_email'),
            'updated_at'            => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        $this->editLog($user->id, $request->get('branch_name'), 'Branch');

        AppHelper::flash('success', 'Record updated successfully.');

        return redirect()->route($this->base_route.'.list');
    }

    public function destroy($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route.'.list');
        }

        $user = Auth::user();

        $branch = Branch::where('id', '=', $id)->first();

        DB::table('branch')
            ->where('id', $id)
            ->update(['is_deleted' => 1]);

        $this->deleteLog($user->id, $branch->branch_name, 'Branch');

        AppHelper::flash('success', 'Record deleted successfully.');

        return redirect()->route($this->base_route.'.list');
    }

    public function restore($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route.'.deletedList');
        }

        DB::table('branch')
            ->where('id', $id)
            ->update(['is_deleted' => 0]);

        AppHelper::flash('success', 'Record restored successfully.');

        return redirect()->route($this->base_route.'.deletedList');
    }

    public function deletePermanent($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route.'.deletedList');
        }

        $branch = Branch::where('id', '=', $id)->first();

        Branch::destroy($id);

        $user = Auth::user();

        $this->deletePermanentLog($user->id, $branch->branch_name, 'Branch');

        AppHelper::flash('success', 'Record permanently deleted from database.');

        return redirect()->route($this->base_route.'.deletedList');
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = Branch::find($id);

        return $this->model;
    }
}
