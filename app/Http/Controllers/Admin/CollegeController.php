<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Gate;
use DB;
use App\Models\College;
use PDF;
use Carbon;
use Excel;
use AppHelper;
use App\Http\Requests;
use App\Http\Requests\Admin\College\AddFormValidation;
use App\Http\Requests\Admin\College\UpdateFormValidation;


class CollegeController extends AdminBaseController {

    protected $view_path  = 'admin.college';
    protected $base_route = 'admin.college';
    protected $model;
    
    public function index()
    {
        $data = [];
        $data['rows'] = DB::select( DB::raw(" SELECT * FROM college WHERE is_deleted = '0' ORDER BY id ASC "));

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function isDeletedList()
    {
        $data = [];
        $data['rows'] = DB::select( DB::raw(" SELECT * FROM college WHERE is_deleted = '1' ORDER BY id ASC "));

        return view(parent::loadDefaultVars($this->view_path . '.deletedCollegeList'), compact('data'));
    }

    public function exportAsPrint()
    {
        $data = [];
        $data['rows'] = College::select('id', 'college_name', 'college_address', 'college_phone', 'college_country', 'remarks')->where('is_deleted', '0')->get();

        return view(parent::loadDefaultVars($this->view_path . '.collegePrint'), compact('data'));
    }

    public function exportAsPdf()
    {
        $data = [];
        $data['rows'] = College::select('id', 'college_name', 'college_address', 'college_phone', 'college_country', 'remarks')->where('is_deleted', '0')->get();

        $pdf = PDF::loadView($this->view_path. '.collegePdf', compact('data'));

        return $pdf->download('CollegePDF.pdf');
    }

    public function exportAsExcel()
    {
        $colleges = College::select('id', 'college_name', 'college_address', 'college_phone', 'college_country', 'remarks', 'created_at')->where('is_deleted', '0')->get();

        $collegeArray = [];

        // Define the Excel spreadsheet headers
        $collegeArray[] = ['S.N.', 'College Name', 'College Address', 'College Phone', 'College Country', 'Remarks', 'Created Date'];

        // Convert each member of the returned collection into an array,
        // and append it to the college array.
        foreach ($colleges as $college) {
            $collegeArray[] = $college->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('College Information', function($excel) use ($collegeArray) {

            // Build the spreadsheet, passing in the college array
            $excel->sheet('sheet1', function($sheet) use ($collegeArray) {
                $sheet->fromArray($collegeArray, null, 'A1', false, false);
            });

        })->export('xls');
    }

    public function deletedExportAsPrint()
    {
        $data = [];
        $data['rows'] = College::select('id', 'college_name', 'college_address', 'college_phone', 'college_country', 'remarks')->where('is_deleted', '1')->get();

        return view(parent::loadDefaultVars($this->view_path . '.deletedCollegePrint'), compact('data'));
    }

    public function deletedExportAsPdf()
    {
        $data = [];
        $data['rows'] = College::select('id', 'college_name', 'college_address', 'college_phone', 'college_country', 'remarks')->where('is_deleted', '1')->get();

        $pdf = PDF::loadView($this->view_path. '.deletedCollegePdf', compact('data'));

        return $pdf->download('deletedCollegePDF.pdf');
    }

    public function deletedExportAsExcel()
    {
        $colleges = College::select('id', 'college_name', 'college_address', 'college_phone', 'college_country', 'remarks', 'created_at')->where('is_deleted', '1')->get();

        $collegeArray = [];

        // Define the Excel spreadsheet headers
        $collegeArray[] = ['S.N.', 'College Name', 'College Address', 'College Phone', 'College Country', 'Remarks', 'Created Date'];

        // Convert each member of the returned collection into an array,
        // and append it to the college array.
        foreach ($colleges as $college) {
            $collegeArray[] = $college->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('Deleted Branch Information', function($excel) use ($collegeArray) {

            // Build the spreadsheet, passing in the college array
            $excel->sheet('sheet1', function($sheet) use ($collegeArray) {
                $sheet->fromArray($collegeArray, null, 'A1', false, false);
            });

        })->export('xls');
    }

    public function create()
    {
        return view(parent::loadDefaultVars($this->view_path . '.create'));
    }

    public function store(AddFormValidation $request)
    {
        College::create([
            'college_name'          => $request->get('college_name'),
            'college_address'       => $request->get('college_address'),
            'college_phone'         => $request->get('college_phone'),
            'college_country'       => $request->get('college_country'),
            'remarks'               => $request->get('remarks'),
            'created_at'            => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'            => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        AppHelper::flash('success', 'Record created Successfully.');

        return redirect()->route($this->base_route.'.list');
    }

    public function edit($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['row'] = $this->model;

        return view(parent::loadDefaultVars($this->view_path . '.edit'), compact('data'));
    }

    public function update(UpdateFormValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        $user = Auth::user();
        $data = $this->model;

        $data->update([
            'college_name'          => $request->get('college_name'),
            'college_address'       => $request->get('college_address'),
            'college_phone'         => $request->get('college_phone'),
            'college_country'       => $request->get('college_country'),
            'remarks'               => $request->get('remarks'),
            'updated_at'            => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        $this->editLog($user->id, $request->get('college_name'), 'College');

        AppHelper::flash('success', 'Record updated successfully.');

        return redirect()->route($this->base_route.'.list');
    }

    public function destroy($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route.'.list');
        }

        $user = Auth::user();

        $college = College::where('id', '=', $id)->first();

        DB::table('college')
            ->where('id', $id)
            ->update(['is_deleted' => 1]);

        $this->deleteLog($user->id, $college->college_name, 'College');

        AppHelper::flash('success', 'Record deleted successfully.');

        return redirect()->route($this->base_route.'.list');
    }

    public function restore($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route.'.deletedList');
        }

        DB::table('college')
            ->where('id', $id)
            ->update(['is_deleted' => 0]);

        AppHelper::flash('success', 'Record restored successfully.');

        return redirect()->route($this->base_route.'.deletedList');
    }

    public function deletePermanent($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route.'.deletedList');
        }

        $college = College::where('id', '=', $id)->first();

        College::destroy($id);

        $user = Auth::user();

        $this->deletePermanentLog($user->id, $college->college_name, 'College');

        AppHelper::flash('success', 'Record permanently deleted from database.');

        return redirect()->route($this->base_route.'.deletedList');
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = College::find($id);

        return $this->model;
    }
}
