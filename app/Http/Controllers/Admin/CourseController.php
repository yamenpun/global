<?php

namespace App\Http\Controllers\Admin;

use App\Models\Course;
use Auth;
use Gate;
use DB;
use PDF;
use Carbon;
use Excel;
use AppHelper;
use App\Http\Requests;
use App\Http\Requests\Admin\Course\AddFormValidation;
use App\Http\Requests\Admin\Course\UpdateFormValidation;


class CourseController extends AdminBaseController {

    protected $view_path  = 'admin.course';
    protected $base_route = 'admin.course';
    protected $model;
    
    public function index()
    {
        $data = [];
        $data['rows'] = DB::select( DB::raw(" 
                        SELECT * FROM course WHERE course.is_deleted = '0' ORDER BY id ASC "));

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function isDeletedList()
    {
        $data = [];
        $data['rows'] = DB::select( DB::raw(" 
                        SELECT * FROM course WHERE course.is_deleted = '1' ORDER BY id ASC "));

        return view(parent::loadDefaultVars($this->view_path . '.deletedCourseList'), compact('data'));
    }

    public function exportAsPrint()
    {
        $data = [];
        $data['rows'] = DB::table('course')
            ->select('course.*')
            ->where('course.is_deleted', '=', '0')
            ->get();

        return view(parent::loadDefaultVars($this->view_path . '.coursePrint'), compact('data'));
    }

    public function exportAsPdf()
    {
        $data = [];
        $data['rows'] = DB::table('course')
            ->select('course.*')
            ->where('course.is_deleted', '=', '0')
            ->get();

        $pdf = PDF::loadView($this->view_path. '.coursePdf', compact('data'));

        return $pdf->download('CoursePDF.pdf');
    }

    public function exportAsExcel()
    {
        $courses = Course::select('course.id', 'course.course_name', 'course.course_remarks', 'course.created_at')
            ->where('course.is_deleted', '1')
            ->orderBy('course.id', 'ASC')
            ->get();


        $courseArray = [];

        // Define the Excel spreadsheet headers
        $courseArray[] = ['S.N.', 'Course Name', 'Course Remarks', 'Created At'];

        // Convert each member of the returned collection into an array,
        // and append it to the course array.
        foreach ($courses as $course) {
            $courseArray[] = $course->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('Deleted Course Information', function($excel) use ($courseArray) {

            // Build the spreadsheet, passing in the course array
            $excel->sheet('sheet1', function($sheet) use ($courseArray) {
                $sheet->fromArray($courseArray, null, 'A1', false, false);
            });

        })->export('xls');
    }

    public function deletedExportAsPrint()
    {
        $data = [];
        $data['rows'] = DB::table('course')
            ->select('course.*')
            ->where('course.is_deleted', '=', '1')
            ->get();

        return view(parent::loadDefaultVars($this->view_path . '.deletedCoursePrint'), compact('data'));
    }

    public function deletedExportAsPdf()
    {
        $data = [];
        $data['rows'] = DB::table('course')
            ->select('course.*')
            ->where('course.is_deleted', '=', '1')
            ->get();

        $pdf = PDF::loadView($this->view_path. '.deletedCoursePdf', compact('data'));

        return $pdf->download('deletedCoursePDF.pdf');
    }

    public function deletedExportAsExcel()
    {
        $courses = Course::select('course.id', 'course.course_name', 'course.course_remarks', 'course.created_at')
            ->where('course.is_deleted', '1')
            ->orderBy('course.id', 'ASC')
            ->get();


        $courseArray = [];

        // Define the Excel spreadsheet headers
        $courseArray[] = ['S.N.', 'Course Name', 'Course Remarks', 'Created At'];

        // Convert each member of the returned collection into an array,
        // and append it to the course array.
        foreach ($courses as $course) {
            $courseArray[] = $course->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('Deleted Course Information', function($excel) use ($courseArray) {

            // Build the spreadsheet, passing in the course array
            $excel->sheet('sheet1', function($sheet) use ($courseArray) {
                $sheet->fromArray($courseArray, null, 'A1', false, false);
            });

        })->export('xls');
    }

    public function create()
    {
        return view(parent::loadDefaultVars($this->view_path . '.create'));
    }

    public function store(AddFormValidation $request)
    {
        Course::create([
            'course_name'           => $request->get('course_name'),
            'course_remarks'        => $request->get('course_remarks'),
            'created_at'            => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'            => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        AppHelper::flash('success', 'Record created Successfully.');

        return redirect()->route($this->base_route.'.list');
    }

    public function edit($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];

        $data['row'] = $this->model;

        return view(parent::loadDefaultVars($this->view_path . '.edit'), compact('data'));
    }

    public function update(UpdateFormValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        $user = Auth::user();
        $data = $this->model;

        $data->update([
            'course_name'           => $request->get('course_name'),
            'course_remarks'        => $request->get('course_remarks'),
            'updated_at'            => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        $this->editLog($user->id, $request->get('course_name'), 'Course');

        AppHelper::flash('success', 'Record updated successfully.');

        return redirect()->route($this->base_route.'.list');
    }

    public function destroy($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route.'.list');
        }

        $user = Auth::user();

        $course = Course::where('id', '=', $id)->first();

        DB::table('course')
            ->where('id', $id)
            ->update(['is_deleted' => 1]);

        $this->deleteLog($user->id, $course->college_name, 'Course');

        AppHelper::flash('success', 'Record deleted successfully.');

        return redirect()->route($this->base_route.'.list');
    }

    public function restore($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route.'.deletedList');
        }

        DB::table('course')
            ->where('id', $id)
            ->update(['is_deleted' => 0]);

        AppHelper::flash('success', 'Record restored successfully.');

        return redirect()->route($this->base_route.'.deletedList');
    }

    public function deletePermanent($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route.'.deletedList');
        }

        $course = Course::where('id', '=', $id)->first();

        Course::destroy($id);

        $user = Auth::user();

        $this->deletePermanentLog($user->id, $course->course_name, 'Course');

        AppHelper::flash('success', 'Record permanently deleted from database.');

        return redirect()->route($this->base_route.'.deletedList');
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = Course::find($id);

        return $this->model;
    }
}
