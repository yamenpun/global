<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;

class DashboardController extends AdminBaseController
{
	protected $view_path = 'admin.dashboard';
    protected $base_route = 'admin.dashboard';

    public function index()
    {
        return view(parent::loadDefaultVars($this->view_path . '.index'));
    }
}
