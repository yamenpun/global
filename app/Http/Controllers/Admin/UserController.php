<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Gate;
use App\User;
use AppHelper;
use App\Http\Requests;
use App\Http\Requests\Admin\User\AddFormValidation;
use App\Http\Requests\Admin\User\UpdateFormValidation;


class UserController extends AdminBaseController {

    protected $view_path = 'admin.user';
    protected $base_route = 'admin.user';
    protected $model;

    public function index()
    {
        $user = Auth::user();
        $data = [];
        $data['rows'] = User::select('id', 'fullname', 'username', 'email', 'role', 'created_at', 'updated_at', 'status')
                        ->where('role', '!=', 'superadmin')
                        ->where('branch_id', $user->branch_id)->get();

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function view($id)
    {
        $data = [];
        $data['row'] = User::find($id);

        return view(parent::loadDefaultVars($this->view_path . '.view'), compact('data'));
    }

    public function create()
    {
        return view(parent::loadDefaultVars($this->view_path . '.create'));
    }

    public function store(AddFormValidation $request)
    {
        $user = Auth::user();

        User::create([
            'fullname'    => $request->get('fullname'),
            'username'    => $request->get('username'),
            'email'       => $request->get('email'),
            'password'    => bcrypt($request->get('password')),
            'role'        => $request->get('role'),
            'status'      => $request->get('status'),
            'branch_id'   => $user->branch_id,
            'created_id'  => $user->id,
        ]);

        AppHelper::flash('success', 'Record created Successfully.');

        return redirect()->route($this->base_route);
    }

    public function edit($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['row'] = $this->model;

        return view(parent::loadDefaultVars($this->view_path . '.edit'), compact('data'));
    }

    public function update(UpdateFormValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }

        $user = Auth::user();

        $data = $this->model;

        if ($request->get('password') === '')
        {
            $data->update([
                'fullname'    => $request->get('fullname'),
                'username'    => $request->get('username'),
                'email'       => $request->get('email'),
                'password'    => $data->password,
                'role'        => $request->get('role'),
                'status'      => $request->get('status'),
                'branch_id'   => $user->branch_id,
                'created_id'  => $user->id,
            ]);
        } else
        {
            $data->update([
                'fullname'    => $request->get('fullname'),
                'username'    => $request->get('username'),
                'email'       => $request->get('email'),
                'password'    => bcrypt($request->get('password')),
                'role'        => $request->get('role'),
                'status'      => $request->get('status'),
                'branch_id'   => $user->branch_id,
                'created_id'  => $user->id,
            ]);
        }

        $this->editLog($user->id, $request->get('fullname'), 'User');

        AppHelper::flash('success', 'Record updated successfully.');

        return redirect()->route($this->base_route);
    }

    public function destroy($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route);
        }

        $user_name = User::where('id', '=', $id)->first();

        $user = Auth::user();

        User::destroy($id);

        $this->deletePermanentLog($user->id, $user_name->fullname, 'User');

        AppHelper::flash('success', 'Record deleted successfully.');

        return redirect()->route($this->base_route);
    }

    public function profile()
    {
        $data = [];
        $data['rows'] = Auth::user();

        return view(parent::loadDefaultVars($this->view_path . '.profile'), compact('data'));
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = User::find($id);

        return $this->model;
    }
}
