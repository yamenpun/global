<?php
namespace App\Http\Controllers;

use App\Models\ActivityLog;
use DB;


class AppBaseController extends Controller
{
    public function __construct()
    {
        if (app()->environment() !== 'production')
            DB::enableQueryLog();
    }

    public function makeViewFolderPath($view_path)
    {
        $tmp = explode('.', $view_path);
        array_pop($tmp);
        
        return implode('/', $tmp).'/';
    }

    public function editLog($user, $record_name, $table_name)
    {
        ActivityLog::create([
            'user_id'       => $user,
            'action'        => 'Edit',
            'description'   => 'Record '.$record_name.' was updated in '.$table_name.' table.',
        ]);
    }

    public function deleteLog($user, $record_name, $table_name)
    {
        ActivityLog::create([
            'user_id'       => $user,
            'action'        => 'Delete',
            'description'   => 'Record '.$record_name.' was deleted in '. $table_name. ' table.',
        ]);
    }

    public function deletePermanentLog($user, $record_name, $table_name)
    {
        ActivityLog::create([
            'user_id'       => $user,
            'action'        => 'Delete Permanent',
            'description'   => 'Record '.$record_name.' was permanently deleted in '.$table_name.' table.',
        ]);
    }

}