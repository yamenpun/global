<?php

namespace App\Http\Controllers\Counselor;

use App\Http\Requests;

class DashboardController extends CounselorBaseController
{
	protected $view_path = 'counselor.dashboard';
    protected $base_route = 'counselor.dashboard';

    public function index()
    {
        return view(parent::loadDefaultVars($this->view_path . '.index'));
    }
}
