<?php

namespace App\Http\Controllers\Counselor;

use App\Models\Branch;
use App\Models\College;
use Auth;
use Gate;
use DB;
use App\Models\StudentEnquiry;
use PDF;
use Carbon;
use Excel;
use AppHelper;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\Enquiry\AddCounselorFeedbackValidation;
use App\Http\Requests\Admin\Enquiry\UpdateCounselorFeedbackValidation;


class EnquiryController extends CounselorBaseController {

    protected $view_path  = 'counselor.enquiry';
    protected $base_route = 'counselor.enquiry';
    protected $model;
    
    public function index(Request $requests)
    {
        $user = Auth::user();

        $data = [];

        $data['college'] = College::select('id', 'college_country')->groupBy('college_country')->get();

        $country_name = $requests->input('country');
        $year = $requests->input('year');
        $month = $requests->input('month');

        if (($country_name === 'all' && $year === 'all' && $month === 'all') || ($country_name === null && $year === null && $month == null)){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND b.id = $user->branch_id ORDER BY id ASC "));

        }elseif ($country_name !== 'all' && $year === 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND s.preferred_place = '$country_name' AND b.id = $user->branch_id ORDER BY id ASC "));

        }elseif ($country_name !== 'all' && $year !== 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND s.preferred_place = '$country_name' 
                          AND Year(s.enquiry_date) = '$year' AND b.id = $user->branch_id ORDER BY id ASC "));

        }elseif($country_name !== 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND s.preferred_place = '$country_name' 
                          AND Year(s.enquiry_date) = '$year' AND Month(s.enquiry_date) = '$month' AND b.id = $user->branch_id ORDER BY id ASC "));

        }elseif($country_name === 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND Month(s.enquiry_date) = '$month' AND b.id = $user->branch_id ORDER BY id ASC "));

        }elseif($country_name === 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND Year(s.enquiry_date) = '$year' 
                          AND Month(s.enquiry_date) = '$month' AND b.id = $user->branch_id ORDER BY id ASC "));

        }elseif($country_name !== 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND s.preferred_place = '$country_name' 
                          AND Month(s.enquiry_date) = '$month' AND b.id = $user->branch_id ORDER BY id ASC "));

        }elseif($country_name === 'all' && $year !== 'all' && $month === 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND Year(s.enquiry_date) = '$year' AND b.id = $user->branch_id ORDER BY id ASC "));

        }

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }


    public function view($id)
    {
        $data = [];

        $data['row'] = DB::select( DB::raw(" SELECT s.*, b.branch_name, u.fullname FROM student_enquiry AS s 
                                            INNER JOIN branch AS b ON s.branch_id = b.id
                                            INNER JOIN users AS u ON s.user_id = u.id WHERE s.id = '$id' " ));

        return view(parent::loadDefaultVars($this->view_path . '.view'), compact('data'));
    }

    public function exportAsPrint(Request $requests)
    {
        $user = Auth::user();
        $data = [];

        $country_name = $requests->input('country');
        $year = $requests->input('year');
        $month = rtrim($requests->input('month'), '"');

        if (($country_name === 'all' && $year === 'all' && $month === 'all') || ($country_name === null && $year === null && $month == null)){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id WHERE s.is_deleted = '0' AND b.id = $user->branch_id ORDER BY id ASC "));

        }elseif ($country_name !== 'all' && $year === 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND s.preferred_place = '$country_name' AND b.id = $user->branch_id ORDER BY id ASC "));

        }elseif ($country_name !== 'all' && $year !== 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND s.preferred_place = '$country_name' 
                          AND Year(s.enquiry_date) = '$year' AND b.id = $user->branch_id ORDER BY id ASC "));

        }elseif($country_name !== 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND s.preferred_place = '$country_name' 
                          AND Year(s.enquiry_date) = '$year' AND Month(s.enquiry_date) = '$month' AND b.id = $user->branch_id ORDER BY id ASC "));

        }elseif($country_name === 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND Month(s.enquiry_date) = '$month' AND b.id = $user->branch_id ORDER BY id ASC "));

        }elseif($country_name === 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND Year(s.enquiry_date) = '$year' 
                          AND Month(s.enquiry_date) = '$month' AND b.id = $user->branch_id ORDER BY id ASC "));

        }elseif($country_name !== 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND s.preferred_place = '$country_name' 
                          AND Month(s.enquiry_date) = '$month' AND b.id = $user->branch_id ORDER BY id ASC "));

        }elseif($country_name === 'all' && $year !== 'all' && $month === 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND Year(s.enquiry_date) = '$year' AND b.id = $user->branch_id ORDER BY id ASC "));

        }

        return view(parent::loadDefaultVars($this->view_path . '.enquiryPrint'), compact('data'));
    }

    public function exportAsPdf(Request $requests)
    {
        $user = Auth::user();

        $data = [];

        $country_name = $requests->input('country');
        $year = $requests->input('year');
        $month = rtrim($requests->input('month'), '"');

        if (($country_name === 'all' && $year === 'all' && $month === 'all') || ($country_name === null && $year === null && $month == null)){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id WHERE s.is_deleted = '0' AND b.id = $user->branch_id ORDER BY id ASC "));

        }elseif ($country_name !== 'all' && $year === 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND s.preferred_place = '$country_name' AND b.id = $user->branch_id ORDER BY id ASC "));

        }elseif ($country_name !== 'all' && $year !== 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND s.preferred_place = '$country_name' 
                          AND Year(s.enquiry_date) = '$year' AND b.id = $user->branch_id ORDER BY id ASC "));

        }elseif($country_name !== 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND s.preferred_place = '$country_name' 
                          AND Year(s.enquiry_date) = '$year' AND Month(s.enquiry_date) = '$month' AND b.id = $user->branch_id ORDER BY id ASC "));

        }elseif($country_name === 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND Month(s.enquiry_date) = '$month' AND b.id = $user->branch_id ORDER BY id ASC "));

        }elseif($country_name === 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND Year(s.enquiry_date) = '$year' 
                          AND Month(s.enquiry_date) = '$month' AND b.id = $user->branch_id ORDER BY id ASC "));

        }elseif($country_name !== 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND s.preferred_place = '$country_name' 
                          AND Month(s.enquiry_date) = '$month' AND b.id = $user->branch_id ORDER BY id ASC "));

        }elseif($country_name === 'all' && $year !== 'all' && $month === 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND Year(s.enquiry_date) = '$year' AND b.id = $user->branch_id ORDER BY id ASC "));

        }

        $pdf = PDF::loadView($this->view_path. '.enquiryPdf', compact('data'));

        return $pdf->download('StudentEnquiryListPDF.pdf');
    }

    public function exportAsExcel(Request $requests)
    {
        $user = Auth::user();

        $country_name = $requests->input('country');
        $year = $requests->input('year');
        $month = rtrim($requests->input('month'), '"');

        if (($country_name === 'all' && $year === 'all' && $month === 'all') || ($country_name === null && $year === null && $month == null)){

            $students = StudentEnquiry::join('users', 'users.id', '=', 'student_enquiry.user_id')
                            ->join('branch', 'branch.id', '=', 'student_enquiry.branch_id')
                            ->select('student_enquiry.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                                'student_enquiry.email', 'student_enquiry.mobile_phone', 'student_enquiry.year_of_completion_slc', 'student_enquiry.slc_percentage',
                                'student_enquiry.year_of_completion_plus_two', 'student_enquiry.plus_two_percentage', 'student_enquiry.preferred_place', 'branch.branch_name')
                            ->where('student_enquiry.is_deleted', '0')
                            ->where('branch.id', '=', $user->branch_id)
                            ->orderBy('student_enquiry.id', 'ASC')
                            ->get();

        }elseif ($country_name !== 'all' && $year === 'all' && $month === 'all'){

            $students = StudentEnquiry::join('users', 'users.id', '=', 'student_enquiry.user_id')
                ->join('branch', 'branch.id', '=', 'student_enquiry.branch_id')
                ->select('student_enquiry.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'student_enquiry.email', 'student_enquiry.mobile_phone', 'student_enquiry.year_of_completion_slc', 'student_enquiry.slc_percentage',
                    'student_enquiry.year_of_completion_plus_two', 'student_enquiry.plus_two_percentage', 'student_enquiry.preferred_place', 'branch.branch_name')
                ->where('student_enquiry.is_deleted', '0')
                ->where('branch.id', '=', $user->branch_id)
                ->where('student_enquiry.preferred_place', $country_name)
                ->orderBy('student_enquiry.id', 'ASC')
                ->get();

        }elseif ($country_name !== 'all' && $year !== 'all' && $month === 'all'){

            $students = StudentEnquiry::join('users', 'users.id', '=', 'student_enquiry.user_id')
                ->join('branch', 'branch.id', '=', 'student_enquiry.branch_id')
                ->select('student_enquiry.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'student_enquiry.email', 'student_enquiry.mobile_phone', 'student_enquiry.year_of_completion_slc', 'student_enquiry.slc_percentage',
                    'student_enquiry.year_of_completion_plus_two', 'student_enquiry.plus_two_percentage', 'student_enquiry.preferred_place', 'branch.branch_name')
                ->where('student_enquiry.is_deleted', '0')
                ->where('branch.id', '=', $user->branch_id)
                ->where('student_enquiry.preferred_place', $country_name)
                ->whereYear('student_enquiry.enquiry_date', '=', $year)
                ->orderBy('student_enquiry.id', 'ASC')
                ->get();

        }elseif($country_name !== 'all' && $year !== 'all' && $month !== 'all') {

            $students = StudentEnquiry::join('users', 'users.id', '=', 'student_enquiry.user_id')
                ->join('branch', 'branch.id', '=', 'student_enquiry.branch_id')
                ->select('student_enquiry.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'student_enquiry.email', 'student_enquiry.mobile_phone', 'student_enquiry.year_of_completion_slc', 'student_enquiry.slc_percentage',
                    'student_enquiry.year_of_completion_plus_two', 'student_enquiry.plus_two_percentage', 'student_enquiry.preferred_place', 'branch.branch_name')
                ->where('student_enquiry.is_deleted', '0')
                ->where('branch.id', '=', $user->branch_id)
                ->where('student_enquiry.preferred_place', $country_name)
                ->whereYear('student_enquiry.enquiry_date', '=', $year)
                ->whereMonth('student_enquiry.enquiry_date', '=', $month)
                ->orderBy('student_enquiry.id', 'ASC')
                ->get();

        }elseif($country_name === 'all' && $year === 'all' && $month !== 'all') {

            $students = StudentEnquiry::join('users', 'users.id', '=', 'student_enquiry.user_id')
                ->join('branch', 'branch.id', '=', 'student_enquiry.branch_id')
                ->select('student_enquiry.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'student_enquiry.email', 'student_enquiry.mobile_phone', 'student_enquiry.year_of_completion_slc', 'student_enquiry.slc_percentage',
                    'student_enquiry.year_of_completion_plus_two', 'student_enquiry.plus_two_percentage', 'student_enquiry.preferred_place', 'branch.branch_name')
                ->where('student_enquiry.is_deleted', '0')
                ->where('branch.id', '=', $user->branch_id)
                ->whereMonth('student_enquiry.enquiry_date', '=', $month)
                ->orderBy('student_enquiry.id', 'ASC')
                ->get();

        }elseif($country_name === 'all' && $year !== 'all' && $month !== 'all') {

            $students = StudentEnquiry::join('users', 'users.id', '=', 'student_enquiry.user_id')
                ->join('branch', 'branch.id', '=', 'student_enquiry.branch_id')
                ->select('student_enquiry.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'student_enquiry.email', 'student_enquiry.mobile_phone', 'student_enquiry.year_of_completion_slc', 'student_enquiry.slc_percentage',
                    'student_enquiry.year_of_completion_plus_two', 'student_enquiry.plus_two_percentage', 'student_enquiry.preferred_place', 'branch.branch_name')
                ->where('student_enquiry.is_deleted', '0')
                ->where('branch.id', '=', $user->branch_id)
                ->whereYear('student_enquiry.enquiry_date', '=', $year)
                ->whereMonth('student_enquiry.enquiry_date', '=', $month)
                ->orderBy('student_enquiry.id', 'ASC')
                ->get();

        }elseif($country_name !== 'all' && $year === 'all' && $month !== 'all') {

            $students = StudentEnquiry::join('users', 'users.id', '=', 'student_enquiry.user_id')
                ->join('branch', 'branch.id', '=', 'student_enquiry.branch_id')
                ->select('student_enquiry.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'student_enquiry.email', 'student_enquiry.mobile_phone', 'student_enquiry.year_of_completion_slc', 'student_enquiry.slc_percentage',
                    'student_enquiry.year_of_completion_plus_two', 'student_enquiry.plus_two_percentage', 'student_enquiry.plus_two_preferred_placeh.branch_name')
                ->where('student_enquiry.is_deleted', '0')
                ->where('branch.id', '=', $user->branch_id)
                ->where('student_enquiry.preferred_place', $country_name)
                ->whereMonth('student_enquiry.enquiry_date', '=', $month)
                ->orderBy('student_enquiry.id', 'ASC')
                ->get();

        }elseif($country_name === 'all' && $year !== 'all' && $month === 'all') {

            $students = StudentEnquiry::join('users', 'users.id', '=', 'student_enquiry.user_id')
                ->join('branch', 'branch.id', '=', 'student_enquiry.branch_id')
                ->select('student_enquiry.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'student_enquiry.email', 'student_enquiry.mobile_phone', 'student_enquiry.year_of_completion_slc', 'student_enquiry.slc_percentage',
                    'student_enquiry.year_of_completion_plus_two', 'student_enquiry.plus_two_percentage', 'student_enquiry.preferred_place', 'branch.branch_name')
                ->where('student_enquiry.is_deleted', '0')
                ->where('branch.id', '=', $user->branch_id)
                ->whereYear('student_enquiry.enquiry_date', '=', $year)
                ->orderBy('student_enquiry.id', 'ASC')
                ->get();

        }

        $studentArray = [];

        // Define the Excel spreadsheet headers
        $studentArray[] = ['S.N.', 'First Name', 'Last Name', 'Email', 'Mobile Number', 'SLC Year', 'SLC Percentage', '+2 Year', '+2 Percentage', 'Preferred Country', 'Branch Name'];

        // Convert each member of the returned collection into an array,
        // and append it to the student array.
        foreach ($students as $student) {
            $studentArray[] = $student->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('Student Enquiry Information', function($excel) use ($studentArray) {

            // Build the spreadsheet, passing in the student array
            $excel->sheet('sheet1', function($sheet) use ($studentArray) {
                $sheet->fromArray($studentArray, null, 'A1', false, false);
            });

        })->export('xls');
    }
    protected function addCounselorFeedback($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['row'] = $this->model;

        return view(parent::loadDefaultVars($this->view_path . '.storeCounselorFeedback'), compact('data'));

    }

    protected function storeCounselorFeedback(AddCounselorFeedbackValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        $user = Auth::user();

        DB::table('student_enquiry')
            ->where('id', $id)
            ->update([
                'is_counselled'         => 1,
                'counselling_date'      => Carbon::now()->format('Y-m-d H:i:s'),
                'counsellor_remarks'    => $request->get('counsellor_remarks'),
                'counsellor_id'         => $user->id,
            ]);

        $this->editLog($user->id, $request->get('counsellor_remarks'), 'Student Enquiry');

        AppHelper::flash('success', 'Record updated successfully.');

        return redirect()->route($this->base_route.'.list');

    }

    protected function editCounselorFeedback($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['row'] = $this->model;

        return view(parent::loadDefaultVars($this->view_path . '.updateCounselorFeedback'), compact('data'));

    }

    protected function updateCounselorFeedback(UpdateCounselorFeedbackValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        $user = Auth::user();

        DB::table('student_enquiry')
            ->where('id', $id)
            ->update([
                'counselling_date'      => Carbon::now()->format('Y-m-d H:i:s'),
                'counsellor_remarks'    => $request->get('counsellor_remarks'),
                'counsellor_id'         => $user->id,
            ]);

        $this->editLog($user->id, $request->get('counsellor_remarks'), 'Student Enquiry');

        AppHelper::flash('success', 'Record updated successfully.');

        return redirect()->route($this->base_route.'.list');

    }

    protected function goToRegistration($id){
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        return view(parent::loadDefaultVars($this->view_path . '.create'), compact('data'));
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = StudentEnquiry::find($id);

        return $this->model;
    }
}
