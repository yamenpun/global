<?php

namespace App\Http\Controllers\Counselor;

use App\Models\Branch;
use App\Models\College;
use App\Models\Course;
use App\Models\StudentRegistration;
use Auth;
use Gate;
use DB;
use App\Models\StudentEnquiry;
use PDF;
use Carbon;
use Excel;
use AppHelper;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\Register\AddFormValidation;
use App\Http\Requests\Admin\Register\UpdateFormValidation;


class RegisterController extends CounselorBaseController {

    protected $view_path  = 'counselor.register';
    protected $base_route = 'counselor.register';
    protected $model;
    
    public function index(Request $requests)
    {
        $user = Auth::user();
        $data = [];

        $data['college_country'] = College::select('id', 'college_country')->groupBy('college_country')->get();

        $country_name = $requests->input('country');
        $year = $requests->input('year');
        $month = $requests->input('month');

        if (($country_name === 'all' && $year === 'all' && $month === 'all') || ($country_name === null && $year === null && $month == null)){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name, co.course_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif ($country_name !== 'all' && $year === 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name, co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND c.college_country = '$country_name'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif ($country_name !== 'all' && $year !== 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND c.college_country = '$country_name' AND Year(sr.register_date) = '$year'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif($country_name !== 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND c.college_country = '$country_name' 
                          AND Year(sr.register_date) = '$year' AND Month(sr.register_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif($country_name === 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND Month(sr.register_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif($country_name === 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' 
                          AND Year(sr.register_date) = '$year' AND Month(sr.register_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif($country_name !== 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND c.college_country = '$country_name' 
                          AND Month(sr.register_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif($country_name === 'all' && $year !== 'all' && $month === 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND Year(sr.register_date) = '$year'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }


    public function view($id)
    {
        $data = [];
        $data['row'] = DB::select( DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name, co.course_name, u.fullname, b.branch_name FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.id = '$id' " ));

        return view(parent::loadDefaultVars($this->view_path . '.view'), compact('data'));
    }


    public function exportAsPrint(Request $requests)
    {
        $user = Auth::user();
        $data = [];
        $data['branch'] = Branch::select('id', 'branch_name')->get();

        $country_name = $requests->input('country');
        $year = $requests->input('year');
        $month = rtrim($requests->input('month'), '"');

        if (($country_name === 'all' && $year === 'all' && $month === 'all') || ($country_name === null && $year === null && $month == null)){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif ($country_name !== 'all' && $year === 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND c.college_country = '$country_name' 
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif ($country_name !== 'all' && $year !== 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND c.college_country = '$country_name' 
                          AND Year(sr.register_date) = '$year' AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif($country_name !== 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND c.college_country = '$country_name' 
                          AND Year(sr.register_date) = '$year' AND Month(sr.register_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif($country_name === 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND Month(sr.register_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif($country_name === 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0'
                          AND Year(sr.register_date) = '$year' AND Month(sr.register_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif($country_name !== 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND c.college_country = '$country_name' 
                          AND Month(sr.register_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif($country_name === 'all' && $year !== 'all' && $month === 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND Year(sr.register_date) = '$year'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }

        return view(parent::loadDefaultVars($this->view_path . '.registerPrint'), compact('data'));
    }

    public function exportAsPdf(Request $requests)
    {
        $user = Auth::user();
        $data = [];
        $data['college_country'] = College::select('id', 'college_country')->groupBy('college_country')->get();

        $country_name = $requests->input('country');
        $year = $requests->input('year');
        $month = rtrim($requests->input('month'), '"');

        if (($country_name === 'all' && $year === 'all' && $month === 'all') || ($country_name === null && $year === null && $month == null)){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif ($country_name !== 'all' && $year === 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND c.college_country = '$country_name' 
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif ($country_name !== 'all' && $year !== 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND c.college_country = '$country_name' 
                          AND Year(sr.register_date) = '$year' AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif($country_name !== 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND c.college_country = '$country_name' 
                          AND Year(sr.register_date) = '$year' AND Month(sr.register_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif($country_name === 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND Month(sr.register_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif($country_name === 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0'
                          AND Year(sr.register_date) = '$year' AND Month(sr.register_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif($country_name !== 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND c.college_country = '$country_name' 
                          AND Month(sr.register_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif($country_name === 'all' && $year !== 'all' && $month === 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND Year(sr.register_date) = '$year'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }

        $pdf = PDF::loadView($this->view_path. '.registerPdf', compact('data'));

        return $pdf->download('StudentRegistrationListPDF.pdf');
    }

    public function exportAsExcel(Request $requests)
    {
        $user = Auth::user();
        $data = [];
        $data['college_country'] = College::select('id', 'college_country')->groupBy('college_country')->get();

        $country_name = $requests->input('country');
        $year = $requests->input('year');
        $month = rtrim($requests->input('month'), '"');

        if (($country_name === 'all' && $year === 'all' && $month === 'all') || ($country_name === null && $year === null && $month == null)){

            $registration = StudentRegistration::join('users', 'users.id', '=', 'student_registration.user_id')
                            ->join('branch', 'branch.id', '=', 'student_registration.branch_id')
                            ->join('student_enquiry', 'student_registration.student_id', '=', 'student_enquiry.id')
                            ->join('college', 'college.id', '=', 'student_registration.college_id')
                            ->join('course', 'course.id', '=', 'student_registration.course_id')
                            ->select('student_registration.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                                'college.college_name', 'course.course_name', 'student_registration.intake',
                                'student_registration.date_of_birth', 'users.fullname', 'branch.branch_name')
                            ->where('student_registration.is_deleted', '0')
                            ->where('branch.id', '=', $user->branch_id)
                            ->orderBy('student_registration.id', 'ASC')
                            ->get();

        }elseif ($country_name !== 'all' && $year === 'all' && $month === 'all'){

            $registration = StudentRegistration::join('users', 'users.id', '=', 'student_registration.user_id')
                ->join('branch', 'branch.id', '=', 'student_registration.branch_id')
                ->join('student_enquiry', 'student_registration.student_id', '=', 'student_enquiry.id')
                ->join('college', 'college.id', '=', 'student_registration.college_id')
                ->join('course', 'course.id', '=', 'student_registration.course_id')
                ->select('student_registration.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'college.college_name', 'course.course_name', 'student_registration.intake',
                    'student_registration.date_of_birth', 'users.fullname', 'branch.branch_name')
                ->where('student_registration.is_deleted', '0')
                ->where('branch.branch_name', $country_name)
                ->where('branch.id', '=', $user->branch_id)
                ->orderBy('student_registration.id', 'ASC')
                ->get();

        }elseif ($country_name !== 'all' && $year !== 'all' && $month === 'all'){

            $registration = StudentRegistration::join('users', 'users.id', '=', 'student_registration.user_id')
                ->join('branch', 'branch.id', '=', 'student_registration.branch_id')
                ->join('student_enquiry', 'student_registration.student_id', '=', 'student_enquiry.id')
                ->join('college', 'college.id', '=', 'student_registration.college_id')
                ->join('course', 'course.id', '=', 'student_registration.course_id')
                ->select('student_registration.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'college.college_name', 'course.course_name', 'student_registration.intake',
                    'student_registration.date_of_birth', 'users.fullname', 'branch.branch_name')
                ->where('student_registration.is_deleted', '0')
                ->where('branch.branch_name', $country_name)
                ->where('branch.id', '=', $user->branch_id)
                ->whereYear('student_registration.register_date', '=', $year)
                ->orderBy('student_registration.id', 'ASC')
                ->get();

        }elseif($country_name !== 'all' && $year !== 'all' && $month !== 'all') {

            $registration = StudentRegistration::join('users', 'users.id', '=', 'student_registration.user_id')
                ->join('branch', 'branch.id', '=', 'student_registration.branch_id')
                ->join('student_enquiry', 'student_registration.student_id', '=', 'student_enquiry.id')
                ->join('college', 'college.id', '=', 'student_registration.college_id')
                ->join('course', 'course.id', '=', 'student_registration.course_id')
                ->select('student_registration.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'college.college_name', 'course.course_name', 'student_registration.intake',
                    'student_registration.date_of_birth', 'users.fullname', 'branch.branch_name')
                ->where('student_registration.is_deleted', '0')
                ->where('branch.branch_name', $country_name)
                ->where('branch.id', '=', $user->branch_id)
                ->whereYear('student_registration.register_date', '=', $year)
                ->whereMonth('student_registration.register_date', '=', $month)
                ->orderBy('student_registration.id', 'ASC')
                ->get();


        }elseif($country_name === 'all' && $year === 'all' && $month !== 'all') {

            $registration = StudentRegistration::join('users', 'users.id', '=', 'student_registration.user_id')
                ->join('branch', 'branch.id', '=', 'student_registration.branch_id')
                ->join('student_enquiry', 'student_registration.student_id', '=', 'student_enquiry.id')
                ->join('college', 'college.id', '=', 'student_registration.college_id')
                ->join('course', 'course.id', '=', 'student_registration.course_id')
                ->select('student_registration.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'college.college_name', 'course.course_name', 'student_registration.intake',
                    'student_registration.date_of_birth', 'users.fullname', 'branch.branch_name')
                ->where('student_registration.is_deleted', '0')
                ->where('branch.id', '=', $user->branch_id)
                ->whereMonth('student_registration.register_date', '=', $month)
                ->orderBy('student_registration.id', 'ASC')
                ->get();

        }elseif($country_name === 'all' && $year !== 'all' && $month !== 'all') {

            $registration = StudentRegistration::join('users', 'users.id', '=', 'student_registration.user_id')
                ->join('branch', 'branch.id', '=', 'student_registration.branch_id')
                ->join('student_enquiry', 'student_registration.student_id', '=', 'student_enquiry.id')
                ->join('college', 'college.id', '=', 'student_registration.college_id')
                ->join('course', 'course.id', '=', 'student_registration.course_id')
                ->select('student_registration.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'college.college_name', 'course.course_name', 'student_registration.intake',
                    'student_registration.date_of_birth', 'users.fullname', 'branch.branch_name')
                ->where('student_registration.is_deleted', '0')
                ->where('branch.id', '=', $user->branch_id)
                ->whereYear('student_registration.register_date', '=', $year)
                ->whereMonth('student_registration.register_date', '=', $month)
                ->orderBy('student_registration.id', 'ASC')
                ->get();

        }elseif($country_name !== 'all' && $year === 'all' && $month !== 'all') {

            $registration = StudentRegistration::join('users', 'users.id', '=', 'student_registration.user_id')
                ->join('branch', 'branch.id', '=', 'student_registration.branch_id')
                ->join('student_enquiry', 'student_registration.student_id', '=', 'student_enquiry.id')
                ->join('college', 'college.id', '=', 'student_registration.college_id')
                ->join('course', 'course.id', '=', 'student_registration.course_id')
                ->select('student_registration.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'college.college_name', 'course.course_name', 'student_registration.intake',
                    'student_registration.date_of_birth', 'users.fullname', 'branch.branch_name')
                ->where('student_registration.is_deleted', '0')
                ->where('branch.id', '=', $user->branch_id)
                ->where('branch.branch_name', $country_name)
                ->whereMonth('student_registration.register_date', '=', $month)
                ->orderBy('student_registration.id', 'ASC')
                ->get();

        }elseif($country_name === 'all' && $year !== 'all' && $month === 'all') {

            $registration = StudentRegistration::join('users', 'users.id', '=', 'student_registration.user_id')
                ->join('branch', 'branch.id', '=', 'student_registration.branch_id')
                ->join('student_enquiry', 'student_registration.student_id', '=', 'student_enquiry.id')
                ->join('college', 'college.id', '=', 'student_registration.college_id')
                ->join('course', 'course.id', '=', 'student_registration.course_id')
                ->select('student_registration.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'college.college_name', 'course.course_name', 'student_registration.intake',
                    'student_registration.date_of_birth', 'users.fullname', 'branch.branch_name')
                ->where('student_registration.is_deleted', '0')
                ->where('branch.id', '=', $user->branch_id)
                ->whereYear('student_registration.register_date', '=', $year)
                ->orderBy('student_registration.id', 'ASC')
                ->get();

        }

        $registrationArray = [];

        // Define the Excel spreadsheet headers
        $registrationArray[] = ['S.N.', 'First Name', 'Last Name', 'College Name', 'Course Name', 'Intake', 'Date of Birth', 'Registered By', 'Branch Name'];

        // Convert each member of the returned collection into an array,
        // and append it to the register array.
        foreach ($registration as $registration) {
            $registrationArray[] = $registration->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('Student Registration Information', function($excel) use ($registrationArray) {

            // Build the spreadsheet, passing in the register array
            $excel->sheet('sheet1', function($sheet) use ($registrationArray) {
                $sheet->fromArray($registrationArray, null, 'A1', false, false);
            });

        })->export('xls');
    }

    public function create()
    {
        $data = [];
        $data['student'] = StudentEnquiry::select('id', 'student_first_name', 'student_middle_name', 'student_last_name')
                            ->orderBy('student_first_name', 'ASC')->get();

        $data['college'] = College::select('id', 'college_name')->orderBy('college_name', 'ASC')->get();

        $data['course'] = Course::select('id', 'course_name')->orderBy('course_name', 'ASC')->get();

        return view(parent::loadDefaultVars($this->view_path . '.create'), compact('data'));
    }

    public function store(AddFormValidation $request)
    {
        $user = Auth::user();

        StudentRegistration::create([
            'student_id'                    => $request->get('student_id'),
            'register_date'                 => $request->get('register_date'),
            'college_id'                    => $request->get('college_id'),
            'course_id'                     => $request->get('course_id'),
            'date_of_birth'                 => $request->get('date_of_birth'),
            'intake'                        => $request->get('intake'),
            'father_name'                   => $request->get('father_name'),
            'father_occupation'             => $request->get('father_occupation'),
            'mother_name'                   => $request->get('mother_name'),
            'mother_occupation'             => $request->get('mother_occupation'),
            'permanent_address'             => $request->get('permanent_address'),
            'temporary_address'             => $request->get('temporary_address'),
            'toefl_score'                   => $request->get('toefl_score'),
            'ielts_score'                   => $request->get('ielts_score'),
            'sat_score'                     => $request->get('sat_score'),
            'gmat_score'                    => $request->get('gmat_score'),
            'signature_date'                => $request->get('signature_date'),
            'emergency_contact_number'      => $request->get('emergency_contact_number'),
            'user_id'                       => $user->id,
            'branch_id'                     => $user->branch_id,
        ]);

        DB::table('student_enquiry')
            ->where('id', $request->get('student_id'))
            ->update(['is_registered' => 1]);

        AppHelper::flash('success', 'Record created Successfully.');

        return redirect()->route($this->base_route.'.list');
    }

    public function edit($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];

        $data['student'] = StudentEnquiry::select('id', 'student_first_name', 'student_middle_name', 'student_last_name')
            ->orderBy('student_first_name', 'ASC')->get();

        $data['college'] = College::select('id', 'college_name')->orderBy('college_name', 'ASC')->get();

        $data['course'] = Course::select('id', 'course_name')->orderBy('course_name', 'ASC')->get();

        $data['row'] = $this->model;

        return view(parent::loadDefaultVars($this->view_path . '.edit'), compact('data'));
    }

    public function update(UpdateFormValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        $user = Auth::user();
        $data = $this->model;

        $data->update([
            'student_id'                    => $request->get('student_id'),
            'register_date'                 => $request->get('register_date'),
            'college_id'                    => $request->get('college_id'),
            'course_applied_for'            => $request->get('course_applied_for'),
            'date_of_birth'                 => $request->get('date_of_birth'),
            'intake'                        => $request->get('intake'),
            'father_name'                   => $request->get('father_name'),
            'father_occupation'             => $request->get('father_occupation'),
            'mother_name'                   => $request->get('mother_name'),
            'mother_occupation'             => $request->get('mother_occupation'),
            'permanent_address'             => $request->get('permanent_address'),
            'temporary_address'             => $request->get('temporary_address'),
            'toefl_score'                   => $request->get('toefl_score'),
            'ielts_score'                   => $request->get('ielts_score'),
            'sat_score'                     => $request->get('sat_score'),
            'gmat_score'                    => $request->get('gmat_score'),
            'signature_date'                => $request->get('signature_date'),
            'emergency_contact_number'      => $request->get('emergency_contact_number'),
            'user_id'                       => $user->id,
            'branch_id'                     => $user->branch_id,
        ]);

        $this->editLog($user->id, $request->get('date_of_birth'), 'Registration');

        AppHelper::flash('success', 'Record updated successfully.');

        return redirect()->route($this->base_route.'.list');
    }

    public function destroy($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route.'.list');
        }

        $user = Auth::user();

        $register = StudentRegistration::where('id', '=', $id)->first();

        DB::table('student_registration')
            ->where('id', $id)
            ->update(['is_deleted' => 1]);

        $this->deleteLog($user->id, $register->date_of_birth, 'Registration');

        AppHelper::flash('success', 'Record deleted successfully.');

        return redirect()->route($this->base_route.'.list');
    }

    public function restore($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route.'.deletedList');
        }

        DB::table('student_registration')
            ->where('id', $id)
            ->update(['is_deleted' => 0]);

        AppHelper::flash('success', 'Record restored successfully.');

        return redirect()->route($this->base_route.'.deletedList');
    }

    public function deletePermanent($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route.'.deletedList');
        }

        $registration = StudentRegistration::where('id', '=', $id)->first();

        StudentRegistration::destroy($id);

        $user = Auth::user();

        $this->deletePermanentLog($user->id, $registration->date_of_birth, 'Registration');

        AppHelper::flash('success', 'Record permanently deleted from database.');

        return redirect()->route($this->base_route.'.deletedList');
    }

    protected function addCounselorFeedback($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['row'] = $this->model;

        return view(parent::loadDefaultVars($this->view_path . '.storeCounselorFeedback'), compact('data'));

    }

    protected function storeCounselorFeedback(AddCounselorFeedbackValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        $user = Auth::user();

        DB::table('student_enquiry')
            ->where('id', $id)
            ->update([
                'is_counselled'         => 1,
                'counselling_date'      => Carbon::now()->format('Y-m-d H:i:s'),
                'counsellor_remarks'    => $request->get('counsellor_remarks'),
                'counsellor_id'         => $user->id,
            ]);

        $this->editLog($user->id, $request->get('counsellor_remarks'), 'Student Enquiry');

        AppHelper::flash('success', 'Record updated successfully.');

        return redirect()->route($this->base_route.'.list');

    }

    protected function editCounselorFeedback($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['row'] = $this->model;

        return view(parent::loadDefaultVars($this->view_path . '.updateCounselorFeedback'), compact('data'));

    }

    protected function updateCounselorFeedback(UpdateCounselorFeedbackValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        $user = Auth::user();

        DB::table('student_enquiry')
            ->where('id', $id)
            ->update([
                'counselling_date'      => Carbon::now()->format('Y-m-d H:i:s'),
                'counsellor_remarks'    => $request->get('counsellor_remarks'),
                'counsellor_id'         => $user->id,
            ]);

        $this->editLog($user->id, $request->get('counsellor_remarks'), 'Student Enquiry');

        AppHelper::flash('success', 'Record updated successfully.');

        return redirect()->route($this->base_route.'.list');

    }

    protected function goToRegistration($id){
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        return view(parent::loadDefaultVars($this->view_path . '.create'), compact('data'));
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = StudentRegistration::find($id);

        return $this->model;
    }
}
