<?php

namespace App\Http\Controllers;

class HomeController extends AppBaseController
{

    public function index()
    {
        if(auth()->check()){

            $user = auth()->user();

            if ($user->role == 'superadmin') {
                return redirect()->route('superadmin.dashboard');
            } elseif($user->role == 'admin') {
                return redirect()->route('admin.dashboard');
            }elseif ($user->role == 'counselor'){
                return redirect()->route('counselor.dashboard');
            }elseif ($user->role == 'normal'){
                return redirect()->route('normal.dashboard');
            }
        }

        return view('auth.login');
    }
}
