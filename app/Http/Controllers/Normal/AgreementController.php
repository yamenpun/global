<?php

namespace App\Http\Controllers\Normal;

use App\Models\Agreement;
use App\Models\College;
use Auth;
use Gate;
use DB;
use Illuminate\Support\Facades\URL;
use PDF;
use Carbon;
use Excel;
use AppHelper;
use App\Http\Requests;
use Illuminate\Http\Request;


class AgreementController extends NormalBaseController {

    protected $view_path  = 'normal.agreement';
    protected $base_route = 'normal.agreement';
    protected $model;
    
    public function index(Request $requests)
    {
        $user = Auth::user();

        $data = [];
        $data['college'] = College::select('id', 'college_country')->groupBy('college_country')->get();

        $country_name = $requests->input('country');
        $year = $requests->input('year');
        $month = $requests->input('month');

        if (($country_name === 'all' && $year === 'all' && $month === 'all') || ($country_name === null && $year === null && $month == null)){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT a.id, a.agreement_date, s.student_first_name, s.student_middle_name, s.student_last_name, a.grand_father_name, 
                          c.college_country, u.fullname FROM agreement AS a INNER JOIN 
                          student_registration AS sr ON a.register_id = sr.id INNER JOIN 
                          student_enquiry AS s ON sr.student_id = s.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id INNER JOIN 
                          users AS u ON a.user_id = u.id 
                          WHERE a.is_deleted = '0' AND b.id = $user->branch_id ORDER BY a.id ASC "));

        }elseif ($country_name !== 'all' && $year === 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT a.id, a.agreement_date, s.student_first_name, s.student_middle_name, s.student_last_name, a.grand_father_name, 
                          c.college_country, u.fullname FROM agreement AS a INNER JOIN 
                          student_registration AS sr ON a.register_id = sr.id INNER JOIN 
                          student_enquiry AS s ON sr.student_id = s.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id INNER JOIN 
                          users AS u ON a.user_id = u.id 
                          WHERE a.is_deleted = '0' AND c.college_country = '$country_name'
                          AND b.id = $user->branch_id ORDER BY a.id ASC "));

        }elseif ($country_name !== 'all' && $year !== 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT a.id, a.agreement_date, s.student_first_name, s.student_middle_name, s.student_last_name, a.grand_father_name, 
                          c.college_country, u.fullname FROM agreement AS a INNER JOIN 
                          student_registration AS sr ON a.register_id = sr.id INNER JOIN 
                          student_enquiry AS s ON sr.student_id = s.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id INNER JOIN 
                          users AS u ON a.user_id = u.id 
                          WHERE a.is_deleted = '0' AND c.college_country = '$country_name' AND Year(a.agreement_date) = '$year'
                          AND b.id = $user->branch_id ORDER BY a.id ASC "));

        }elseif($country_name !== 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT a.id, a.agreement_date, s.student_first_name, s.student_middle_name, s.student_last_name, a.grand_father_name, 
                          c.college_country, u.fullname FROM agreement AS a INNER JOIN 
                          student_registration AS sr ON a.register_id = sr.id INNER JOIN 
                          student_enquiry AS s ON sr.student_id = s.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id INNER JOIN 
                          users AS u ON a.user_id = u.id 
                          WHERE a.is_deleted = '0' AND c.college_country = '$country_name' 
                          AND Year(a.agreement_date) = '$year' AND Month(a.agreement_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY a.id ASC "));

        }elseif($country_name === 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT a.id, a.agreement_date, s.student_first_name, s.student_middle_name, s.student_last_name, a.grand_father_name, 
                          c.college_country, u.fullname FROM agreement AS a INNER JOIN 
                          student_registration AS sr ON a.register_id = sr.id INNER JOIN 
                          student_enquiry AS s ON sr.student_id = s.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id INNER JOIN 
                          users AS u ON a.user_id = u.id WHERE a.is_deleted = '0' AND Month(a.agreement_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY a.id ASC "));

        }elseif($country_name === 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT a.id, a.agreement_date, s.student_first_name, s.student_middle_name, s.student_last_name, a.grand_father_name, 
                          c.college_country, u.fullname FROM agreement AS a INNER JOIN 
                          student_registration AS sr ON a.register_id = sr.id INNER JOIN 
                          student_enquiry AS s ON sr.student_id = s.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id INNER JOIN 
                          users AS u ON a.user_id = u.id WHERE a.is_deleted = '0'
                          AND Year(a.agreement_date) = '$year' AND Month(a.agreement_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY a.id ASC "));

        }elseif($country_name !== 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT a.id, a.agreement_date, s.student_first_name, s.student_middle_name, s.student_last_name, a.grand_father_name, 
                          c.college_country, u.fullname FROM agreement AS a INNER JOIN 
                          student_registration AS sr ON a.register_id = sr.id INNER JOIN 
                          student_enquiry AS s ON sr.student_id = s.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id INNER JOIN 
                          users AS u ON a.user_id = u.id WHERE a.is_deleted = '0' AND c.college_country = '$country_name' 
                          AND Month(a.agreement_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY a.id ASC "));

        }elseif($country_name === 'all' && $year !== 'all' && $month === 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT a.id, a.agreement_date, s.student_first_name, s.student_middle_name, s.student_last_name, a.grand_father_name, 
                          c.college_country, u.fullname FROM agreement AS a INNER JOIN 
                          student_registration AS sr ON a.register_id = sr.id INNER JOIN 
                          student_enquiry AS s ON sr.student_id = s.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id INNER JOIN 
                          users AS u ON a.user_id = u.id WHERE a.is_deleted = '0' AND Year(a.agreement_date) = '$year'
                          AND b.id = $user->branch_id ORDER BY a.id ASC "));

        }

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function view($id)
    {
        $data = [];

        $data['row'] = DB::select( DB::raw(" 
                          SELECT a.*, s.student_first_name, s.student_middle_name, s.student_last_name, sr.father_name, 
                           sr.date_of_birth, co.course_name, c.college_name, c.college_country
                          FROM agreement AS a INNER JOIN 
                          student_registration AS sr ON a.register_id = sr.id INNER JOIN 
                          student_enquiry AS s ON sr.student_id = s.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id WHERE a.id = '$id' " ));

        return view(parent::loadDefaultVars($this->view_path . '.view'), compact('data'));
    }

    public function exportAsPrint(Request $requests)
    {
        $user = Auth::user();
        $data = [];
        $data['college'] = College::select('id', 'college_country')->groupBy('college_country')->get();

        $country_name    = $requests->input('country');
        $year           = $requests->input('year');
        $month          = rtrim($requests->input('month'), '"');

        if (($country_name === 'all' && $year === 'all' && $month === 'all') || ($country_name === null && $year === null && $month == null)){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT a.agreement_date, a.grand_father_name, s.student_first_name, s.student_middle_name, s.student_last_name, 
                          a.district, a.vdc_municipality, a.ward_no, a.tole,  c.college_name, co.course_name, b.branch_name FROM agreement AS a INNER JOIN 
                          student_registration AS sr ON a.register_id = sr.id INNER JOIN 
                          student_enquiry AS s ON sr.student_id = s.id INNER JOIN 
                          college AS c ON c.id = sr.college_id INNER JOIN 
                          course AS co ON co.id = sr.course_id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE a.is_deleted = '0' AND 
                          b.id = $user->branch_id ORDER BY a.id ASC "));

        }elseif ($country_name !== 'all' && $year === 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT a.agreement_date, a.grand_father_name, s.student_first_name, s.student_middle_name, s.student_last_name, 
                          a.district, a.vdc_municipality, a.ward_no, a.tole, c.college_name, co.course_name, b.branch_name FROM agreement AS a INNER JOIN 
                          student_registration AS sr ON a.register_id = sr.id INNER JOIN 
                          student_enquiry AS s ON sr.student_id = s.id INNER JOIN 
                          college AS c ON c.id = sr.college_id INNER JOIN 
                          course AS co ON co.id = sr.course_id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE a.is_deleted = '0' AND c.college_country = '$country_name' AND
                          b.id = $user->branch_id ORDER BY a.id ASC "));

        }elseif ($country_name !== 'all' && $year !== 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT a.agreement_date, a.grand_father_name, s.student_first_name, s.student_middle_name, s.student_last_name, 
                          a.district, a.vdc_municipality, a.ward_no, a.tole, c.college_name, co.course_name, b.branch_name FROM agreement AS a INNER JOIN 
                          student_registration AS sr ON a.register_id = sr.id INNER JOIN 
                          student_enquiry AS s ON sr.student_id = s.id INNER JOIN 
                          college AS c ON c.id = sr.college_id INNER JOIN 
                          course AS co ON co.id = sr.course_id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE a.is_deleted = '0' AND c.college_country = '$country_name' 
                          AND Year(a.agreement_date) = '$year' AND b.id = $user->branch_id ORDER BY a.id ASC "));

        }elseif($country_name !== 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT a.agreement_date, a.grand_father_name, s.student_first_name, s.student_middle_name, s.student_last_name, 
                          a.district, a.vdc_municipality, a.ward_no, a.tole, c.college_name, co.course_name, b.branch_name FROM agreement AS a INNER JOIN 
                          student_registration AS sr ON a.register_id = sr.id INNER JOIN 
                          student_enquiry AS s ON sr.student_id = s.id INNER JOIN 
                          college AS c ON c.id = sr.college_id INNER JOIN 
                          course AS co ON co.id = sr.course_id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE a.is_deleted = '0' AND c.college_country = '$country_name' 
                          AND Year(a.agreement_date) = '$year' AND Month(a.agreement_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY a.id ASC "));

        }elseif($country_name === 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT a.agreement_date, a.grand_father_name, s.student_first_name, s.student_middle_name, s.student_last_name, 
                          a.district, a.vdc_municipality, a.ward_no, a.tole, c.college_name, co.course_name, b.branch_name FROM agreement AS a INNER JOIN 
                          student_registration AS sr ON a.register_id = sr.id INNER JOIN 
                          student_enquiry AS s ON sr.student_id = s.id INNER JOIN 
                          college AS c ON c.id = sr.college_id INNER JOIN 
                          course AS co ON co.id = sr.course_id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE a.is_deleted = '0' AND Month(a.agreement_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY a.id ASC "));

        }elseif($country_name === 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT a.agreement_date, a.grand_father_name, s.student_first_name, s.student_middle_name, s.student_last_name, 
                          a.district, a.vdc_municipality, a.ward_no, a.tole, c.college_name, co.course_name, b.branch_name FROM agreement AS a INNER JOIN 
                          student_registration AS sr ON a.register_id = sr.id INNER JOIN 
                          student_enquiry AS s ON sr.student_id = s.id INNER JOIN 
                          college AS c ON c.id = sr.college_id INNER JOIN 
                          course AS co ON co.id = sr.course_id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE a.is_deleted = '0'
                          AND Year(a.agreement_date) = '$year' AND Month(a.agreement_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY a.id ASC "));

        }elseif($country_name !== 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT a.agreement_date, a.grand_father_name, s.student_first_name, s.student_middle_name, s.student_last_name, 
                          a.district, a.vdc_municipality, a.ward_no, a.tole, c.college_name, co.course_name, b.branch_name FROM agreement AS a INNER JOIN 
                          student_registration AS sr ON a.register_id = sr.id INNER JOIN 
                          student_enquiry AS s ON sr.student_id = s.id INNER JOIN 
                          college AS c ON c.id = sr.college_id INNER JOIN 
                          course AS co ON co.id = sr.course_id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE a.is_deleted = '0' AND c.college_country = '$country_name' 
                          AND Month(a.agreement_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY a.id ASC "));

        }elseif($country_name === 'all' && $year !== 'all' && $month === 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT a.agreement_date, a.grand_father_name, s.student_first_name, s.student_middle_name, s.student_last_name, 
                          a.district, a.vdc_municipality, a.ward_no, a.tole, c.college_name, co.course_name, b.branch_name FROM agreement AS a INNER JOIN 
                          student_registration AS sr ON a.register_id = sr.id INNER JOIN 
                          student_enquiry AS s ON sr.student_id = s.id INNER JOIN 
                          college AS c ON c.id = sr.college_id INNER JOIN 
                          course AS co ON co.id = sr.course_id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE a.is_deleted = '0' AND Year(a.agreement_date) = '$year'
                          AND b.id = $user->branch_id ORDER BY a.id ASC "));

        }

        return view(parent::loadDefaultVars($this->view_path . '.agreementPrint'), compact('data'));
    }

    public function exportAsPdf(Request $requests)
    {
        $user = Auth::user();
        $data = [];
        $data['college'] = College::select('id', 'college_country')->groupBy('college_country')->get();

        $country_name    = $requests->input('country');
        $year           = $requests->input('year');
        $month          = rtrim($requests->input('month'), '"');

        if (($country_name === 'all' && $year === 'all' && $month === 'all') || ($country_name === null && $year === null && $month == null)){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT a.agreement_date, a.grand_father_name, s.student_first_name, s.student_middle_name, s.student_last_name, 
                          a.district, a.vdc_municipality, a.ward_no, a.tole,  c.college_name, co.course_name, b.branch_name FROM agreement AS a INNER JOIN 
                          student_registration AS sr ON a.register_id = sr.id INNER JOIN 
                          student_enquiry AS s ON sr.student_id = s.id INNER JOIN 
                          college AS c ON c.id = sr.college_id INNER JOIN 
                          course AS co ON co.id = sr.course_id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE a.is_deleted = '0'
                          AND b.id = $user->branch_id ORDER BY a.id ASC "));

        }elseif ($country_name !== 'all' && $year === 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT a.agreement_date, a.grand_father_name, s.student_first_name, s.student_middle_name, s.student_last_name, 
                          a.district, a.vdc_municipality, a.ward_no, a.tole, c.college_name, co.course_name, b.branch_name FROM agreement AS a INNER JOIN 
                          student_registration AS sr ON a.register_id = sr.id INNER JOIN 
                          student_enquiry AS s ON sr.student_id = s.id INNER JOIN 
                          college AS c ON c.id = sr.college_id INNER JOIN 
                          course AS co ON co.id = sr.course_id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE a.is_deleted = '0' AND c.college_country = '$country_name' 
                          AND b.id = $user->branch_id ORDER BY a.id ASC "));

        }elseif ($country_name !== 'all' && $year !== 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT a.agreement_date, a.grand_father_name, s.student_first_name, s.student_middle_name, s.student_last_name, 
                          a.district, a.vdc_municipality, a.ward_no, a.tole, c.college_name, co.course_name, b.branch_name FROM agreement AS a INNER JOIN 
                          student_registration AS sr ON a.register_id = sr.id INNER JOIN 
                          student_enquiry AS s ON sr.student_id = s.id INNER JOIN 
                          college AS c ON c.id = sr.college_id INNER JOIN 
                          course AS co ON co.id = sr.course_id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE a.is_deleted = '0' AND c.college_country = '$country_name' 
                          AND Year(a.agreement_date) = '$year' AND b.id = $user->branch_id ORDER BY a.id ASC "));

        }elseif($country_name !== 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT a.agreement_date, a.grand_father_name, s.student_first_name, s.student_middle_name, s.student_last_name, 
                          a.district, a.vdc_municipality, a.ward_no, a.tole, c.college_name, co.course_name, b.branch_name FROM agreement AS a INNER JOIN 
                          student_registration AS sr ON a.register_id = sr.id INNER JOIN 
                          student_enquiry AS s ON sr.student_id = s.id INNER JOIN 
                          college AS c ON c.id = sr.college_id INNER JOIN 
                          course AS co ON co.id = sr.course_id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE a.is_deleted = '0' AND c.college_country = '$country_name' 
                          AND Year(a.agreement_date) = '$year' AND Month(a.agreement_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY a.id ASC "));

        }elseif($country_name === 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT a.agreement_date, a.grand_father_name, s.student_first_name, s.student_middle_name, s.student_last_name, 
                          a.district, a.vdc_municipality, a.ward_no, a.tole, c.college_name, co.course_name, b.branch_name FROM agreement AS a INNER JOIN 
                          student_registration AS sr ON a.register_id = sr.id INNER JOIN 
                          student_enquiry AS s ON sr.student_id = s.id INNER JOIN 
                          college AS c ON c.id = sr.college_id INNER JOIN 
                          course AS co ON co.id = sr.course_id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE a.is_deleted = '0' AND Month(a.agreement_date) = '$month'
                          AMD b.id = $user->branch_id ORDER BY a.id ASC "));

        }elseif($country_name === 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT a.agreement_date, a.grand_father_name, s.student_first_name, s.student_middle_name, s.student_last_name, 
                          a.district, a.vdc_municipality, a.ward_no, a.tole, c.college_name, co.course_name, b.branch_name FROM agreement AS a INNER JOIN 
                          student_registration AS sr ON a.register_id = sr.id INNER JOIN 
                          student_enquiry AS s ON sr.student_id = s.id INNER JOIN 
                          college AS c ON c.id = sr.college_id INNER JOIN 
                          course AS co ON co.id = sr.course_id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE a.is_deleted = '0'
                          AND Year(a.agreement_date) = '$year' AND Month(a.agreement_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY a.id ASC "));

        }elseif($country_name !== 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT a.agreement_date, a.grand_father_name, s.student_first_name, s.student_middle_name, s.student_last_name, 
                          a.district, a.vdc_municipality, a.ward_no, a.tole, c.college_name, co.course_name, b.branch_name FROM agreement AS a INNER JOIN 
                          student_registration AS sr ON a.register_id = sr.id INNER JOIN 
                          student_enquiry AS s ON sr.student_id = s.id INNER JOIN 
                          college AS c ON c.id = sr.college_id INNER JOIN 
                          course AS co ON co.id = sr.course_id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE a.is_deleted = '0' AND c.college_country = '$country_name' 
                          AND Month(a.agreement_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY a.id ASC "));

        }elseif($country_name === 'all' && $year !== 'all' && $month === 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT a.agreement_date, a.grand_father_name, s.student_first_name, s.student_middle_name, s.student_last_name, 
                          a.district, a.vdc_municipality, a.ward_no, a.tole, c.college_name, co.course_name, b.branch_name FROM agreement AS a INNER JOIN 
                          student_registration AS sr ON a.register_id = sr.id INNER JOIN 
                          student_enquiry AS s ON sr.student_id = s.id INNER JOIN 
                          college AS c ON c.id = sr.college_id INNER JOIN 
                          course AS co ON co.id = sr.course_id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE a.is_deleted = '0' AND Year(a.agreement_date) = '$year'
                          AND b.id = $user->branch_id ORDER BY a.id ASC "));

        }

        $pdf = PDF::loadView($this->view_path. '.agreementPdf', compact('data'));

        return $pdf->download('StudentAgreementListPDF.pdf');
    }

    public function exportAsExcel(Request $requests)
    {
        $user = Auth::user();
        $data = [];
        $data['college'] = College::select('id', 'college_country')->groupBy('college_country')->get();

        $country_name   = $requests->input('country');
        $year           = $requests->input('year');
        $month          = rtrim($requests->input('month'), '"');

        if (($country_name === 'all' && $year === 'all' && $month === 'all') || ($country_name === null && $year === null && $month == null)){

            $agreement = Agreement::join('student_registration', 'student_registration.id', '=', 'agreement.register_id')
                            ->join('student_enquiry', 'student_enquiry.id', '=', 'student_registration.student_id')
                            ->join('branch', 'branch.id', '=','student_registration.branch_id' )
                            ->join('college', 'college.id', '=', 'student_registration.college_id')
                            ->join('course', 'course.id', '=', 'student_registration.course_id')
                            ->join('users', 'users.id', '=', 'agreement.user_id')
                            ->select('agreement.id','agreement.agreement_date', 'student_enquiry.student_first_name', 'student_enquiry.student_middle_name',
                                'student_enquiry.student_last_name','student_registration.father_name', 'agreement.grand_father_name',
                                'college.college_name', 'course.course_name', 'branch.branch_name', 'users.fullname')
                            ->where('agreement.is_deleted', '0')
                            ->where('branch.id', '=', $user->branch_id)
                            ->orderBy('agreement.id', 'ASC')
                            ->get();

        }elseif ($country_name !== 'all' && $year === 'all' && $month === 'all'){

            $agreement = Agreement::join('student_registration', 'student_registration.id', '=', 'agreement.register_id')
                ->join('student_enquiry', 'student_enquiry.id', '=', 'student_registration.student_id')
                ->join('branch', 'branch.id', '=','student_registration.branch_id' )
                ->join('college', 'college.id', '=', 'student_registration.college_id')
                ->join('course', 'course.id', '=', 'student_registration.course_id')
                ->join('users', 'users.id', '=', 'agreement.user_id')
                ->select('agreement.id','agreement.agreement_date', 'student_enquiry.student_first_name', 'student_enquiry.student_middle_name',
                    'student_enquiry.student_last_name','student_registration.father_name', 'agreement.grand_father_name',
                    'college.college_name', 'course.course_name', 'branch.branch_name', 'users.fullname')
                ->where('college.college_country', $country_name)
                ->where('branch.id', '=', $user->branch_id)
                ->where('agreement.is_deleted', '0')
                ->orderBy('agreement.id', 'ASC')
                ->get();

        }elseif ($country_name !== 'all' && $year !== 'all' && $month === 'all'){

            $agreement = Agreement::join('student_registration', 'student_registration.id', '=', 'agreement.register_id')
                ->join('student_enquiry', 'student_enquiry.id', '=', 'student_registration.student_id')
                ->join('branch', 'branch.id', '=','student_registration.branch_id' )
                ->join('college', 'college.id', '=', 'student_registration.college_id')
                ->join('course', 'course.id', '=', 'student_registration.course_id')
                ->join('users', 'users.id', '=', 'agreement.user_id')
                ->select('agreement.id','agreement.agreement_date', 'student_enquiry.student_first_name', 'student_enquiry.student_middle_name',
                    'student_enquiry.student_last_name','student_registration.father_name', 'agreement.grand_father_name',
                    'college.college_name', 'course.course_name', 'branch.branch_name', 'users.fullname')
                ->where('college.college_country', $country_name)
                ->whereYear('agreement.agreement_date', '=', $year)
                ->where('branch.id', '=', $user->branch_id)
                ->where('agreement.is_deleted', '0')
                ->orderBy('agreement.id', 'ASC')
                ->get();

        }elseif($country_name !== 'all' && $year !== 'all' && $month !== 'all') {

            $agreement = Agreement::join('student_registration', 'student_registration.id', '=', 'agreement.register_id')
                ->join('student_enquiry', 'student_enquiry.id', '=', 'student_registration.student_id')
                ->join('branch', 'branch.id', '=','student_registration.branch_id' )
                ->join('college', 'college.id', '=', 'student_registration.college_id')
                ->join('course', 'course.id', '=', 'student_registration.course_id')
                ->join('users', 'users.id', '=', 'agreement.user_id')
                ->select('agreement.id','agreement.agreement_date', 'student_enquiry.student_first_name', 'student_enquiry.student_middle_name',
                    'student_enquiry.student_last_name','student_registration.father_name', 'agreement.grand_father_name',
                    'college.college_name', 'course.course_name', 'branch.branch_name', 'users.fullname')
                ->where('college.college_country', $country_name)
                ->whereYear('agreement.agreement_date', '=', $year)
                ->whereMonth('agreement.agreement_date', '=', $month)
                ->where('branch.id', '=', $user->branch_id)
                ->where('agreement.is_deleted', '0')
                ->orderBy('agreement.id', 'ASC')
                ->get();


        }elseif($country_name === 'all' && $year === 'all' && $month !== 'all') {

            $agreement = Agreement::join('student_registration', 'student_registration.id', '=', 'agreement.register_id')
                ->join('student_enquiry', 'student_enquiry.id', '=', 'student_registration.student_id')
                ->join('branch', 'branch.id', '=','student_registration.branch_id' )
                ->join('college', 'college.id', '=', 'student_registration.college_id')
                ->join('course', 'course.id', '=', 'student_registration.course_id')
                ->join('users', 'users.id', '=', 'agreement.user_id')
                ->select('agreement.id','agreement.agreement_date', 'student_enquiry.student_first_name', 'student_enquiry.student_middle_name',
                    'student_enquiry.student_last_name','student_registration.father_name', 'agreement.grand_father_name',
                    'college.college_name', 'course.course_name', 'branch.branch_name', 'users.fullname')
                ->whereMonth('agreement.agreement_date', '=', $month)
                ->where('branch.id', '=', $user->branch_id)
                ->where('agreement.is_deleted', '0')
                ->orderBy('agreement.id', 'ASC')
                ->get();

        }elseif($country_name === 'all' && $year !== 'all' && $month !== 'all') {

            $agreement = Agreement::join('student_registration', 'student_registration.id', '=', 'agreement.register_id')
                ->join('student_enquiry', 'student_enquiry.id', '=', 'student_registration.student_id')
                ->join('branch', 'branch.id', '=','student_registration.branch_id' )
                ->join('college', 'college.id', '=', 'student_registration.college_id')
                ->join('course', 'course.id', '=', 'student_registration.course_id')
                ->join('users', 'users.id', '=', 'agreement.user_id')
                ->select('agreement.id','agreement.agreement_date', 'student_enquiry.student_first_name', 'student_enquiry.student_middle_name',
                    'student_enquiry.student_last_name','student_registration.father_name', 'agreement.grand_father_name',
                    'college.college_name', 'course.course_name', 'branch.branch_name', 'users.fullname')
                ->whereYear('agreement.agreement_date', '=', $year)
                ->whereMonth('agreement.agreement_date', '=', $month)
                ->where('branch.id', '=', $user->branch_id)
                ->where('agreement.is_deleted', '0')
                ->orderBy('agreement.id', 'ASC')
                ->get();

        }elseif($country_name !== 'all' && $year === 'all' && $month !== 'all') {

            $agreement = Agreement::join('student_registration', 'student_registration.id', '=', 'agreement.register_id')
                ->join('student_enquiry', 'student_enquiry.id', '=', 'student_registration.student_id')
                ->join('branch', 'branch.id', '=','student_registration.branch_id' )
                ->join('college', 'college.id', '=', 'student_registration.college_id')
                ->join('course', 'course.id', '=', 'student_registration.course_id')
                ->join('users', 'users.id', '=', 'agreement.user_id')
                ->select('agreement.id','agreement.agreement_date', 'student_enquiry.student_first_name', 'student_enquiry.student_middle_name',
                    'student_enquiry.student_last_name','student_registration.father_name', 'agreement.grand_father_name',
                    'college.college_name', 'course.course_name', 'branch.branch_name', 'users.fullname')
                ->where('college.college_country', $country_name)
                ->whereMonth('agreement.agreement_date', '=', $month)
                ->where('branch.id', '=', $user->branch_id)
                ->where('agreement.is_deleted', '0')
                ->orderBy('agreement.id', 'ASC')
                ->get();

        }elseif($country_name === 'all' && $year !== 'all' && $month === 'all') {

            $agreement = Agreement::join('student_registration', 'student_registration.id', '=', 'agreement.register_id')
                ->join('student_enquiry', 'student_enquiry.id', '=', 'student_registration.student_id')
                ->join('branch', 'branch.id', '=','student_registration.branch_id' )
                ->join('college', 'college.id', '=', 'student_registration.college_id')
                ->join('course', 'course.id', '=', 'student_registration.course_id')
                ->join('users', 'users.id', '=', 'agreement.user_id')
                ->select('agreement.id','agreement.agreement_date', 'student_enquiry.student_first_name', 'student_enquiry.student_middle_name',
                    'student_enquiry.student_last_name','student_registration.father_name', 'agreement.grand_father_name',
                    'college.college_name', 'course.course_name', 'branch.branch_name', 'users.fullname')
                ->whereYear('agreement.agreement_date', '=', $year)
                ->where('branch.id', '=', $user->branch_id)
                ->where('agreement.is_deleted', '0')
                ->orderBy('agreement.id', 'ASC')
                ->get();

        }

        $agreementArray = [];

        // Define the Excel spreadsheet headers
        $agreementArray[] = ['S.N.','Agreement Date', 'First Name','Middle Name', 'Last Name', 'Father Name', 'Grand Father Name', 'College Name', 'Course Name', 'Branch Name', 'Created By',];

        // Convert each member of the returned collection into an array,
        // and append it to the agreement array.
        foreach ($agreement as $agreement) {
            $agreementArray[] = $agreement->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('Student Agreement Information', function($excel) use ($agreementArray) {

            // Build the spreadsheet, passing in the agreement array
            $excel->sheet('sheet1', function($sheet) use ($agreementArray) {
                $sheet->fromArray($agreementArray, null, 'A1', false, false);
            });

        })->export('xls');
    }

    public function agreementView()
    {
        $url = URL::previous();
        $array = explode('/', $url);
        $id = array_pop($array);

        $data = [];

        $data['row'] = DB::select( DB::raw(" 
                          SELECT a.*, s.student_first_name, s.student_middle_name, s.student_last_name, sr.father_name, 
                           sr.date_of_birth, co.course_name, c.college_name, c.college_country
                          FROM agreement AS a INNER JOIN 
                          student_registration AS sr ON a.register_id = sr.id INNER JOIN 
                          student_enquiry AS s ON sr.student_id = s.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id WHERE a.id = '$id' " ));
        
        return view(parent::loadDefaultVars($this->view_path . '.agreementView'), compact('data'));
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = Agreement::find($id);

        return $this->model;
    }
}
