<?php

namespace App\Http\Controllers\Normal;

use Auth;
use Gate;
use DB;
use App\Models\College;
use PDF;
use Carbon;
use Excel;
use AppHelper;
use App\Http\Requests;


class CollegeController extends NormalBaseController {

    protected $view_path  = 'normal.college';
    protected $base_route = 'normal.college';
    protected $model;
    
    public function index()
    {
        $data = [];
        $data['rows'] = DB::select( DB::raw(" SELECT * FROM college WHERE is_deleted = '0' ORDER BY id ASC "));

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function exportAsPrint()
    {
        $data = [];
        $data['rows'] = College::select('id', 'college_name', 'college_address', 'college_phone', 'college_country', 'remarks')->where('is_deleted', '0')->get();

        return view(parent::loadDefaultVars($this->view_path . '.collegePrint'), compact('data'));
    }

    public function exportAsPdf()
    {
        $data = [];
        $data['rows'] = College::select('id', 'college_name', 'college_address', 'college_phone', 'college_country', 'remarks')->where('is_deleted', '0')->get();

        $pdf = PDF::loadView($this->view_path. '.collegePdf', compact('data'));

        return $pdf->download('CollegePDF.pdf');
    }

    public function exportAsExcel()
    {
        $colleges = College::select('id', 'college_name', 'college_address', 'college_phone', 'college_country', 'remarks', 'created_at')->where('is_deleted', '0')->get();

        $collegeArray = [];

        // Define the Excel spreadsheet headers
        $collegeArray[] = ['S.N.', 'College Name', 'College Address', 'College Phone', 'College Country', 'Remarks', 'Created Date'];

        // Convert each member of the returned collection into an array,
        // and append it to the college array.
        foreach ($colleges as $college) {
            $collegeArray[] = $college->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('College Information', function($excel) use ($collegeArray) {

            // Build the spreadsheet, passing in the college array
            $excel->sheet('sheet1', function($sheet) use ($collegeArray) {
                $sheet->fromArray($collegeArray, null, 'A1', false, false);
            });

        })->export('xls');
    }
    

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = College::find($id);

        return $this->model;
    }
}
