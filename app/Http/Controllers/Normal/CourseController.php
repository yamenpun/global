<?php

namespace App\Http\Controllers\Normal;

use App\Models\Course;
use Auth;
use Gate;
use DB;
use PDF;
use Carbon;
use Excel;
use AppHelper;
use App\Http\Requests;


class CourseController extends NormalBaseController {

    protected $view_path  = 'normal.course';
    protected $base_route = 'normal.course';
    protected $model;
    
    public function index()
    {
        $data = [];
        $data['rows'] = DB::select( DB::raw(" 
                        SELECT * FROM course WHERE course.is_deleted = '0' ORDER BY id ASC "));

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function exportAsPrint()
    {
        $data = [];
        $data['rows'] = DB::table('course')
            ->select('course.*')
            ->where('course.is_deleted', '=', '0')
            ->get();

        return view(parent::loadDefaultVars($this->view_path . '.coursePrint'), compact('data'));
    }

    public function exportAsPdf()
    {
        $data = [];
        $data['rows'] = DB::table('course')
            ->select('course.*')
            ->where('course.is_deleted', '=', '0')
            ->get();

        $pdf = PDF::loadView($this->view_path. '.coursePdf', compact('data'));

        return $pdf->download('CoursePDF.pdf');
    }

    public function exportAsExcel()
    {
        $courses = Course::select('course.id', 'course.course_name', 'course.course_remarks', 'course.created_at')
            ->where('course.is_deleted', '1')
            ->orderBy('course.id', 'ASC')
            ->get();


        $courseArray = [];

        // Define the Excel spreadsheet headers
        $courseArray[] = ['S.N.', 'Course Name', 'Course Remarks', 'Created At'];

        // Convert each member of the returned collection into an array,
        // and append it to the course array.
        foreach ($courses as $course) {
            $courseArray[] = $course->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('Deleted Course Information', function($excel) use ($courseArray) {

            // Build the spreadsheet, passing in the course array
            $excel->sheet('sheet1', function($sheet) use ($courseArray) {
                $sheet->fromArray($courseArray, null, 'A1', false, false);
            });

        })->export('xls');
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = Course::find($id);

        return $this->model;
    }
}
