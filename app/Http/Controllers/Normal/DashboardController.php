<?php

namespace App\Http\Controllers\Normal;

use App\Http\Requests;

class DashboardController extends NormalBaseController
{
	protected $view_path = 'normal.dashboard';
    protected $base_route = 'normal.dashboard';

    public function index()
    {
        return view(parent::loadDefaultVars($this->view_path . '.index'));
    }
}
