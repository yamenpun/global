<?php

namespace App\Http\Controllers\Normal;

use App\Models\Branch;
use App\Models\College;
use App\Models\StudentRegistration;
use Auth;
use Gate;
use DB;
use PDF;
use Carbon;
use Excel;
use AppHelper;
use App\Http\Requests;
use Illuminate\Http\Request;


class RegisterController extends NormalBaseController {

    protected $view_path  = 'normal.register';
    protected $base_route = 'normal.register';
    protected $model;
    
    public function index(Request $requests)
    {
        $user = Auth::user();
        $data = [];

        $data['college_country'] = College::select('id', 'college_country')->groupBy('college_country')->get();

        $country_name = $requests->input('country');
        $year = $requests->input('year');
        $month = $requests->input('month');

        if (($country_name === 'all' && $year === 'all' && $month === 'all') || ($country_name === null && $year === null && $month == null)){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name, co.course_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif ($country_name !== 'all' && $year === 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name, co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND c.college_country = '$country_name'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif ($country_name !== 'all' && $year !== 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND c.college_country = '$country_name' AND Year(sr.register_date) = '$year'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif($country_name !== 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND c.college_country = '$country_name' 
                          AND Year(sr.register_date) = '$year' AND Month(sr.register_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif($country_name === 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND Month(sr.register_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif($country_name === 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' 
                          AND Year(sr.register_date) = '$year' AND Month(sr.register_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif($country_name !== 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND c.college_country = '$country_name' 
                          AND Month(sr.register_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif($country_name === 'all' && $year !== 'all' && $month === 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND Year(sr.register_date) = '$year'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function view($id)
    {
        $data = [];
        $data['row'] = DB::select( DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name, co.course_name, u.fullname, b.branch_name FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.id = '$id' " ));

        return view(parent::loadDefaultVars($this->view_path . '.view'), compact('data'));
    }

    public function exportAsPrint(Request $requests)
    {
        $user = Auth::user();
        $data = [];
        $data['branch'] = Branch::select('id', 'branch_name')->get();

        $country_name = $requests->input('country');
        $year = $requests->input('year');
        $month = rtrim($requests->input('month'), '"');

        if (($country_name === 'all' && $year === 'all' && $month === 'all') || ($country_name === null && $year === null && $month == null)){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif ($country_name !== 'all' && $year === 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND c.college_country = '$country_name' 
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif ($country_name !== 'all' && $year !== 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND c.college_country = '$country_name' 
                          AND Year(sr.register_date) = '$year' AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif($country_name !== 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND c.college_country = '$country_name' 
                          AND Year(sr.register_date) = '$year' AND Month(sr.register_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif($country_name === 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND Month(sr.register_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif($country_name === 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0'
                          AND Year(sr.register_date) = '$year' AND Month(sr.register_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif($country_name !== 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND c.college_country = '$country_name' 
                          AND Month(sr.register_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif($country_name === 'all' && $year !== 'all' && $month === 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND Year(sr.register_date) = '$year'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }

        return view(parent::loadDefaultVars($this->view_path . '.registerPrint'), compact('data'));
    }

    public function exportAsPdf(Request $requests)
    {
        $user = Auth::user();
        $data = [];
        $data['college_country'] = College::select('id', 'college_country')->groupBy('college_country')->get();

        $country_name = $requests->input('country');
        $year = $requests->input('year');
        $month = rtrim($requests->input('month'), '"');

        if (($country_name === 'all' && $year === 'all' && $month === 'all') || ($country_name === null && $year === null && $month == null)){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif ($country_name !== 'all' && $year === 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND c.college_country = '$country_name' 
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif ($country_name !== 'all' && $year !== 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND c.college_country = '$country_name' 
                          AND Year(sr.register_date) = '$year' AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif($country_name !== 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND c.college_country = '$country_name' 
                          AND Year(sr.register_date) = '$year' AND Month(sr.register_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif($country_name === 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND Month(sr.register_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif($country_name === 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0'
                          AND Year(sr.register_date) = '$year' AND Month(sr.register_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif($country_name !== 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND c.college_country = '$country_name' 
                          AND Month(sr.register_date) = '$month'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }elseif($country_name === 'all' && $year !== 'all' && $month === 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT sr.*, se.student_first_name, se.student_middle_name, se.student_last_name, c.college_name,co.course_name, u.fullname, b.branch_name 
                          FROM student_registration AS sr INNER JOIN 
                          student_enquiry AS se ON sr.student_id = se.id INNER JOIN 
                          college AS c ON sr.college_id = c.id INNER JOIN 
                          course AS co ON sr.course_id = co.id INNER JOIN 
                          users AS u ON sr.user_id = u.id INNER JOIN 
                          branch AS b ON sr.branch_id = b.id WHERE sr.is_deleted = '0' AND Year(sr.register_date) = '$year'
                          AND b.id = $user->branch_id ORDER BY sr.id ASC "));

        }

        $pdf = PDF::loadView($this->view_path. '.registerPdf', compact('data'));

        return $pdf->download('StudentRegistrationListPDF.pdf');
    }

    public function exportAsExcel(Request $requests)
    {
        $user = Auth::user();
        $data = [];
        $data['college_country'] = College::select('id', 'college_country')->groupBy('college_country')->get();

        $country_name = $requests->input('country');
        $year = $requests->input('year');
        $month = rtrim($requests->input('month'), '"');

        if (($country_name === 'all' && $year === 'all' && $month === 'all') || ($country_name === null && $year === null && $month == null)){

            $registration = StudentRegistration::join('users', 'users.id', '=', 'student_registration.user_id')
                            ->join('branch', 'branch.id', '=', 'student_registration.branch_id')
                            ->join('student_enquiry', 'student_registration.student_id', '=', 'student_enquiry.id')
                            ->join('college', 'college.id', '=', 'student_registration.college_id')
                            ->join('course', 'course.id', '=', 'student_registration.course_id')
                            ->select('student_registration.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                                'college.college_name', 'course.course_name', 'student_registration.intake',
                                'student_registration.date_of_birth', 'users.fullname', 'branch.branch_name')
                            ->where('student_registration.is_deleted', '0')
                            ->where('branch.id', '=', $user->branch_id)
                            ->orderBy('student_registration.id', 'ASC')
                            ->get();

        }elseif ($country_name !== 'all' && $year === 'all' && $month === 'all'){

            $registration = StudentRegistration::join('users', 'users.id', '=', 'student_registration.user_id')
                ->join('branch', 'branch.id', '=', 'student_registration.branch_id')
                ->join('student_enquiry', 'student_registration.student_id', '=', 'student_enquiry.id')
                ->join('college', 'college.id', '=', 'student_registration.college_id')
                ->join('course', 'course.id', '=', 'student_registration.course_id')
                ->select('student_registration.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'college.college_name', 'course.course_name', 'student_registration.intake',
                    'student_registration.date_of_birth', 'users.fullname', 'branch.branch_name')
                ->where('student_registration.is_deleted', '0')
                ->where('branch.branch_name', $country_name)
                ->where('branch.id', '=', $user->branch_id)
                ->orderBy('student_registration.id', 'ASC')
                ->get();

        }elseif ($country_name !== 'all' && $year !== 'all' && $month === 'all'){

            $registration = StudentRegistration::join('users', 'users.id', '=', 'student_registration.user_id')
                ->join('branch', 'branch.id', '=', 'student_registration.branch_id')
                ->join('student_enquiry', 'student_registration.student_id', '=', 'student_enquiry.id')
                ->join('college', 'college.id', '=', 'student_registration.college_id')
                ->join('course', 'course.id', '=', 'student_registration.course_id')
                ->select('student_registration.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'college.college_name', 'course.course_name', 'student_registration.intake',
                    'student_registration.date_of_birth', 'users.fullname', 'branch.branch_name')
                ->where('student_registration.is_deleted', '0')
                ->where('branch.branch_name', $country_name)
                ->where('branch.id', '=', $user->branch_id)
                ->whereYear('student_registration.register_date', '=', $year)
                ->orderBy('student_registration.id', 'ASC')
                ->get();

        }elseif($country_name !== 'all' && $year !== 'all' && $month !== 'all') {

            $registration = StudentRegistration::join('users', 'users.id', '=', 'student_registration.user_id')
                ->join('branch', 'branch.id', '=', 'student_registration.branch_id')
                ->join('student_enquiry', 'student_registration.student_id', '=', 'student_enquiry.id')
                ->join('college', 'college.id', '=', 'student_registration.college_id')
                ->join('course', 'course.id', '=', 'student_registration.course_id')
                ->select('student_registration.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'college.college_name', 'course.course_name', 'student_registration.intake',
                    'student_registration.date_of_birth', 'users.fullname', 'branch.branch_name')
                ->where('student_registration.is_deleted', '0')
                ->where('branch.branch_name', $country_name)
                ->where('branch.id', '=', $user->branch_id)
                ->whereYear('student_registration.register_date', '=', $year)
                ->whereMonth('student_registration.register_date', '=', $month)
                ->orderBy('student_registration.id', 'ASC')
                ->get();


        }elseif($country_name === 'all' && $year === 'all' && $month !== 'all') {

            $registration = StudentRegistration::join('users', 'users.id', '=', 'student_registration.user_id')
                ->join('branch', 'branch.id', '=', 'student_registration.branch_id')
                ->join('student_enquiry', 'student_registration.student_id', '=', 'student_enquiry.id')
                ->join('college', 'college.id', '=', 'student_registration.college_id')
                ->join('course', 'course.id', '=', 'student_registration.course_id')
                ->select('student_registration.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'college.college_name', 'course.course_name', 'student_registration.intake',
                    'student_registration.date_of_birth', 'users.fullname', 'branch.branch_name')
                ->where('student_registration.is_deleted', '0')
                ->where('branch.id', '=', $user->branch_id)
                ->whereMonth('student_registration.register_date', '=', $month)
                ->orderBy('student_registration.id', 'ASC')
                ->get();

        }elseif($country_name === 'all' && $year !== 'all' && $month !== 'all') {

            $registration = StudentRegistration::join('users', 'users.id', '=', 'student_registration.user_id')
                ->join('branch', 'branch.id', '=', 'student_registration.branch_id')
                ->join('student_enquiry', 'student_registration.student_id', '=', 'student_enquiry.id')
                ->join('college', 'college.id', '=', 'student_registration.college_id')
                ->join('course', 'course.id', '=', 'student_registration.course_id')
                ->select('student_registration.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'college.college_name', 'course.course_name', 'student_registration.intake',
                    'student_registration.date_of_birth', 'users.fullname', 'branch.branch_name')
                ->where('student_registration.is_deleted', '0')
                ->where('branch.id', '=', $user->branch_id)
                ->whereYear('student_registration.register_date', '=', $year)
                ->whereMonth('student_registration.register_date', '=', $month)
                ->orderBy('student_registration.id', 'ASC')
                ->get();

        }elseif($country_name !== 'all' && $year === 'all' && $month !== 'all') {

            $registration = StudentRegistration::join('users', 'users.id', '=', 'student_registration.user_id')
                ->join('branch', 'branch.id', '=', 'student_registration.branch_id')
                ->join('student_enquiry', 'student_registration.student_id', '=', 'student_enquiry.id')
                ->join('college', 'college.id', '=', 'student_registration.college_id')
                ->join('course', 'course.id', '=', 'student_registration.course_id')
                ->select('student_registration.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'college.college_name', 'course.course_name', 'student_registration.intake',
                    'student_registration.date_of_birth', 'users.fullname', 'branch.branch_name')
                ->where('student_registration.is_deleted', '0')
                ->where('branch.id', '=', $user->branch_id)
                ->where('branch.branch_name', $country_name)
                ->whereMonth('student_registration.register_date', '=', $month)
                ->orderBy('student_registration.id', 'ASC')
                ->get();

        }elseif($country_name === 'all' && $year !== 'all' && $month === 'all') {

            $registration = StudentRegistration::join('users', 'users.id', '=', 'student_registration.user_id')
                ->join('branch', 'branch.id', '=', 'student_registration.branch_id')
                ->join('student_enquiry', 'student_registration.student_id', '=', 'student_enquiry.id')
                ->join('college', 'college.id', '=', 'student_registration.college_id')
                ->join('course', 'course.id', '=', 'student_registration.course_id')
                ->select('student_registration.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'college.college_name', 'course.course_name', 'student_registration.intake',
                    'student_registration.date_of_birth', 'users.fullname', 'branch.branch_name')
                ->where('student_registration.is_deleted', '0')
                ->where('branch.id', '=', $user->branch_id)
                ->whereYear('student_registration.register_date', '=', $year)
                ->orderBy('student_registration.id', 'ASC')
                ->get();

        }

        $registrationArray = [];

        // Define the Excel spreadsheet headers
        $registrationArray[] = ['S.N.', 'First Name', 'Last Name', 'College Name', 'Course Name', 'Intake', 'Date of Birth', 'Registered By', 'Branch Name'];

        // Convert each member of the returned collection into an array,
        // and append it to the register array.
        foreach ($registration as $registration) {
            $registrationArray[] = $registration->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('Student Registration Information', function($excel) use ($registrationArray) {

            // Build the spreadsheet, passing in the register array
            $excel->sheet('sheet1', function($sheet) use ($registrationArray) {
                $sheet->fromArray($registrationArray, null, 'A1', false, false);
            });

        })->export('xls');
    }
    

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = StudentRegistration::find($id);

        return $this->model;
    }
}
