<?php

namespace App\Http\Controllers\Normal;

use Auth;
use Gate;
use App\User;
use AppHelper;
use App\Http\Requests;


class UserController extends NormalBaseController {

    protected $view_path  = 'normal.user';
    protected $base_route = 'normal.user';
    protected $model;

    public function index()
    {
        $data = [];
        $data['rows'] = User::select('id', 'fullname', 'username', 'email', 'role', 'created_at', 'updated_at', 'status')->get();

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }
    public function profile()
    {
        $data = [];
        $data['rows'] = Auth::user();

        return view(parent::loadDefaultVars($this->view_path . '.profile'), compact('data'));
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = User::find($id);

        return $this->model;
    }
}
