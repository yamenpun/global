<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Requests\Superadmin\Log\AddFormValidation;
use App\Models\ActivityLog;
use Auth;
use Gate;
use DB;
use AppHelper;
use App\Http\Requests;


class ActivityLogController extends SuperadminBaseController {

    protected $view_path  = 'superadmin.log';
    protected $base_route = 'superadmin.log';
    protected $model;

    public function index()
    {
        $data = [];
        $data['rows'] = DB::table('activity_log')
            ->select('activity_log.*', 'users.fullname')
            ->join('users', 'activity_log.user_id', '=', 'users.id')
            ->orderBy('activity_log.id', 'DESC')
            ->get();

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function deleteForm()
    {
        return view(parent::loadDefaultVars($this->view_path . '.deleteForm'));
    }

    public function delete(AddFormValidation $request)
    {
        $from_date = date('Y-m-d', strtotime($request->get('from-date')));
        $to_date   = date('Y-m-d', strtotime($request->get('to-date')));

        $affected = DB::delete("DELETE FROM activity_log WHERE created_at BETWEEN '$from_date' AND '$to_date'");

        if($affected > 0){
            AppHelper::flash('success', 'Record deleted Successfully.');
        }else{
            AppHelper::flash('danger', 'No Record Found To Delete.');
        }

        return redirect()->route($this->base_route.'.list');

    }

    public function deletePermanent($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route.'.list');
        }

        ActivityLog::destroy($id);

        AppHelper::flash('success', 'Record permanently deleted from database.');

        return redirect()->route($this->base_route.'.list');
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = ActivityLog::find($id);

        return $this->model;
    }
}
