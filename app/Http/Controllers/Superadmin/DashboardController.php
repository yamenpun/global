<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Requests;

class DashboardController extends SuperadminBaseController
{
	protected $view_path = 'superadmin.dashboard';
    protected $base_route = 'superadmin.dashboard';

    public function index()
    {
        return view(parent::loadDefaultVars($this->view_path . '.index'));
    }
}
