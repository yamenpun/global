<?php

namespace App\Http\Controllers\Superadmin;

use App\Models\Branch;
use Auth;
use Gate;
use DB;
use App\Models\StudentEnquiry;
use PDF;
use Carbon;
use Excel;
use AppHelper;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\Superadmin\Enquiry\AddFormValidation;
use App\Http\Requests\Superadmin\Enquiry\UpdateFormValidation;
use App\Http\Requests\Superadmin\Enquiry\AddCounselorFeedbackValidation;
use App\Http\Requests\Superadmin\Enquiry\UpdateCounselorFeedbackValidation;


class EnquiryController extends SuperadminBaseController {

    protected $view_path  = 'superadmin.enquiry';
    protected $base_route = 'superadmin.enquiry';
    protected $model;
    
    public function index(Request $requests)
    {
        $data = [];
        $data['branch'] = Branch::select('id', 'branch_name')->get();

        $branch_name = $requests->input('branch');
        $year = $requests->input('year');
        $month = $requests->input('month');

        if (($branch_name === 'all' && $year === 'all' && $month === 'all') || ($branch_name === null && $year === null && $month == null)){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id WHERE s.is_deleted = '0' ORDER BY id ASC "));

        }elseif ($branch_name !== 'all' && $year === 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND b.branch_name = '$branch_name' ORDER BY id ASC "));

        }elseif ($branch_name !== 'all' && $year !== 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND b.branch_name = '$branch_name' 
                          AND Year(s.enquiry_date) = '$year' ORDER BY id ASC "));

        }elseif($branch_name !== 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND b.branch_name = '$branch_name' 
                          AND Year(s.enquiry_date) = '$year' AND Month(s.enquiry_date) = '$month' ORDER BY id ASC "));

        }elseif($branch_name === 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND Month(s.enquiry_date) = '$month' ORDER BY id ASC "));

        }elseif($branch_name === 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND Year(s.enquiry_date) = '$year' 
                          AND Month(s.enquiry_date) = '$month' ORDER BY id ASC "));

        }elseif($branch_name !== 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND b.branch_name = '$branch_name' 
                          AND Month(s.enquiry_date) = '$month' ORDER BY id ASC "));

        }elseif($branch_name === 'all' && $year !== 'all' && $month === 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND Year(s.enquiry_date) = '$year' ORDER BY id ASC "));

        }

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function isDeletedList(Request $requests)
    {
        $data = [];
        $data['branch'] = Branch::select('id', 'branch_name')->get();

        $branch_name = $requests->input('branch');
        $year = $requests->input('year');
        $month = $requests->input('month');

        if (($branch_name === 'all' && $year === 'all' && $month === 'all') || ($branch_name === null && $year === null && $month == null)){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id WHERE s.is_deleted = '1' ORDER BY id ASC "));

        }elseif ($branch_name !== 'all' && $year === 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '1' AND b.branch_name = '$branch_name' ORDER BY id ASC "));

        }elseif ($branch_name !== 'all' && $year !== 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '1' AND b.branch_name = '$branch_name' 
                          AND Year(s.enquiry_date) = '$year' ORDER BY id ASC "));

        }elseif($branch_name !== 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '1' AND b.branch_name = '$branch_name' 
                          AND Year(s.enquiry_date) = '$year' AND Month(s.enquiry_date) = '$month' ORDER BY id ASC "));

        }elseif($branch_name === 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '1' AND Month(s.enquiry_date) = '$month' ORDER BY id ASC "));

        }elseif($branch_name === 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '1' AND Year(s.enquiry_date) = '$year' 
                          AND Month(s.enquiry_date) = '$month' ORDER BY id ASC "));

        }elseif($branch_name !== 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '1' AND b.branch_name = '$branch_name' 
                          AND Month(s.enquiry_date) = '$month' ORDER BY id ASC "));

        }elseif($branch_name === 'all' && $year !== 'all' && $month === 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '1' AND Year(s.enquiry_date) = '$year' ORDER BY id ASC "));

        }

        return view(parent::loadDefaultVars($this->view_path . '.deletedEnquiryList'), compact('data'));
    }

    public function view($id)
    {
        $data = [];

        $data['row'] = DB::select( DB::raw(" SELECT s.*, b.branch_name, u.fullname FROM student_enquiry AS s 
                                            INNER JOIN branch AS b ON s.branch_id = b.id
                                            INNER JOIN users AS u ON s.user_id = u.id WHERE s.id = '$id' " ));

        return view(parent::loadDefaultVars($this->view_path . '.view'), compact('data'));
    }

    public function deletedView($id)
    {
        $data = [];

        $data['row'] = DB::select( DB::raw(" SELECT s.*, b.branch_name, u.fullname FROM student_enquiry AS s 
                                            INNER JOIN branch AS b ON s.branch_id = b.id
                                            INNER JOIN users AS u ON s.user_id = u.id WHERE s.id = '$id' " ));

        return view(parent::loadDefaultVars($this->view_path . '.viewDeleted'), compact('data'));
    }

    public function exportAsPrint(Request $requests)
    {
        $data = [];
        $data['branch'] = Branch::select('id', 'branch_name')->get();

        $branch_name = $requests->input('branch');
        $year = $requests->input('year');
        $month = rtrim($requests->input('month'), '"');

        if (($branch_name === 'all' && $year === 'all' && $month === 'all') || ($branch_name === null && $year === null && $month == null)){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id WHERE s.is_deleted = '0' ORDER BY id ASC "));

        }elseif ($branch_name !== 'all' && $year === 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND b.branch_name = '$branch_name' ORDER BY id ASC "));

        }elseif ($branch_name !== 'all' && $year !== 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND b.branch_name = '$branch_name' 
                          AND Year(s.enquiry_date) = '$year' ORDER BY id ASC "));

        }elseif($branch_name !== 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND b.branch_name = '$branch_name' 
                          AND Year(s.enquiry_date) = '$year' AND Month(s.enquiry_date) = '$month' ORDER BY id ASC "));

        }elseif($branch_name === 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND Month(s.enquiry_date) = '$month' ORDER BY id ASC "));

        }elseif($branch_name === 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND Year(s.enquiry_date) = '$year' 
                          AND Month(s.enquiry_date) = '$month' ORDER BY id ASC "));

        }elseif($branch_name !== 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND b.branch_name = '$branch_name' 
                          AND Month(s.enquiry_date) = '$month' ORDER BY id ASC "));

        }elseif($branch_name === 'all' && $year !== 'all' && $month === 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND Year(s.enquiry_date) = '$year' ORDER BY id ASC "));

        }

        return view(parent::loadDefaultVars($this->view_path . '.enquiryPrint'), compact('data'));
    }

    public function exportAsPdf(Request $requests)
    {
        $data = [];
        $data['branch'] = Branch::select('id', 'branch_name')->get();

        $branch_name = $requests->input('branch');
        $year = $requests->input('year');
        $month = rtrim($requests->input('month'), '"');

        if (($branch_name === 'all' && $year === 'all' && $month === 'all') || ($branch_name === null && $year === null && $month == null)){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id WHERE s.is_deleted = '0' ORDER BY id ASC "));

        }elseif ($branch_name !== 'all' && $year === 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND b.branch_name = '$branch_name' ORDER BY id ASC "));

        }elseif ($branch_name !== 'all' && $year !== 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND b.branch_name = '$branch_name' 
                          AND Year(s.enquiry_date) = '$year' ORDER BY id ASC "));

        }elseif($branch_name !== 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND b.branch_name = '$branch_name' 
                          AND Year(s.enquiry_date) = '$year' AND Month(s.enquiry_date) = '$month' ORDER BY id ASC "));

        }elseif($branch_name === 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND Month(s.enquiry_date) = '$month' ORDER BY id ASC "));

        }elseif($branch_name === 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND Year(s.enquiry_date) = '$year' 
                          AND Month(s.enquiry_date) = '$month' ORDER BY id ASC "));

        }elseif($branch_name !== 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND b.branch_name = '$branch_name' 
                          AND Month(s.enquiry_date) = '$month' ORDER BY id ASC "));

        }elseif($branch_name === 'all' && $year !== 'all' && $month === 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '0' AND Year(s.enquiry_date) = '$year' ORDER BY id ASC "));

        }

        $pdf = PDF::loadView($this->view_path. '.enquiryPdf', compact('data'));

        return $pdf->download('StudentEnquiryListPDF.pdf');
    }

    public function exportAsExcel(Request $requests)
    {
        $data = [];
        $data['branch'] = Branch::select('id', 'branch_name')->get();

        $branch_name = $requests->input('branch');
        $year = $requests->input('year');
        $month = rtrim($requests->input('month'), '"');

        if (($branch_name === 'all' && $year === 'all' && $month === 'all') || ($branch_name === null && $year === null && $month == null)){

            $students = StudentEnquiry::join('users', 'users.id', '=', 'student_enquiry.user_id')
                            ->join('branch', 'branch.id', '=', 'student_enquiry.branch_id')
                            ->select('student_enquiry.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                                'student_enquiry.email', 'student_enquiry.mobile_phone', 'student_enquiry.year_of_completion_slc', 'student_enquiry.slc_percentage',
                                'student_enquiry.year_of_completion_plus_two', 'student_enquiry.plus_two_percentage', 'branch.branch_name')
                            ->where('student_enquiry.is_deleted', '0')
                            ->orderBy('student_enquiry.id', 'ASC')
                            ->get();

        }elseif ($branch_name !== 'all' && $year === 'all' && $month === 'all'){

            $students = StudentEnquiry::join('users', 'users.id', '=', 'student_enquiry.user_id')
                ->join('branch', 'branch.id', '=', 'student_enquiry.branch_id')
                ->select('student_enquiry.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'student_enquiry.email', 'student_enquiry.mobile_phone', 'student_enquiry.year_of_completion_slc', 'student_enquiry.slc_percentage',
                    'student_enquiry.year_of_completion_plus_two', 'student_enquiry.plus_two_percentage', 'branch.branch_name')
                ->where('student_enquiry.is_deleted', '0')
                ->where('branch.branch_name', $branch_name)
                ->orderBy('student_enquiry.id', 'ASC')
                ->get();

        }elseif ($branch_name !== 'all' && $year !== 'all' && $month === 'all'){

            $students = StudentEnquiry::join('users', 'users.id', '=', 'student_enquiry.user_id')
                ->join('branch', 'branch.id', '=', 'student_enquiry.branch_id')
                ->select('student_enquiry.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'student_enquiry.email', 'student_enquiry.mobile_phone', 'student_enquiry.year_of_completion_slc', 'student_enquiry.slc_percentage',
                    'student_enquiry.year_of_completion_plus_two', 'student_enquiry.plus_two_percentage', 'branch.branch_name')
                ->where('student_enquiry.is_deleted', '0')
                ->where('branch.branch_name', $branch_name)
                ->whereYear('student_enquiry.enquiry_date', '=', $year)
                ->orderBy('student_enquiry.id', 'ASC')
                ->get();

        }elseif($branch_name !== 'all' && $year !== 'all' && $month !== 'all') {

            $students = StudentEnquiry::join('users', 'users.id', '=', 'student_enquiry.user_id')
                ->join('branch', 'branch.id', '=', 'student_enquiry.branch_id')
                ->select('student_enquiry.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'student_enquiry.email', 'student_enquiry.mobile_phone', 'student_enquiry.year_of_completion_slc', 'student_enquiry.slc_percentage',
                    'student_enquiry.year_of_completion_plus_two', 'student_enquiry.plus_two_percentage', 'branch.branch_name')
                ->where('student_enquiry.is_deleted', '0')
                ->where('branch.branch_name', $branch_name)
                ->whereYear('student_enquiry.enquiry_date', '=', $year)
                ->whereMonth('student_enquiry.enquiry_date', '=', $month)
                ->orderBy('student_enquiry.id', 'ASC')
                ->get();

        }elseif($branch_name === 'all' && $year === 'all' && $month !== 'all') {

            $students = StudentEnquiry::join('users', 'users.id', '=', 'student_enquiry.user_id')
                ->join('branch', 'branch.id', '=', 'student_enquiry.branch_id')
                ->select('student_enquiry.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'student_enquiry.email', 'student_enquiry.mobile_phone', 'student_enquiry.year_of_completion_slc', 'student_enquiry.slc_percentage',
                    'student_enquiry.year_of_completion_plus_two', 'student_enquiry.plus_two_percentage', 'branch.branch_name')
                ->where('student_enquiry.is_deleted', '0')
                ->whereMonth('student_enquiry.enquiry_date', '=', $month)
                ->orderBy('student_enquiry.id', 'ASC')
                ->get();

        }elseif($branch_name === 'all' && $year !== 'all' && $month !== 'all') {

            $students = StudentEnquiry::join('users', 'users.id', '=', 'student_enquiry.user_id')
                ->join('branch', 'branch.id', '=', 'student_enquiry.branch_id')
                ->select('student_enquiry.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'student_enquiry.email', 'student_enquiry.mobile_phone', 'student_enquiry.year_of_completion_slc', 'student_enquiry.slc_percentage',
                    'student_enquiry.year_of_completion_plus_two', 'student_enquiry.plus_two_percentage', 'branch.branch_name')
                ->where('student_enquiry.is_deleted', '0')
                ->whereYear('student_enquiry.enquiry_date', '=', $year)
                ->whereMonth('student_enquiry.enquiry_date', '=', $month)
                ->orderBy('student_enquiry.id', 'ASC')
                ->get();

        }elseif($branch_name !== 'all' && $year === 'all' && $month !== 'all') {

            $students = StudentEnquiry::join('users', 'users.id', '=', 'student_enquiry.user_id')
                ->join('branch', 'branch.id', '=', 'student_enquiry.branch_id')
                ->select('student_enquiry.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'student_enquiry.email', 'student_enquiry.mobile_phone', 'student_enquiry.year_of_completion_slc', 'student_enquiry.slc_percentage',
                    'student_enquiry.year_of_completion_plus_two', 'student_enquiry.plus_two_percentage', 'branch.branch_name')
                ->where('student_enquiry.is_deleted', '0')
                ->where('branch.branch_name', $branch_name)
                ->whereMonth('student_enquiry.enquiry_date', '=', $month)
                ->orderBy('student_enquiry.id', 'ASC')
                ->get();

        }elseif($branch_name === 'all' && $year !== 'all' && $month === 'all') {

            $students = StudentEnquiry::join('users', 'users.id', '=', 'student_enquiry.user_id')
                ->join('branch', 'branch.id', '=', 'student_enquiry.branch_id')
                ->select('student_enquiry.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'student_enquiry.email', 'student_enquiry.mobile_phone', 'student_enquiry.year_of_completion_slc', 'student_enquiry.slc_percentage',
                    'student_enquiry.year_of_completion_plus_two', 'student_enquiry.plus_two_percentage', 'branch.branch_name')
                ->where('student_enquiry.is_deleted', '0')
                ->whereYear('student_enquiry.enquiry_date', '=', $year)
                ->orderBy('student_enquiry.id', 'ASC')
                ->get();

        }

        $studentArray = [];

        // Define the Excel spreadsheet headers
        $studentArray[] = ['S.N.', 'First Name', 'Last Name', 'Email', 'Mobile Number', 'SLC Year', 'SLC Percentage', '+2 Year', '+2 Percentage', 'Branch Name'];

        // Convert each member of the returned collection into an array,
        // and append it to the student array.
        foreach ($students as $student) {
            $studentArray[] = $student->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('Student Enquiry Information', function($excel) use ($studentArray) {

            // Build the spreadsheet, passing in the student array
            $excel->sheet('sheet1', function($sheet) use ($studentArray) {
                $sheet->fromArray($studentArray, null, 'A1', false, false);
            });

        })->export('xls');
    }

    public function deletedExportAsPrint(Request $requests)
    {
        $data = [];
        $data['branch'] = Branch::select('id', 'branch_name')->get();

        $branch_name = $requests->input('branch');
        $year = $requests->input('year');
        $month = rtrim($requests->input('month'), '"');

        if (($branch_name === 'all' && $year === 'all' && $month === 'all') || ($branch_name === null && $year === null && $month == null)){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id WHERE s.is_deleted = '1' ORDER BY id ASC "));

        }elseif ($branch_name !== 'all' && $year === 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '1' AND b.branch_name = '$branch_name' ORDER BY id ASC "));

        }elseif ($branch_name !== 'all' && $year !== 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '1' AND b.branch_name = '$branch_name' 
                          AND Year(s.enquiry_date) = '$year' ORDER BY id ASC "));

        }elseif($branch_name !== 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '1' AND b.branch_name = '$branch_name' 
                          AND Year(s.enquiry_date) = '$year' AND Month(s.enquiry_date) = '$month' ORDER BY id ASC "));

        }elseif($branch_name === 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '1' AND Month(s.enquiry_date) = '$month' ORDER BY id ASC "));

        }elseif($branch_name === 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '1' AND Year(s.enquiry_date) = '$year' 
                          AND Month(s.enquiry_date) = '$month' ORDER BY id ASC "));

        }elseif($branch_name !== 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '1' AND b.branch_name = '$branch_name' 
                          AND Month(s.enquiry_date) = '$month' ORDER BY id ASC "));

        }elseif($branch_name === 'all' && $year !== 'all' && $month === 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '1' AND Year(s.enquiry_date) = '$year' ORDER BY id ASC "));

        }

        return view(parent::loadDefaultVars($this->view_path . '.deletedEnquiryPrint'), compact('data'));
    }

    public function deletedExportAsPdf(Request $requests)
    {
        $data = [];
        $data['branch'] = Branch::select('id', 'branch_name')->get();

        $branch_name = $requests->input('branch');
        $year = $requests->input('year');
        $month = rtrim($requests->input('month'), '"');

        if (($branch_name === 'all' && $year === 'all' && $month === 'all') || ($branch_name === null && $year === null && $month == null)){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id WHERE s.is_deleted = '1' ORDER BY id ASC "));

        }elseif ($branch_name !== 'all' && $year === 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '1' AND b.branch_name = '$branch_name' ORDER BY id ASC "));

        }elseif ($branch_name !== 'all' && $year !== 'all' && $month === 'all'){

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '1' AND b.branch_name = '$branch_name' 
                          AND Year(s.enquiry_date) = '$year' ORDER BY id ASC "));

        }elseif($branch_name !== 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '1' AND b.branch_name = '$branch_name' 
                          AND Year(s.enquiry_date) = '$year' AND Month(s.enquiry_date) = '$month' ORDER BY id ASC "));

        }elseif($branch_name === 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '1' AND Month(s.enquiry_date) = '$month' ORDER BY id ASC "));

        }elseif($branch_name === 'all' && $year !== 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '1' AND Year(s.enquiry_date) = '$year' 
                          AND Month(s.enquiry_date) = '$month' ORDER BY id ASC "));

        }elseif($branch_name !== 'all' && $year === 'all' && $month !== 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '1' AND b.branch_name = '$branch_name' 
                          AND Month(s.enquiry_date) = '$month' ORDER BY id ASC "));

        }elseif($branch_name === 'all' && $year !== 'all' && $month === 'all') {

            $data['rows'] = DB::select(DB::raw(" 
                          SELECT s.*, u.fullname, b.branch_name FROM student_enquiry AS s INNER  JOIN 
                          users AS u ON s.user_id = u.id INNER  JOIN 
                          branch AS b ON s.branch_id = b.id 
                          WHERE s.is_deleted = '1' AND Year(s.enquiry_date) = '$year' ORDER BY id ASC "));

        }

        $pdf = PDF::loadView($this->view_path. '.deletedEnquiryPdf', compact('data'));

        return $pdf->download('DeletedStudentEnquiryListPDF.pdf');
    }

    public function deletedExportAsExcel(Request $requests)
    {
        $data = [];
        $data['branch'] = Branch::select('id', 'branch_name')->get();

        $branch_name = $requests->input('branch');
        $year = $requests->input('year');
        $month = rtrim($requests->input('month'), '"');

        if (($branch_name === 'all' && $year === 'all' && $month === 'all') || ($branch_name === null && $year === null && $month == null)){

            $students = StudentEnquiry::join('users', 'users.id', '=', 'student_enquiry.user_id')
                ->join('branch', 'branch.id', '=', 'student_enquiry.branch_id')
                ->select('student_enquiry.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'student_enquiry.email', 'student_enquiry.mobile_phone', 'student_enquiry.year_of_completion_slc', 'student_enquiry.slc_percentage',
                    'student_enquiry.year_of_completion_plus_two', 'student_enquiry.plus_two_percentage', 'branch.branch_name')
                ->where('student_enquiry.is_deleted', '1')
                ->orderBy('student_enquiry.id', 'ASC')
                ->get();

        }elseif ($branch_name !== 'all' && $year === 'all' && $month === 'all'){

            $students = StudentEnquiry::join('users', 'users.id', '=', 'student_enquiry.user_id')
                ->join('branch', 'branch.id', '=', 'student_enquiry.branch_id')
                ->select('student_enquiry.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'student_enquiry.email', 'student_enquiry.mobile_phone', 'student_enquiry.year_of_completion_slc', 'student_enquiry.slc_percentage',
                    'student_enquiry.year_of_completion_plus_two', 'student_enquiry.plus_two_percentage', 'branch.branch_name')
                ->where('student_enquiry.is_deleted', '1')
                ->where('branch.branch_name', $branch_name)
                ->orderBy('student_enquiry.id', 'ASC')
                ->get();

        }elseif ($branch_name !== 'all' && $year !== 'all' && $month === 'all'){

            $students = StudentEnquiry::join('users', 'users.id', '=', 'student_enquiry.user_id')
                ->join('branch', 'branch.id', '=', 'student_enquiry.branch_id')
                ->select('student_enquiry.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'student_enquiry.email', 'student_enquiry.mobile_phone', 'student_enquiry.year_of_completion_slc', 'student_enquiry.slc_percentage',
                    'student_enquiry.year_of_completion_plus_two', 'student_enquiry.plus_two_percentage', 'branch.branch_name')
                ->where('student_enquiry.is_deleted', '1')
                ->where('branch.branch_name', $branch_name)
                ->whereYear('student_enquiry.enquiry_date', '=', $year)
                ->orderBy('student_enquiry.id', 'ASC')
                ->get();

        }elseif($branch_name !== 'all' && $year !== 'all' && $month !== 'all') {

            $students = StudentEnquiry::join('users', 'users.id', '=', 'student_enquiry.user_id')
                ->join('branch', 'branch.id', '=', 'student_enquiry.branch_id')
                ->select('student_enquiry.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'student_enquiry.email', 'student_enquiry.mobile_phone', 'student_enquiry.year_of_completion_slc', 'student_enquiry.slc_percentage',
                    'student_enquiry.year_of_completion_plus_two', 'student_enquiry.plus_two_percentage', 'branch.branch_name')
                ->where('student_enquiry.is_deleted', '1')
                ->where('branch.branch_name', $branch_name)
                ->whereYear('student_enquiry.enquiry_date', '=', $year)
                ->whereMonth('student_enquiry.enquiry_date', '=', $month)
                ->orderBy('student_enquiry.id', 'ASC')
                ->get();

        }elseif($branch_name === 'all' && $year === 'all' && $month !== 'all') {

            $students = StudentEnquiry::join('users', 'users.id', '=', 'student_enquiry.user_id')
                ->join('branch', 'branch.id', '=', 'student_enquiry.branch_id')
                ->select('student_enquiry.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'student_enquiry.email', 'student_enquiry.mobile_phone', 'student_enquiry.year_of_completion_slc', 'student_enquiry.slc_percentage',
                    'student_enquiry.year_of_completion_plus_two', 'student_enquiry.plus_two_percentage', 'branch.branch_name')
                ->where('student_enquiry.is_deleted', '1')
                ->whereMonth('student_enquiry.enquiry_date', '=', $month)
                ->orderBy('student_enquiry.id', 'ASC')
                ->get();

        }elseif($branch_name === 'all' && $year !== 'all' && $month !== 'all') {

            $students = StudentEnquiry::join('users', 'users.id', '=', 'student_enquiry.user_id')
                ->join('branch', 'branch.id', '=', 'student_enquiry.branch_id')
                ->select('student_enquiry.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'student_enquiry.email', 'student_enquiry.mobile_phone', 'student_enquiry.year_of_completion_slc', 'student_enquiry.slc_percentage',
                    'student_enquiry.year_of_completion_plus_two', 'student_enquiry.plus_two_percentage', 'branch.branch_name')
                ->where('student_enquiry.is_deleted', '1')
                ->whereYear('student_enquiry.enquiry_date', '=', $year)
                ->whereMonth('student_enquiry.enquiry_date', '=', $month)
                ->orderBy('student_enquiry.id', 'ASC')
                ->get();

        }elseif($branch_name !== 'all' && $year === 'all' && $month !== 'all') {

            $students = StudentEnquiry::join('users', 'users.id', '=', 'student_enquiry.user_id')
                ->join('branch', 'branch.id', '=', 'student_enquiry.branch_id')
                ->select('student_enquiry.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'student_enquiry.email', 'student_enquiry.mobile_phone', 'student_enquiry.year_of_completion_slc', 'student_enquiry.slc_percentage',
                    'student_enquiry.year_of_completion_plus_two', 'student_enquiry.plus_two_percentage', 'branch.branch_name')
                ->where('student_enquiry.is_deleted', '1')
                ->where('branch.branch_name', $branch_name)
                ->whereMonth('student_enquiry.enquiry_date', '=', $month)
                ->orderBy('student_enquiry.id', 'ASC')
                ->get();

        }elseif($branch_name === 'all' && $year !== 'all' && $month === 'all') {

            $students = StudentEnquiry::join('users', 'users.id', '=', 'student_enquiry.user_id')
                ->join('branch', 'branch.id', '=', 'student_enquiry.branch_id')
                ->select('student_enquiry.id','student_enquiry.student_first_name', 'student_enquiry.student_last_name',
                    'student_enquiry.email', 'student_enquiry.mobile_phone', 'student_enquiry.year_of_completion_slc', 'student_enquiry.slc_percentage',
                    'student_enquiry.year_of_completion_plus_two', 'student_enquiry.plus_two_percentage', 'branch.branch_name')
                ->where('student_enquiry.is_deleted', '1')
                ->whereYear('student_enquiry.enquiry_date', '=', $year)
                ->orderBy('student_enquiry.id', 'ASC')
                ->get();

        }

        $studentArray = [];

        // Define the Excel spreadsheet headers
        $studentArray[] = ['S.N.', 'First Name', 'Last Name', 'Email', 'Mobile Number', 'SLC Year', 'SLC Percentage', '+2 Year', '+2 Percentage', 'Branch Name'];

        // Convert each member of the returned collection into an array,
        // and append it to the student array.
        foreach ($students as $student) {
            $studentArray[] = $student->toArray();
        }

        // Generate and return the spreadsheet
        Excel::create('Deleted Student Enquiry Information', function($excel) use ($studentArray) {

            // Build the spreadsheet, passing in the student array
            $excel->sheet('sheet1', function($sheet) use ($studentArray) {
                $sheet->fromArray($studentArray, null, 'A1', false, false);
            });

        })->export('xls');
    }

    public function create()
    {
        $data = [];
        $data['branch']  = Branch::select('id', 'branch_name')->get();

        return view(parent::loadDefaultVars($this->view_path . '.create'), compact('data'));
    }

    public function store(AddFormValidation $request)
    {
        $user = Auth::user();

        StudentEnquiry::create([
            'student_first_name'            => $request->get('student_first_name'),
            'student_middle_name'           => $request->get('student_middle_name'),
            'student_last_name'             => $request->get('student_last_name'),
            'email'                         => $request->get('email'),
            'home_phone'                    => $request->get('home_phone'),
            'mobile_phone'                  => $request->get('mobile_phone'),
            'slc_percentage'                => $request->get('slc_percentage'),
            'plus_two_percentage'           => $request->get('plus_two_percentage'),
            'year_of_completion_slc'        => $request->get('year_of_completion_slc'),
            'year_of_completion_plus_two'   => $request->get('year_of_completion_plus_two'),
            'language'                      => $request->get('language'),
            'language_score'                => $request->get('language_score'),
            'guardian_first_name'           => $request->get('guardian_first_name'),
            'guardian_middle_name'          => $request->get('guardian_middle_name'),
            'guardian_last_name'            => $request->get('guardian_last_name'),
            'guardian_email'                => $request->get('guardian_email'),
            'guardian_phone'                => $request->get('guardian_phone'),
            'guardian_mobile'               => $request->get('guardian_mobile'),
            'guardian_relationship'         => $request->get('guardian_relationship'),
            'interested_course'             => $request->get('interested_course'),
            'preferred_place'               => $request->get('preferred_place'),
            'how_know_us'                   => $request->get('how_know_us'),
            'enquiry_date'                  => $request->get('enquiry_date'),
            'user_id'                       => $user->id,
            'branch_id'                     => $request->get('branch_id'),
        ]);

        AppHelper::flash('success', 'Record created Successfully.');

        return redirect()->route($this->base_route.'.list');
    }

    public function edit($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['branch']  = Branch::select('id', 'branch_name')->get();
        $data['row'] = $this->model;

        return view(parent::loadDefaultVars($this->view_path . '.edit'), compact('data'));
    }

    public function update(UpdateFormValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        $user = Auth::user();
        $data = $this->model;

        $data->update([
            'student_first_name'            => $request->get('student_first_name'),
            'student_middle_name'           => $request->get('student_middle_name'),
            'student_last_name'             => $request->get('student_last_name'),
            'email'                         => $request->get('email'),
            'home_phone'                    => $request->get('home_phone'),
            'mobile_phone'                  => $request->get('mobile_phone'),
            'slc_percentage'                => $request->get('slc_percentage'),
            'plus_two_percentage'           => $request->get('plus_two_percentage'),
            'year_of_completion_slc'        => $request->get('year_of_completion_slc'),
            'year_of_completion_plus_two'   => $request->get('year_of_completion_plus_two'),
            'language'                      => $request->get('language'),
            'language_score'                => $request->get('language_score'),
            'guardian_first_name'           => $request->get('guardian_first_name'),
            'guardian_middle_name'          => $request->get('guardian_middle_name'),
            'guardian_last_name'            => $request->get('guardian_last_name'),
            'guardian_email'                => $request->get('guardian_email'),
            'guardian_phone'                => $request->get('guardian_phone'),
            'guardian_mobile'               => $request->get('guardian_mobile'),
            'guardian_relationship'         => $request->get('guardian_relationship'),
            'interested_course'             => $request->get('interested_course'),
            'preferred_place'               => $request->get('preferred_place'),
            'how_know_us'                   => $request->get('how_know_us'),
            'enquiry_date'                  => $request->get('enquiry_date'),
            'user_id'                       => $user->id,
            'branch_id'                     => $request->get('branch_id'),
        ]);

        $this->editLog($user->id, $request->get('student_first_name'), 'Student Enquiry');

        AppHelper::flash('success', 'Record updated successfully.');

        return redirect()->route($this->base_route.'.list');
    }

    public function destroy($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route.'.list');
        }

        $user = Auth::user();

        $student = StudentEnquiry::where('id', '=', $id)->first();

        DB::table('student_enquiry')
            ->where('id', $id)
            ->update(['is_deleted' => 1]);

        $this->deleteLog($user->id, $student->student_first_name, 'Student Enquiry');

        AppHelper::flash('success', 'Record deleted successfully.');

        return redirect()->route($this->base_route.'.list');
    }

    public function restore($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route.'.deletedList');
        }

        DB::table('student_enquiry')
            ->where('id', $id)
            ->update(['is_deleted' => 0]);

        AppHelper::flash('success', 'Record restored successfully.');

        return redirect()->route($this->base_route.'.deletedList');
    }

    public function deletePermanent($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route.'.deletedList');
        }

        $student = StudentEnquiry::where('id', '=', $id)->first();

        StudentEnquiry::destroy($id);

        $user = Auth::user();

        $this->deletePermanentLog($user->id, $student->student_first_name, 'Student Enquiry');

        AppHelper::flash('success', 'Record permanently deleted from database.');

        return redirect()->route($this->base_route.'.deletedList');
    }

    protected function addCounselorFeedback($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['row'] = $this->model;

        return view(parent::loadDefaultVars($this->view_path . '.storeCounselorFeedback'), compact('data'));

    }

    protected function storeCounselorFeedback(AddCounselorFeedbackValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        $user = Auth::user();

        DB::table('student_enquiry')
            ->where('id', $id)
            ->update([
                'is_counselled'         => 1,
                'counselling_date'      => Carbon::now()->format('Y-m-d H:i:s'),
                'counsellor_remarks'    => $request->get('counsellor_remarks'),
                'counsellor_id'         => $user->id,
            ]);

        $this->editLog($user->id, $request->get('counsellor_remarks'), 'Student Enquiry');

        AppHelper::flash('success', 'Record updated successfully.');

        return redirect()->route($this->base_route.'.list');

    }

    protected function editCounselorFeedback($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['row'] = $this->model;

        return view(parent::loadDefaultVars($this->view_path . '.updateCounselorFeedback'), compact('data'));

    }

    protected function updateCounselorFeedback(UpdateCounselorFeedbackValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        $user = Auth::user();

        DB::table('student_enquiry')
            ->where('id', $id)
            ->update([
                'counselling_date'      => Carbon::now()->format('Y-m-d H:i:s'),
                'counsellor_remarks'    => $request->get('counsellor_remarks'),
                'counsellor_id'         => $user->id,
            ]);

        $this->editLog($user->id, $request->get('counsellor_remarks'), 'Student Enquiry');

        AppHelper::flash('success', 'Record updated successfully.');

        return redirect()->route($this->base_route.'.list');

    }

    protected function goToRegistration($id){
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route.'.list')->withErrors(['message' => 'Invalid Request']);
        }

        return view(parent::loadDefaultVars($this->view_path . '.create'), compact('data'));
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = StudentEnquiry::find($id);

        return $this->model;
    }
}
