<?php

namespace App\Http\Controllers\Superadmin;

use App\Models\Branch;
use Auth;
use Gate;
use Carbon;
use DB;
use App\User;
use AppHelper;
use App\Http\Requests;
use App\Http\Requests\Superadmin\User\AddFormValidation;
use App\Http\Requests\Superadmin\User\UpdateFormValidation;


class UserController extends SuperadminBaseController {

    protected $view_path = 'superadmin.user';
    protected $base_route = 'superadmin.user';
    protected $model;

    public function index()
    {
        $data = [];
        $data['rows'] = DB::select( DB::raw("SELECT u.*, b.branch_name FROM users AS u 
                        INNER JOIN branch AS b ON u.branch_id = b.id ORDER BY u.id ASC"));

        return view(parent::loadDefaultVars($this->view_path . '.index'), compact('data'));
    }

    public function view($id)
    {
        $data = [];
        $data['branch'] = Branch::select('id', 'branch_name')->get();
        $data['row']     = User::find($id);

        return view(parent::loadDefaultVars($this->view_path . '.view'), compact('data'));
    }

    public function create()
    {
        $data = [];
        $data['branch'] = Branch::select('id', 'branch_name')->where('is_deleted', '0')->get();

        return view(parent::loadDefaultVars($this->view_path . '.create'), compact('data'));
    }

    public function store(AddFormValidation $request)
    {
        User::create([
            'fullname'    => $request->get('fullname'),
            'username'    => $request->get('username'),
            'email'       => $request->get('email'),
            'password'    => bcrypt($request->get('password')),
            'role'        => $request->get('role'),
            'branch_id'   => $request->get('branch_id'),
            'created_at'  => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'  => Carbon::now()->format('Y-m-d H:i:s'),
            'status'      => $request->get('status')
        ]);

        AppHelper::flash('success', 'Record created Successfully.');

        return redirect()->route($this->base_route);
    }

    public function edit($id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }

        $data = [];
        $data['branch'] = Branch::select('id', 'branch_name')->where('is_deleted', '0')->get();
        $data['row'] = $this->model;

        return view(parent::loadDefaultVars($this->view_path . '.edit'), compact('data'));
    }

    public function update(UpdateFormValidation $request, $id)
    {
        if (!$this->idExist($id))
        {
            return redirect()->route($this->base_route)->withErrors(['message' => 'Invalid Request']);
        }

        $user = Auth::user();

        $data = $this->model;

        if ($request->get('password') === '')
        {
            $data->update([
                'fullname'    => $request->get('fullname'),
                'username'    => $request->get('username'),
                'email'       => $request->get('email'),
                'password'    => $data->password,
                'role'        => $request->get('role'),
                'branch_id'   => $request->get('branch_id'),
                'updated_at'  => Carbon::now()->format('Y-m-d H:i:s'),
                'status'      => $request->get('status')
            ]);
        } else
        {
            $data->update([
                'fullname'    => $request->get('fullname'),
                'username'    => $request->get('username'),
                'email'       => $request->get('email'),
                'password'    => bcrypt($request->get('password')),
                'role'        => $request->get('role'),
                'branch_id'   => $request->get('branch_id'),
                'updated_at'  => Carbon::now()->format('Y-m-d H:i:s'),
                'status'      => $request->get('status')
            ]);
        }

        $this->editLog($user->id, $request->get('fullname'), 'User');

        AppHelper::flash('success', 'Record updated successfully.');

        return redirect()->route($this->base_route);
    }

    public function destroy($id)
    {
        if (!$this->idExist($id))
        {
            AppHelper::flash('warning', 'Invalid Request.');

            return redirect()->route($this->base_route);
        }

        $user_name = User::where('id', '=', $id)->first();

        $user = Auth::user();

        User::destroy($id);

        $this->deletePermanentLog($user->id, $user_name->fullname, 'User');

        AppHelper::flash('success', 'Record deleted successfully.');

        return redirect()->route($this->base_route);
    }

    public function profile()
    {
        $data = [];
        $data['rows'] = Auth::user();

        return view(parent::loadDefaultVars($this->view_path . '.profile'), compact('data'));
    }

    /**
     * Helper Methods
     */
    protected function idExist($id)
    {
        $this->model = User::find($id);

        return $this->model;
    }
}
