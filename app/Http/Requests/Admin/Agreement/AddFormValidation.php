<?php

namespace App\Http\Requests\Admin\Agreement;

use App\Http\Requests\Request;

class AddFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'grand_father_name'     => 'required',
            'agreement_date'        => 'required',
            'district'              => 'required',
            'vdc_municipality'      => 'required',
            'ward_no'               => 'required|numeric',
            'tole'                  => 'required',
            'witness1_name'         => 'required',
            'witness2_name'         => 'required',
            'first_party'           => 'required',
            'year'                  => 'required',
            'month'                 => 'required',
            'day'                   => 'required',
            'bar'                   => 'required',
        ];
    }
    
}
