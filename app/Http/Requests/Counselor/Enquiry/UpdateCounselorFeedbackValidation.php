<?php

namespace App\Http\Requests\Counselor\Enquiry;

use App\Http\Requests\Request;

class UpdateCounselorFeedbackValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'counsellor_remarks'  => 'required'
        ];
    }
    
}
