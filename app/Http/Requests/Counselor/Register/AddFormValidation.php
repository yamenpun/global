<?php

namespace App\Http\Requests\Counselor\Register;

use App\Http\Requests\Request;

class AddFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'college_id'                    => 'required',
            'college_id'                    => 'required',
            'date_of_birth'                 => 'required',
            'intake'                        => 'required',
            'father_name'                   => 'required',
            'mother_name'                   => 'required',
            'permanent_address'             => 'required',
            'emergency_contact_number'      => 'required',
        ];
    }
    
}
