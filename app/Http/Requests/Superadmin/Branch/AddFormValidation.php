<?php

namespace App\Http\Requests\Superadmin\Branch;

use App\Http\Requests\Request;

class AddFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'branch_name'      => 'required',
            'branch_address'   => 'required',
            'branch_phone'     => 'required',
            'branch_email'     => 'required|email|unique:branch,branch_email'
        ];
    }
    
}
