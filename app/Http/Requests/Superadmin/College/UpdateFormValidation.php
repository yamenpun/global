<?php

namespace App\Http\Requests\Superadmin\College;

use App\Http\Requests\Request;

class UpdateFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'college_name'          => 'required',
            'college_address'       => 'required',
            'college_phone'         => 'required',
            'college_country'       => 'required',
        ];
    }
}
