<?php

namespace App\Http\Requests\Superadmin\Course;

use App\Http\Requests\Request;

class UpdateFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'course_name'      => 'required',
        ];
    }
}
