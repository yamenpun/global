<?php

namespace App\Http\Requests\Superadmin\Enquiry;

use App\Http\Requests\Request;

class UpdateFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'student_first_name'                => 'required',
            'student_last_name'                 => 'required',
            'mobile_phone'                      => 'required',
            'slc_percentage'                    => 'required',
            'year_of_completion_slc'            => 'required',
            'plus_two_percentage'               => 'required',
            'year_of_completion_plus_two'       => 'required',
            'guardian_first_name'               => 'required',
            'guardian_last_name'                => 'required',
            'guardian_phone'                    => 'required',
            'interested_course'                 => 'required'
        ];
    }
}
