<?php

namespace App\Http\Requests\Superadmin\Register;

use App\Http\Requests\Request;

class UpdateFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'college_id'                    => 'required',
            'course_id'                     => 'required',
            'date_of_birth'                 => 'required',
            'intake'                        => 'required',
            'father_name'                   => 'required',
            'mother_name'                   => 'required',
            'permanent_address'             => 'required',
            'emergency_contact_number'      => 'required',
        ];
    }
}
