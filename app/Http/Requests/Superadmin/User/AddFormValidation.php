<?php

namespace App\Http\Requests\Superadmin\User;

use App\Http\Requests\Request;

class AddFormValidation extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fullname'      => 'required',
            'username'      => 'required|min:5|unique:users,username',
            'email'         => 'required|email|unique:users,email',
            'password'      => 'required|confirmed'
        ];
    }
    
}
