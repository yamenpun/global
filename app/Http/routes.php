<?php

Route::get('/',                                     ['as' => 'home', 'uses' => 'HomeController@index']);

Route::auth();

Route::group(['prefix' => 'superadmin', 'middleware' => 'auth'], function(){

    Route::get('dashboard',                             ['as' => 'superadmin.dashboard',                        'uses' => 'Superadmin\DashboardController@index']);

    Route::get('user',                                  ['as' => 'superadmin.user',                             'uses' => 'Superadmin\UserController@index']);
    Route::get('user/add',                              ['as' => 'superadmin.user.add',                         'uses' => 'Superadmin\UserController@create']);
    Route::post('user/store',                           ['as' => 'superadmin.user.store',                       'uses' => 'Superadmin\UserController@store']);
    Route::get('user/edit/{id}',                        ['as' => 'superadmin.user.edit',                        'uses' => 'Superadmin\UserController@edit']);
    Route::post('user/update/{id}',                     ['as' => 'superadmin.user.update',                      'uses' => 'Superadmin\UserController@update']);
    Route::get('user/delete/{id}',                      ['as' => 'superadmin.user.delete',                      'uses' => 'Superadmin\UserController@destroy']);
    Route::get('user/profile',                          ['as' => 'superadmin.user.profile',                     'uses' => 'Superadmin\UserController@profile']);

    Route::get('log/list',                              ['as' => 'superadmin.log.list',                         'uses' => 'Superadmin\ActivityLogController@index']);
    Route::get('log/deleteForm',                        ['as' => 'superadmin.log.deleteForm',                   'uses' => 'Superadmin\ActivityLogController@deleteForm']);
    Route::post('log/delete',                           ['as' => 'superadmin.log.delete',                       'uses' => 'Superadmin\ActivityLogController@delete']);
    Route::get('log/delete-permanent/{id}',             ['as' => 'superadmin.log.delete-permanent',             'uses' => 'Superadmin\ActivityLogController@deletePermanent']);

    Route::get('branch/list',                           ['as' => 'superadmin.branch.list',                      'uses' => 'Superadmin\BranchController@index']);
    Route::get('branch/add',                            ['as' => 'superadmin.branch.add',                       'uses' => 'Superadmin\BranchController@create']);
    Route::post('branch/store',                         ['as' => 'superadmin.branch.store',                     'uses' => 'Superadmin\BranchController@store']);
    Route::get('branch/edit/{id}',                      ['as' => 'superadmin.branch.edit',                      'uses' => 'Superadmin\BranchController@edit']);
    Route::post('branch/update/{id}',                   ['as' => 'superadmin.branch.update',                    'uses' => 'Superadmin\BranchController@update']);
    Route::get('branch/delete/{id}',                    ['as' => 'superadmin.branch.delete',                    'uses' => 'Superadmin\BranchController@destroy']);
    Route::get('branch/deletedList',                    ['as' => 'superadmin.branch.deletedList',               'uses' => 'Superadmin\BranchController@isDeletedList']);
    Route::get('branch/restore/{id}',                   ['as' => 'superadmin.branch.restore',                   'uses' => 'Superadmin\BranchController@restore']);
    Route::get('branch/delete-permanent/{id}',          ['as' => 'superadmin.branch.delete-permanent',          'uses' => 'Superadmin\BranchController@deletePermanent']);
    Route::get('branch/print',                          ['as' => 'superadmin.branch.print',                     'uses' => 'Superadmin\BranchController@exportAsPrint']);
    Route::get('branch/pdf',                            ['as' => 'superadmin.branch.pdf',                       'uses' => 'Superadmin\BranchController@exportAsPdf']);
    Route::get('branch/excel',                          ['as' => 'superadmin.branch.excel',                     'uses' => 'Superadmin\BranchController@exportAsExcel']);
    Route::get('branch/deletedPrint',                   ['as' => 'superadmin.branch.deletedPrint',              'uses' => 'Superadmin\BranchController@deletedExportAsPrint']);
    Route::get('branch/deletedPdf',                     ['as' => 'superadmin.branch.deletedPdf',                'uses' => 'Superadmin\BranchController@deletedExportAsPdf']);
    Route::get('branch/deletedExcel',                   ['as' => 'superadmin.branch.deletedExcel',              'uses' => 'Superadmin\BranchController@deletedExportAsExcel']);

    Route::get('college/list',                          ['as' => 'superadmin.college.list',                     'uses' => 'Superadmin\CollegeController@index']);
    Route::get('college/add',                           ['as' => 'superadmin.college.add',                      'uses' => 'Superadmin\CollegeController@create']);
    Route::post('college/store',                        ['as' => 'superadmin.college.store',                    'uses' => 'Superadmin\CollegeController@store']);
    Route::get('college/edit/{id}',                     ['as' => 'superadmin.college.edit',                     'uses' => 'Superadmin\CollegeController@edit']);
    Route::post('college/update/{id}',                  ['as' => 'superadmin.college.update',                   'uses' => 'Superadmin\CollegeController@update']);
    Route::get('college/delete/{id}',                   ['as' => 'superadmin.college.delete',                   'uses' => 'Superadmin\CollegeController@destroy']);
    Route::get('college/deletedList',                   ['as' => 'superadmin.college.deletedList',              'uses' => 'Superadmin\CollegeController@isDeletedList']);
    Route::get('college/restore/{id}',                  ['as' => 'superadmin.college.restore',                  'uses' => 'Superadmin\CollegeController@restore']);
    Route::get('college/delete-permanent/{id}',         ['as' => 'superadmin.college.delete-permanent',         'uses' => 'Superadmin\CollegeController@deletePermanent']);
    Route::get('college/print',                         ['as' => 'superadmin.college.print',                    'uses' => 'Superadmin\CollegeController@exportAsPrint']);
    Route::get('college/pdf',                           ['as' => 'superadmin.college.pdf',                      'uses' => 'Superadmin\CollegeController@exportAsPdf']);
    Route::get('college/excel',                         ['as' => 'superadmin.college.excel',                    'uses' => 'Superadmin\CollegeController@exportAsExcel']);
    Route::get('college/deletedPrint',                  ['as' => 'superadmin.college.deletedPrint',             'uses' => 'Superadmin\CollegeController@deletedExportAsPrint']);
    Route::get('college/deletedPdf',                    ['as' => 'superadmin.college.deletedPdf',               'uses' => 'Superadmin\CollegeController@deletedExportAsPdf']);
    Route::get('college/deletedExcel',                  ['as' => 'superadmin.college.deletedExcel',             'uses' => 'Superadmin\CollegeController@deletedExportAsExcel']);

    Route::get('course/list',                           ['as' => 'superadmin.course.list',                      'uses' => 'Superadmin\CourseController@index']);
    Route::get('course/add',                            ['as' => 'superadmin.course.add',                       'uses' => 'Superadmin\CourseController@create']);
    Route::post('course/store',                         ['as' => 'superadmin.course.store',                     'uses' => 'Superadmin\CourseController@store']);
    Route::get('course/edit/{id}',                      ['as' => 'superadmin.course.edit',                      'uses' => 'Superadmin\CourseController@edit']);
    Route::post('course/update/{id}',                   ['as' => 'superadmin.course.update',                    'uses' => 'Superadmin\CourseController@update']);
    Route::get('course/delete/{id}',                    ['as' => 'superadmin.course.delete',                    'uses' => 'Superadmin\CourseController@destroy']);
    Route::get('course/deletedList',                    ['as' => 'superadmin.course.deletedList',               'uses' => 'Superadmin\CourseController@isDeletedList']);
    Route::get('course/restore/{id}',                   ['as' => 'superadmin.course.restore',                   'uses' => 'Superadmin\CourseController@restore']);
    Route::get('course/delete-permanent/{id}',          ['as' => 'superadmin.course.delete-permanent',          'uses' => 'Superadmin\CourseController@deletePermanent']);
    Route::get('course/print',                          ['as' => 'superadmin.course.print',                     'uses' => 'Superadmin\CourseController@exportAsPrint']);
    Route::get('course/pdf',                            ['as' => 'superadmin.course.pdf',                       'uses' => 'Superadmin\CourseController@exportAsPdf']);
    Route::get('course/excel',                          ['as' => 'superadmin.course.excel',                     'uses' => 'Superadmin\CourseController@exportAsExcel']);
    Route::get('course/deletedPrint',                   ['as' => 'superadmin.course.deletedPrint',              'uses' => 'Superadmin\CourseController@deletedExportAsPrint']);
    Route::get('course/deletedPdf',                     ['as' => 'superadmin.course.deletedPdf',                'uses' => 'Superadmin\CourseController@deletedExportAsPdf']);
    Route::get('course/deletedExcel',                   ['as' => 'superadmin.course.deletedExcel',              'uses' => 'Superadmin\CourseController@deletedExportAsExcel']);

    Route::get('enquiry/list',                          ['as' => 'superadmin.enquiry.list',                     'uses' => 'Superadmin\EnquiryController@index']);
    Route::get('enquiry/add',                           ['as' => 'superadmin.enquiry.add',                      'uses' => 'Superadmin\EnquiryController@create']);
    Route::post('enquiry/store',                        ['as' => 'superadmin.enquiry.store',                    'uses' => 'Superadmin\EnquiryController@store']);
    Route::get('enquiry/view/{id}',                     ['as' => 'superadmin.enquiry.view',                     'uses' => 'Superadmin\EnquiryController@view']);
    Route::get('enquiry/deletedView/{id}',              ['as' => 'superadmin.enquiry.deletedView',              'uses' => 'Superadmin\EnquiryController@deletedView']);
    Route::get('enquiry/edit/{id}',                     ['as' => 'superadmin.enquiry.edit',                     'uses' => 'Superadmin\EnquiryController@edit']);
    Route::post('enquiry/update/{id}',                  ['as' => 'superadmin.enquiry.update',                   'uses' => 'Superadmin\EnquiryController@update']);
    Route::get('enquiry/delete/{id}',                   ['as' => 'superadmin.enquiry.delete',                   'uses' => 'Superadmin\EnquiryController@destroy']);
    Route::get('enquiry/deletedList',                   ['as' => 'superadmin.enquiry.deletedList',              'uses' => 'Superadmin\EnquiryController@isDeletedList']);
    Route::get('enquiry/restore/{id}',                  ['as' => 'superadmin.enquiry.restore',                  'uses' => 'Superadmin\EnquiryController@restore']);
    Route::get('enquiry/delete-permanent/{id}',         ['as' => 'superadmin.enquiry.delete-permanent',         'uses' => 'Superadmin\EnquiryController@deletePermanent']);
    Route::get('enquiry/print',                         ['as' => 'superadmin.enquiry.print',                    'uses' => 'Superadmin\EnquiryController@exportAsPrint']);
    Route::get('enquiry/pdf',                           ['as' => 'superadmin.enquiry.pdf',                      'uses' => 'Superadmin\EnquiryController@exportAsPdf']);
    Route::get('enquiry/excel',                         ['as' => 'superadmin.enquiry.excel',                    'uses' => 'Superadmin\EnquiryController@exportAsExcel']);
    Route::get('enquiry/deletedPrint',                  ['as' => 'superadmin.enquiry.deletedPrint',             'uses' => 'Superadmin\EnquiryController@deletedExportAsPrint']);
    Route::get('enquiry/deletedPdf',                    ['as' => 'superadmin.enquiry.deletedPdf',               'uses' => 'Superadmin\EnquiryController@deletedExportAsPdf']);
    Route::get('enquiry/deletedExcel',                  ['as' => 'superadmin.enquiry.deletedExcel',             'uses' => 'Superadmin\EnquiryController@deletedExportAsExcel']);
    Route::get('enquiry/addCounselorFeedback/{id}',     ['as' => 'superadmin.enquiry.addCounselorFeedback',     'uses' => 'Superadmin\EnquiryController@addCounselorFeedback']);
    Route::post('enquiry/storeCounselorFeedback/{id}',  ['as' => 'superadmin.enquiry.storeCounselorFeedback',   'uses' => 'Superadmin\EnquiryController@storeCounselorFeedback']);
    Route::get('enquiry/editCounselorFeedback/{id}',    ['as' => 'superadmin.enquiry.editCounselorFeedback',    'uses' => 'Superadmin\EnquiryController@editCounselorFeedback']);
    Route::post('enquiry/updateCounselorFeedback/{id}', ['as' => 'superadmin.enquiry.updateCounselorFeedback',  'uses' => 'Superadmin\EnquiryController@updateCounselorFeedback']);
    Route::get('enquiry/goToRegistration/{id}',         ['as' => 'superadmin.enquiry.goToRegistration',         'uses' => 'Superadmin\EnquiryController@goToRegistration']);

    Route::get('register/list',                         ['as' => 'superadmin.register.list',                    'uses' => 'Superadmin\RegisterController@index']);
    Route::get('register/add',                          ['as' => 'superadmin.register.add',                     'uses' => 'Superadmin\RegisterController@create']);
    Route::post('register/store',                       ['as' => 'superadmin.register.store',                   'uses' => 'Superadmin\RegisterController@store']);
    Route::get('register/view/{id}',                    ['as' => 'superadmin.register.view',                    'uses' => 'Superadmin\RegisterController@view']);
    Route::get('register/deletedView/{id}',             ['as' => 'superadmin.register.deletedView',             'uses' => 'Superadmin\RegisterController@deletedView']);
    Route::get('register/edit/{id}',                    ['as' => 'superadmin.register.edit',                    'uses' => 'Superadmin\RegisterController@edit']);
    Route::post('register/update/{id}',                 ['as' => 'superadmin.register.update',                  'uses' => 'Superadmin\RegisterController@update']);
    Route::get('register/delete/{id}',                  ['as' => 'superadmin.register.delete',                  'uses' => 'Superadmin\RegisterController@destroy']);
    Route::get('register/deletedList',                  ['as' => 'superadmin.register.deletedList',             'uses' => 'Superadmin\RegisterController@isDeletedList']);
    Route::get('register/restore/{id}',                 ['as' => 'superadmin.register.restore',                 'uses' => 'Superadmin\RegisterController@restore']);
    Route::get('register/delete-permanent/{id}',        ['as' => 'superadmin.register.delete-permanent',        'uses' => 'Superadmin\RegisterController@deletePermanent']);
    Route::get('register/print',                        ['as' => 'superadmin.register.print',                   'uses' => 'Superadmin\RegisterController@exportAsPrint']);
    Route::get('register/pdf',                          ['as' => 'superadmin.register.pdf',                     'uses' => 'Superadmin\RegisterController@exportAsPdf']);
    Route::get('register/excel',                        ['as' => 'superadmin.register.excel',                   'uses' => 'Superadmin\RegisterController@exportAsExcel']);
    Route::get('register/deletedPrint',                 ['as' => 'superadmin.register.deletedPrint',            'uses' => 'Superadmin\RegisterController@deletedExportAsPrint']);
    Route::get('register/deletedPdf',                   ['as' => 'superadmin.register.deletedPdf',              'uses' => 'Superadmin\RegisterController@deletedExportAsPdf']);
    Route::get('register/deletedExcel',                 ['as' => 'superadmin.register.deletedExcel',            'uses' => 'Superadmin\RegisterController@deletedExportAsExcel']);

    Route::get('agreement/list',                        ['as' => 'superadmin.agreement.list',                   'uses' => 'Superadmin\AgreementController@index']);
    Route::get('agreement/add',                         ['as' => 'superadmin.agreement.add',                    'uses' => 'Superadmin\AgreementController@create']);
    Route::post('agreement/store',                      ['as' => 'superadmin.agreement.store',                  'uses' => 'Superadmin\AgreementController@store']);
    Route::get('agreement/view/{id}',                   ['as' => 'superadmin.agreement.view',                   'uses' => 'Superadmin\AgreementController@view']);
    Route::get('agreement/deletedView/{id}',            ['as' => 'superadmin.agreement.deletedView',            'uses' => 'Superadmin\AgreementController@deletedView']);
    Route::get('agreement/edit/{id}',                   ['as' => 'superadmin.agreement.edit',                   'uses' => 'Superadmin\AgreementController@edit']);
    Route::post('agreement/update/{id}',                ['as' => 'superadmin.agreement.update',                 'uses' => 'Superadmin\AgreementController@update']);
    Route::get('agreement/delete/{id}',                 ['as' => 'superadmin.agreement.delete',                 'uses' => 'Superadmin\AgreementController@destroy']);
    Route::get('agreement/deletedList',                 ['as' => 'superadmin.agreement.deletedList',            'uses' => 'Superadmin\AgreementController@isDeletedList']);
    Route::get('agreement/restore/{id}',                ['as' => 'superadmin.agreement.restore',                'uses' => 'Superadmin\AgreementController@restore']);
    Route::get('agreement/delete-permanent/{id}',       ['as' => 'superadmin.agreement.delete-permanent',       'uses' => 'Superadmin\AgreementController@deletePermanent']);
    Route::get('agreement/print',                       ['as' => 'superadmin.agreement.print',                  'uses' => 'Superadmin\AgreementController@exportAsPrint']);
    Route::get('agreement/pdf',                         ['as' => 'superadmin.agreement.pdf',                    'uses' => 'Superadmin\AgreementController@exportAsPdf']);
    Route::get('agreement/excel',                       ['as' => 'superadmin.agreement.excel',                  'uses' => 'Superadmin\AgreementController@exportAsExcel']);
    Route::get('agreement/deletedPrint',                ['as' => 'superadmin.agreement.deletedPrint',           'uses' => 'Superadmin\AgreementController@deletedExportAsPrint']);
    Route::get('agreement/deletedPdf',                  ['as' => 'superadmin.agreement.deletedPdf',             'uses' => 'Superadmin\AgreementController@deletedExportAsPdf']);
    Route::get('agreement/deletedExcel',                ['as' => 'superadmin.agreement.deletedExcel',           'uses' => 'Superadmin\AgreementController@deletedExportAsExcel']);
    Route::get('agreement/agreementView',               ['as' => 'superadmin.agreement.agreementView',          'uses' => 'Superadmin\AgreementController@agreementView']);

});

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function(){
    Route::get('dashboard',                             ['as' => 'admin.dashboard',                         'uses' => 'Admin\DashboardController@index']);

    Route::get('user',                                  ['as' => 'admin.user',                              'uses' => 'Admin\UserController@index']);
    Route::get('user/add',                              ['as' => 'admin.user.add',                          'uses' => 'Admin\UserController@create']);
    Route::post('user/store',                           ['as' => 'admin.user.store',                        'uses' => 'Admin\UserController@store']);
    Route::get('user/view/{id}',                        ['as' => 'admin.user.view',                         'uses' => 'Admin\UserController@view']);
    Route::get('user/edit/{id}',                        ['as' => 'admin.user.edit',                         'uses' => 'Admin\UserController@edit']);
    Route::post('user/update/{id}',                     ['as' => 'admin.user.update',                       'uses' => 'Admin\UserController@update']);
    Route::get('user/delete/{id}',                      ['as' => 'admin.user.delete',                       'uses' => 'Admin\UserController@destroy']);
    Route::get('user/profile',                          ['as' => 'admin.user.profile',                      'uses' => 'Admin\UserController@profile']);

    Route::get('log/list',                              ['as' => 'admin.log.list',                          'uses' => 'Admin\ActivityLogController@index']);
    Route::get('log/delete-permanent/{id}',             ['as' => 'admin.log.delete-permanent',              'uses' => 'Admin\ActivityLogController@deletePermanent']);

    Route::get('enquiry/list',                          ['as' => 'admin.enquiry.list',                      'uses' => 'Admin\EnquiryController@index']);
    Route::get('enquiry/add',                           ['as' => 'admin.enquiry.add',                       'uses' => 'Admin\EnquiryController@create']);
    Route::post('enquiry/store',                        ['as' => 'admin.enquiry.store',                     'uses' => 'Admin\EnquiryController@store']);
    Route::get('enquiry/view/{id}',                     ['as' => 'admin.enquiry.view',                      'uses' => 'Admin\EnquiryController@view']);
    Route::get('enquiry/deletedView/{id}',              ['as' => 'admin.enquiry.deletedView',               'uses' => 'Admin\EnquiryController@deletedView']);
    Route::get('enquiry/edit/{id}',                     ['as' => 'admin.enquiry.edit',                      'uses' => 'Admin\EnquiryController@edit']);
    Route::post('enquiry/update/{id}',                  ['as' => 'admin.enquiry.update',                    'uses' => 'Admin\EnquiryController@update']);
    Route::get('enquiry/delete/{id}',                   ['as' => 'admin.enquiry.delete',                    'uses' => 'Admin\EnquiryController@destroy']);
    Route::get('enquiry/deletedList',                   ['as' => 'admin.enquiry.deletedList',               'uses' => 'Admin\EnquiryController@isDeletedList']);
    Route::get('enquiry/restore/{id}',                  ['as' => 'admin.enquiry.restore',                   'uses' => 'Admin\EnquiryController@restore']);
    Route::get('enquiry/delete-permanent/{id}',         ['as' => 'admin.enquiry.delete-permanent',          'uses' => 'Admin\EnquiryController@deletePermanent']);
    Route::get('enquiry/print',                         ['as' => 'admin.enquiry.print',                     'uses' => 'Admin\EnquiryController@exportAsPrint']);
    Route::get('enquiry/pdf',                           ['as' => 'admin.enquiry.pdf',                       'uses' => 'Admin\EnquiryController@exportAsPdf']);
    Route::get('enquiry/excel',                         ['as' => 'admin.enquiry.excel',                     'uses' => 'Admin\EnquiryController@exportAsExcel']);
    Route::get('enquiry/deletedPrint',                  ['as' => 'admin.enquiry.deletedPrint',              'uses' => 'Admin\EnquiryController@deletedExportAsPrint']);
    Route::get('enquiry/deletedPdf',                    ['as' => 'admin.enquiry.deletedPdf',                'uses' => 'Admin\EnquiryController@deletedExportAsPdf']);
    Route::get('enquiry/deletedExcel',                  ['as' => 'admin.enquiry.deletedExcel',              'uses' => 'Admin\EnquiryController@deletedExportAsExcel']);
    Route::get('enquiry/addCounselorFeedback/{id}',     ['as' => 'admin.enquiry.addCounselorFeedback',      'uses' => 'Admin\EnquiryController@addCounselorFeedback']);
    Route::post('enquiry/storeCounselorFeedback/{id}',  ['as' => 'admin.enquiry.storeCounselorFeedback',    'uses' => 'Admin\EnquiryController@storeCounselorFeedback']);
    Route::get('enquiry/editCounselorFeedback/{id}',    ['as' => 'admin.enquiry.editCounselorFeedback',     'uses' => 'Admin\EnquiryController@editCounselorFeedback']);
    Route::post('enquiry/updateCounselorFeedback/{id}', ['as' => 'admin.enquiry.updateCounselorFeedback',   'uses' => 'Admin\EnquiryController@updateCounselorFeedback']);
    Route::get('enquiry/goToRegistration/{id}',         ['as' => 'admin.enquiry.goToRegistration',          'uses' => 'Admin\EnquiryController@goToRegistration']);

    Route::get('register/list',                         ['as' => 'admin.register.list',                     'uses' => 'Admin\RegisterController@index']);
    Route::get('register/add',                          ['as' => 'admin.register.add',                      'uses' => 'Admin\RegisterController@create']);
    Route::post('register/store',                       ['as' => 'admin.register.store',                    'uses' => 'Admin\RegisterController@store']);
    Route::get('register/view/{id}',                    ['as' => 'admin.register.view',                     'uses' => 'Admin\RegisterController@view']);
    Route::get('register/deletedView/{id}',             ['as' => 'admin.register.deletedView',              'uses' => 'Admin\RegisterController@deletedView']);
    Route::get('register/edit/{id}',                    ['as' => 'admin.register.edit',                     'uses' => 'Admin\RegisterController@edit']);
    Route::post('register/update/{id}',                 ['as' => 'admin.register.update',                   'uses' => 'Admin\RegisterController@update']);
    Route::get('register/delete/{id}',                  ['as' => 'admin.register.delete',                   'uses' => 'Admin\RegisterController@destroy']);
    Route::get('register/deletedList',                  ['as' => 'admin.register.deletedList',              'uses' => 'Admin\RegisterController@isDeletedList']);
    Route::get('register/restore/{id}',                 ['as' => 'admin.register.restore',                  'uses' => 'Admin\RegisterController@restore']);
    Route::get('register/delete-permanent/{id}',        ['as' => 'admin.register.delete-permanent',         'uses' => 'Admin\RegisterController@deletePermanent']);
    Route::get('register/print',                        ['as' => 'admin.register.print',                    'uses' => 'Admin\RegisterController@exportAsPrint']);
    Route::get('register/pdf',                          ['as' => 'admin.register.pdf',                      'uses' => 'Admin\RegisterController@exportAsPdf']);
    Route::get('register/excel',                        ['as' => 'admin.register.excel',                    'uses' => 'Admin\RegisterController@exportAsExcel']);
    Route::get('register/deletedPrint',                 ['as' => 'admin.register.deletedPrint',             'uses' => 'Admin\RegisterController@deletedExportAsPrint']);
    Route::get('register/deletedPdf',                   ['as' => 'admin.register.deletedPdf',               'uses' => 'Admin\RegisterController@deletedExportAsPdf']);
    Route::get('register/deletedExcel',                 ['as' => 'admin.register.deletedExcel',             'uses' => 'Admin\RegisterController@deletedExportAsExcel']);

    Route::get('agreement/list',                        ['as' => 'admin.agreement.list',                    'uses' => 'Admin\AgreementController@index']);
    Route::get('agreement/add',                         ['as' => 'admin.agreement.add',                     'uses' => 'Admin\AgreementController@create']);
    Route::post('agreement/store',                      ['as' => 'admin.agreement.store',                   'uses' => 'Admin\AgreementController@store']);
    Route::get('agreement/view/{id}',                   ['as' => 'admin.agreement.view',                    'uses' => 'Admin\AgreementController@view']);
    Route::get('agreement/deletedView/{id}',            ['as' => 'admin.agreement.deletedView',             'uses' => 'Admin\AgreementController@deletedView']);
    Route::get('agreement/edit/{id}',                   ['as' => 'admin.agreement.edit',                    'uses' => 'Admin\AgreementController@edit']);
    Route::post('agreement/update/{id}',                ['as' => 'admin.agreement.update',                  'uses' => 'Admin\AgreementController@update']);
    Route::get('agreement/delete/{id}',                 ['as' => 'admin.agreement.delete',                  'uses' => 'Admin\AgreementController@destroy']);
    Route::get('agreement/deletedList',                 ['as' => 'admin.agreement.deletedList',             'uses' => 'Admin\AgreementController@isDeletedList']);
    Route::get('agreement/restore/{id}',                ['as' => 'admin.agreement.restore',                 'uses' => 'Admin\AgreementController@restore']);
    Route::get('agreement/delete-permanent/{id}',       ['as' => 'admin.agreement.delete-permanent',        'uses' => 'Admin\AgreementController@deletePermanent']);
    Route::get('agreement/print',                       ['as' => 'admin.agreement.print',                   'uses' => 'Admin\AgreementController@exportAsPrint']);
    Route::get('agreement/pdf',                         ['as' => 'admin.agreement.pdf',                     'uses' => 'Admin\AgreementController@exportAsPdf']);
    Route::get('agreement/excel',                       ['as' => 'admin.agreement.excel',                   'uses' => 'Admin\AgreementController@exportAsExcel']);
    Route::get('agreement/deletedPrint',                ['as' => 'admin.agreement.deletedPrint',            'uses' => 'Admin\AgreementController@deletedExportAsPrint']);
    Route::get('agreement/deletedPdf',                  ['as' => 'admin.agreement.deletedPdf',              'uses' => 'Admin\AgreementController@deletedExportAsPdf']);
    Route::get('agreement/deletedExcel',                ['as' => 'admin.agreement.deletedExcel',            'uses' => 'Admin\AgreementController@deletedExportAsExcel']);
    Route::get('agreement/agreementView',               ['as' => 'admin.agreement.agreementView',           'uses' => 'Admin\AgreementController@agreementView']);

    Route::get('college/list',                          ['as' => 'admin.college.list',                      'uses' => 'Admin\CollegeController@index']);
    Route::get('college/add',                           ['as' => 'admin.college.add',                       'uses' => 'Admin\CollegeController@create']);
    Route::post('college/store',                        ['as' => 'admin.college.store',                     'uses' => 'Admin\CollegeController@store']);
    Route::get('college/edit/{id}',                     ['as' => 'admin.college.edit',                      'uses' => 'Admin\CollegeController@edit']);
    Route::post('college/update/{id}',                  ['as' => 'admin.college.update',                    'uses' => 'Admin\CollegeController@update']);
    Route::get('college/delete/{id}',                   ['as' => 'admin.college.delete',                    'uses' => 'Admin\CollegeController@destroy']);
    Route::get('college/deletedList',                   ['as' => 'admin.college.deletedList',               'uses' => 'Admin\CollegeController@isDeletedList']);
    Route::get('college/restore/{id}',                  ['as' => 'admin.college.restore',                   'uses' => 'Admin\CollegeController@restore']);
    Route::get('college/delete-permanent/{id}',         ['as' => 'admin.college.delete-permanent',          'uses' => 'Admin\CollegeController@deletePermanent']);
    Route::get('college/print',                         ['as' => 'admin.college.print',                     'uses' => 'Admin\CollegeController@exportAsPrint']);
    Route::get('college/pdf',                           ['as' => 'admin.college.pdf',                       'uses' => 'Admin\CollegeController@exportAsPdf']);
    Route::get('college/excel',                         ['as' => 'admin.college.excel',                     'uses' => 'Admin\CollegeController@exportAsExcel']);
    Route::get('college/deletedPrint',                  ['as' => 'admin.college.deletedPrint',              'uses' => 'Admin\CollegeController@deletedExportAsPrint']);
    Route::get('college/deletedPdf',                    ['as' => 'admin.college.deletedPdf',                'uses' => 'Admin\CollegeController@deletedExportAsPdf']);
    Route::get('college/deletedExcel',                  ['as' => 'admin.college.deletedExcel',              'uses' => 'Admin\CollegeController@deletedExportAsExcel']);

    Route::get('course/list',                           ['as' => 'admin.course.list',                       'uses' => 'Admin\CourseController@index']);
    Route::get('course/add',                            ['as' => 'admin.course.add',                        'uses' => 'Admin\CourseController@create']);
    Route::post('course/store',                         ['as' => 'admin.course.store',                      'uses' => 'Admin\CourseController@store']);
    Route::get('course/edit/{id}',                      ['as' => 'admin.course.edit',                       'uses' => 'Admin\CourseController@edit']);
    Route::post('course/update/{id}',                   ['as' => 'admin.course.update',                     'uses' => 'Admin\CourseController@update']);
    Route::get('course/delete/{id}',                    ['as' => 'admin.course.delete',                     'uses' => 'Admin\CourseController@destroy']);
    Route::get('course/deletedList',                    ['as' => 'admin.course.deletedList',                'uses' => 'Admin\CourseController@isDeletedList']);
    Route::get('course/restore/{id}',                   ['as' => 'admin.course.restore',                    'uses' => 'Admin\CourseController@restore']);
    Route::get('course/delete-permanent/{id}',          ['as' => 'admin.course.delete-permanent',           'uses' => 'Admin\CourseController@deletePermanent']);
    Route::get('course/print',                          ['as' => 'admin.course.print',                      'uses' => 'Admin\CourseController@exportAsPrint']);
    Route::get('course/pdf',                            ['as' => 'admin.course.pdf',                        'uses' => 'Admin\CourseController@exportAsPdf']);
    Route::get('course/excel',                          ['as' => 'admin.course.excel',                      'uses' => 'Admin\CourseController@exportAsExcel']);
    Route::get('course/deletedPrint',                   ['as' => 'admin.course.deletedPrint',               'uses' => 'Admin\CourseController@deletedExportAsPrint']);
    Route::get('course/deletedPdf',                     ['as' => 'admin.course.deletedPdf',                 'uses' => 'Admin\CourseController@deletedExportAsPdf']);
    Route::get('course/deletedExcel',                   ['as' => 'admin.course.deletedExcel',               'uses' => 'Admin\CourseController@deletedExportAsExcel']);

});

Route::group(['prefix' => 'counselor', 'middleware' => 'auth'], function(){
    Route::get('dashboard',                         ['as' => 'counselor.dashboard',                     'uses' => 'Counselor\DashboardController@index']);

    Route::get('user/profile',                          ['as' => 'counselor.user.profile',                      'uses' => 'Counselor\UserController@profile']);

    Route::get('enquiry/list',                          ['as' => 'counselor.enquiry.list',                      'uses' => 'Counselor\EnquiryController@index']);
    Route::get('enquiry/view/{id}',                     ['as' => 'counselor.enquiry.view',                      'uses' => 'Counselor\EnquiryController@view']);
    Route::get('enquiry/print',                         ['as' => 'counselor.enquiry.print',                     'uses' => 'Counselor\EnquiryController@exportAsPrint']);
    Route::get('enquiry/pdf',                           ['as' => 'counselor.enquiry.pdf',                       'uses' => 'Counselor\EnquiryController@exportAsPdf']);
    Route::get('enquiry/excel',                         ['as' => 'counselor.enquiry.excel',                     'uses' => 'Counselor\EnquiryController@exportAsExcel']);
    Route::get('enquiry/addCounselorFeedback/{id}',     ['as' => 'counselor.enquiry.addCounselorFeedback',      'uses' => 'Counselor\EnquiryController@addCounselorFeedback']);
    Route::post('enquiry/storeCounselorFeedback/{id}',  ['as' => 'counselor.enquiry.storeCounselorFeedback',    'uses' => 'Counselor\EnquiryController@storeCounselorFeedback']);
    Route::get('enquiry/editCounselorFeedback/{id}',    ['as' => 'counselor.enquiry.editCounselorFeedback',     'uses' => 'Counselor\EnquiryController@editCounselorFeedback']);
    Route::post('enquiry/updateCounselorFeedback/{id}', ['as' => 'counselor.enquiry.updateCounselorFeedback',   'uses' => 'Counselor\EnquiryController@updateCounselorFeedback']);
    Route::get('enquiry/goToRegistration/{id}',         ['as' => 'counselor.enquiry.goToRegistration',          'uses' => 'Counselor\EnquiryController@goToRegistration']);

    Route::get('register/list',                         ['as' => 'counselor.register.list',                     'uses' => 'Counselor\RegisterController@index']);
    Route::get('register/add',                          ['as' => 'counselor.register.add',                      'uses' => 'Counselor\RegisterController@create']);
    Route::post('register/store',                       ['as' => 'counselor.register.store',                    'uses' => 'Counselor\RegisterController@store']);
    Route::get('register/view/{id}',                    ['as' => 'counselor.register.view',                     'uses' => 'Counselor\RegisterController@view']);
    Route::get('register/edit/{id}',                    ['as' => 'counselor.register.edit',                     'uses' => 'Counselor\RegisterController@edit']);
    Route::post('register/update/{id}',                 ['as' => 'counselor.register.update',                   'uses' => 'Counselor\RegisterController@update']);
    Route::get('register/delete/{id}',                  ['as' => 'counselor.register.delete',                   'uses' => 'Counselor\RegisterController@destroy']);
    Route::get('register/print',                        ['as' => 'counselor.register.print',                    'uses' => 'Counselor\RegisterController@exportAsPrint']);
    Route::get('register/pdf',                          ['as' => 'counselor.register.pdf',                      'uses' => 'Counselor\RegisterController@exportAsPdf']);
    Route::get('register/excel',                        ['as' => 'counselor.register.excel',                    'uses' => 'Counselor\RegisterController@exportAsExcel']);

    Route::get('agreement/list',                        ['as' => 'counselor.agreement.list',                    'uses' => 'Counselor\AgreementController@index']);
    Route::get('agreement/view/{id}',                   ['as' => 'counselor.agreement.view',                    'uses' => 'Counselor\AgreementController@view']);
    Route::get('agreement/print',                       ['as' => 'counselor.agreement.print',                   'uses' => 'Counselor\AgreementController@exportAsPrint']);
    Route::get('agreement/pdf',                         ['as' => 'counselor.agreement.pdf',                     'uses' => 'Counselor\AgreementController@exportAsPdf']);
    Route::get('agreement/excel',                       ['as' => 'counselor.agreement.excel',                   'uses' => 'Counselor\AgreementController@exportAsExcel']);
    Route::get('agreement/agreementView',               ['as' => 'counselor.agreement.agreementView',           'uses' => 'Counselor\AgreementController@agreementView']);

    Route::get('college/list',                          ['as' => 'counselor.college.list',                      'uses' => 'Counselor\CollegeController@index']);
    Route::get('college/print',                         ['as' => 'counselor.college.print',                     'uses' => 'Counselor\CollegeController@exportAsPrint']);
    Route::get('college/pdf',                           ['as' => 'counselor.college.pdf',                       'uses' => 'Counselor\CollegeController@exportAsPdf']);
    Route::get('college/excel',                         ['as' => 'counselor.college.excel',                     'uses' => 'Counselor\CollegeController@exportAsExcel']);

    Route::get('course/list',                           ['as' => 'counselor.course.list',                       'uses' => 'Counselor\CourseController@index']);
    Route::get('course/print',                          ['as' => 'counselor.course.print',                      'uses' => 'Counselor\CourseController@exportAsPrint']);
    Route::get('course/pdf',                            ['as' => 'counselor.course.pdf',                        'uses' => 'Counselor\CourseController@exportAsPdf']);
    Route::get('course/excel',                          ['as' => 'counselor.course.excel',                      'uses' => 'Counselor\CourseController@exportAsExcel']);



});

Route::group(['prefix' => 'normal', 'middleware' => 'auth'], function(){
    Route::get('dashboard',                         ['as' => 'normal.dashboard',                     'uses' => 'Normal\DashboardController@index']);

    Route::get('user/profile',                      ['as' => 'normal.user.profile',                  'uses' => 'Normal\UserController@profile']);

    Route::get('enquiry/list',                          ['as' => 'normal.enquiry.list',                      'uses' => 'Normal\EnquiryController@index']);
    Route::get('enquiry/add',                           ['as' => 'normal.enquiry.add',                       'uses' => 'Normal\EnquiryController@create']);
    Route::post('enquiry/store',                        ['as' => 'normal.enquiry.store',                     'uses' => 'Normal\EnquiryController@store']);
    Route::get('enquiry/view/{id}',                     ['as' => 'normal.enquiry.view',                      'uses' => 'Normal\EnquiryController@view']);
    Route::get('enquiry/edit/{id}',                     ['as' => 'normal.enquiry.edit',                      'uses' => 'Normal\EnquiryController@edit']);
    Route::post('enquiry/update/{id}',                  ['as' => 'normal.enquiry.update',                    'uses' => 'Normal\EnquiryController@update']);
    Route::get('enquiry/delete/{id}',                   ['as' => 'normal.enquiry.delete',                    'uses' => 'Normal\EnquiryController@destroy']);
    Route::get('enquiry/print',                         ['as' => 'normal.enquiry.print',                     'uses' => 'Normal\EnquiryController@exportAsPrint']);
    Route::get('enquiry/pdf',                           ['as' => 'normal.enquiry.pdf',                       'uses' => 'Normal\EnquiryController@exportAsPdf']);
    Route::get('enquiry/excel',                         ['as' => 'normal.enquiry.excel',                     'uses' => 'Normal\EnquiryController@exportAsExcel']);

    Route::get('register/list',                         ['as' => 'normal.register.list',                     'uses' => 'Normal\RegisterController@index']);
    Route::get('register/view/{id}',                    ['as' => 'normal.register.view',                     'uses' => 'Normal\RegisterController@view']);
    Route::get('register/print',                        ['as' => 'normal.register.print',                    'uses' => 'Normal\RegisterController@exportAsPrint']);
    Route::get('register/pdf',                          ['as' => 'normal.register.pdf',                      'uses' => 'Normal\RegisterController@exportAsPdf']);
    Route::get('register/excel',                        ['as' => 'normal.register.excel',                    'uses' => 'Normal\RegisterController@exportAsExcel']);

    Route::get('agreement/list',                        ['as' => 'normal.agreement.list',                    'uses' => 'Normal\AgreementController@index']);
    Route::get('agreement/view/{id}',                   ['as' => 'normal.agreement.view',                    'uses' => 'Normal\AgreementController@view']);
    Route::get('agreement/print',                       ['as' => 'normal.agreement.print',                   'uses' => 'Normal\AgreementController@exportAsPrint']);
    Route::get('agreement/pdf',                         ['as' => 'normal.agreement.pdf',                     'uses' => 'Normal\AgreementController@exportAsPdf']);
    Route::get('agreement/excel',                       ['as' => 'normal.agreement.excel',                   'uses' => 'Normal\AgreementController@exportAsExcel']);
    Route::get('agreement/agreementView',               ['as' => 'normal.agreement.agreementView',           'uses' => 'Normal\AgreementController@agreementView']);

    Route::get('college/list',                          ['as' => 'normal.college.list',                      'uses' => 'Normal\CollegeController@index']);
    Route::get('college/print',                         ['as' => 'normal.college.print',                     'uses' => 'Normal\CollegeController@exportAsPrint']);
    Route::get('college/pdf',                           ['as' => 'normal.college.pdf',                       'uses' => 'Normal\CollegeController@exportAsPdf']);
    Route::get('college/excel',                         ['as' => 'normal.college.excel',                     'uses' => 'Normal\CollegeController@exportAsExcel']);

    Route::get('course/list',                           ['as' => 'normal.course.list',                       'uses' => 'Normal\CourseController@index']);
    Route::get('course/print',                          ['as' => 'normal.course.print',                      'uses' => 'Normal\CourseController@exportAsPrint']);
    Route::get('course/pdf',                            ['as' => 'normal.course.pdf',                        'uses' => 'Normal\CourseController@exportAsPdf']);
    Route::get('course/excel',                          ['as' => 'normal.course.excel',                      'uses' => 'Normal\CourseController@exportAsExcel']);

});









