<?php

namespace App\Models;


class Agreement extends BaseModel
{
    public $timestamps = false;
    protected $table = 'agreement';

    protected $fillable = [
        'id', 'user_id', 'register_id', 'agreement_date', 'grand_father_name', 'district', 'vdc_municipality','ward_no',
        'tole', 'witness1_name', 'witness2_name', 'first_party', 'year', 'month', 'day', 'bar', 'is_deleted'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function student_registration()
    {
        return $this->belongsTo('App\Models\StudentRegistration');
    }


}
