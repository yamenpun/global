<?php

namespace App\Models;


class Branch extends BaseModel
{
    protected $table = 'branch';

    protected $fillable = [
        'id', 'branch_name', 'branch_address', 'branch_phone','branch_email', 'is_deleted','created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->hasMany('App\User');
    }

    public function student_enquiry()
    {
        return $this->hasMany('App\Models\StudentEnquiry');
    }

    public function student_registration()
    {
        return $this->hasMany('App\Models\StudentRegistration');
    }
    
}
