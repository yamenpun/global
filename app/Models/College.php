<?php

namespace App\Models;


class College extends BaseModel
{
    protected $table = 'college';

    protected $fillable = [
        'id', 'college_name', 'college_address', 'college_phone','college_country',
        'remarks', 'is_deleted', 'created_at', 'updated_at'
    ];
    
    public function student_registration()
    {
        return $this->hasMany('App\Models\StudentRegistration');
    }
}
