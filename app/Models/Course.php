<?php

namespace App\Models;


class Course extends BaseModel
{
    public $timestamps = false;
    protected $table = 'course';

    protected $fillable = [
        'id', 'course_name', 'course_remarks', 'is_deleted', 'created_at', 'updated_at'
    ];

    public function student_registration()
    {
        return $this->hasMany('App\Models\StudentRegistration');
    }
    
}
