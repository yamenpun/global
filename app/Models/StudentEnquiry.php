<?php

namespace App\Models;


class StudentEnquiry extends BaseModel
{
    public $timestamps = false;
    protected $table = 'student_enquiry';

    protected $fillable = [
        'id', 'branch_id','user_id', 'student_first_name', 'student_middle_name', 'student_last_name','email',
        'home_phone', 'mobile_phone', 'slc_percentage', 'plus_two_percentage', 'year_of_completion_slc', 'year_of_completion_plus_two',
        'language', 'language_score', 'guardian_first_name', 'guardian_middle_name', 'guardian_last_name', 'guardian_email', 'guardian_phone',
        'guardian_mobile', 'guardian_relationship', 'interested_course', 'preferred_place', 'how_know_us', 'enquiry_date', 'is_registered',
        'is_counselled', 'counsellor_id', 'counselling_date', 'counsellor_remarks', 'is_deleted',
    ];

    public function branch()
    {
        return $this->belongsTo('App\Models\Branch');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function student_registration()
    {
        return $this->hasMany('App\Models\StudentRegistration');
    }
}
