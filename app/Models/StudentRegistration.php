<?php

namespace App\Models;


class StudentRegistration extends BaseModel
{
    public $timestamps = false;
    protected $table = 'student_registration';

    protected $fillable = [
        'id', 'branch_id', 'user_id', 'college_id', 'student_id', 'course_id', 'intake',
        'father_name', 'father_occupation', 'mother_name', 'mother_occupation', 'date_of_birth', 'permanent_address', 'temporary_address',
        'emergency_contact_number', 'toefl_score', 'ielts_score', 'gre_score', 'gmat_score', 'sat_score', 'is_signature_student', 'is_signature_parent',
        'signature_date', 'register_date', 'is_deleted',
    ];

    public function branch()
    {
        return $this->belongsTo('App\Models\Branch');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function student_enquiry()
    {
        return $this->belongsTo('App\Models\StudentEnquiry');
    }

    public function college()
    {
        return $this->belongsTo('App\Models\College');
    }

    public function course()
    {
        return $this->belongsTo('App\Models\Course');
    }

    public function agreement()
    {
        return $this->hasMany('App\Models\Agreement');
    }


}
