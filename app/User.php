<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    protected $fillable = [
        'fullname', 'username', 'email', 'role', 'password', 'status', 'created_at', 'updated_at', 'branch_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function branch()
    {
        return $this->belongsTo('App\Branch');
    }

    public function student_enquiry()
    {
        return $this->hasMany('App\Models\StudentEnquiry');
    }

    public function student_registration()
    {
        return $this->hasMany('App\Models\StudentRegistration');
    }

    public function agreement()
    {
        return $this->hasMany('App\Models\Agreement');
    }

    public function activity_log()
    {
        return $this->hasMany('App\Models\ActivityLog');
    }

}
