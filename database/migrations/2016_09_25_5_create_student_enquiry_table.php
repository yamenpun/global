<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentEnquiryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_enquiry', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('branch_id');
            $table->unsignedInteger('user_id');
            $table->date('enquiry_date');
            $table->string('student_first_name', 255);
            $table->string('student_middle_name', 255);
            $table->string('student_last_name', 255);
            $table->string('email', 255)->nullable();
            $table->string('home_phone', 15)->nullable();
            $table->string('mobile_phone', 15);
            $table->double('slc_percentage', 10, 2)->default(0);
            $table->double('plus_two_percentage', 10, 2)->default(0);
            $table->integer('year_of_completion_slc')->default(0);
            $table->integer('year_of_completion_plus_two')->default(0);
            $table->string('language', 255)->nullable();
            $table->integer('language_score')->default(0);
            $table->string('guardian_first_name', 255);
            $table->string('guardian_middle_name', 255);
            $table->string('guardian_last_name', 255);
            $table->string('guardian_email', 255)->nullable();
            $table->string('guardian_phone', 15)->nullable();
            $table->string('guardian_mobile', 15);
            $table->string('guardian_relationship', 255)->nullable();
            $table->string('interested_course', 255);
            $table->string('preferred_place', 255);
            $table->string('how_know_us', 255)->nullable();
            $table->boolean('is_registered')->default(0);
            $table->boolean('is_counselled')->default(0);
            $table->unsignedInteger('counsellor_id')->nullable();
            $table->date('counselling_date')->nullable();
            $table->string('counsellor_remarks')->nullable();
            $table->boolean('is_deleted')->default(0);

            // Foreign Key
            $table->foreign('branch_id')->references('id')->on('branch')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('student_enquiry');
    }
}
