<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentRegistrationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_registration', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('branch_id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('student_id');
            $table->unsignedInteger('college_id');
            $table->unsignedInteger('course_id');
            $table->string('intake', 255);
            $table->date('date_of_birth');
            $table->string('father_name', 255);
            $table->string('father_occupation', 255);
            $table->string('mother_name', 255);
            $table->string('mother_occupation', 255);
            $table->string('permanent_address', 255);
            $table->string('temporary_address', 255);
            $table->string('emergency_contact_number', 20);
            $table->double('toefl_score', 10, 2);
            $table->double('ielts_score', 10, 2);
            $table->double('gre_score', 10, 2);
            $table->double('gmat_score', 10, 2);
            $table->double('sat_score', 10, 2);
            $table->boolean('is_signature_student')->default(0);
            $table->boolean('is_signature_parent')->default(0);
            $table->date('signature_date');
            $table->date('register_date');
            $table->boolean('is_agreed')->default(0);
            $table->boolean('is_deleted')->default(0);
            
            // Foreign Key
            $table->foreign('branch_id')->references('id')->on('branch')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('student_id')->references('id')->on('student_enquiry')->onDelete('cascade');
            $table->foreign('college_id')->references('id')->on('college')->onDelete('cascade');
            $table->foreign('course_id')->references('id')->on('course')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('student_registration');
    }
}
