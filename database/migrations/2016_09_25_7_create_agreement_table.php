<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgreementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agreement', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('register_id');
            $table->date('agreement_date');
            $table->string('grand_father_name', 255);
            $table->string('district', 255);
            $table->string('vdc_municipality', 255);
            $table->integer('ward_no')->default(0);
            $table->string('tole', 255);
            $table->string('witness1_name', 255);
            $table->string('witness2_name', 255);
            $table->string('first_party', 255);
            $table->string('year', 20);
            $table->string('month', 20);
            $table->string('day', 20);
            $table->string('bar', 20);
            $table->double('is_deleted')->default(0);

            // Foreign Key
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('register_id')->references('id')->on('student_registration')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('agreement');
    }
}
