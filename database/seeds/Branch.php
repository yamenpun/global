<?php

use Illuminate\Database\Seeder;

class Branch extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('branch')->insert([
            'id'             => '1',
            'branch_name'    => 'Bagbazar',
            'branch_address' => 'Bagbazar, Kathmandu',
            'branch_phone'   => '01-4215762',
            'branch_email'   => 'globaleducationcc@gmail.com',
            'created_at'     => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'     => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('branch')->insert([
            'id'             => '2',
            'branch_name'    => 'Pokhara',
            'branch_address' => 'Mahendrapul, Pokhara',
            'branch_phone'   => '061-534343',
            'branch_email'   => 'globalpokhara@gmail.com',
            'created_at'     => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'     => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('branch')->insert([
            'id'             => '3',
            'branch_name'    => 'Chitwan',
            'branch_address' => 'Chitwan',
            'branch_phone'   => '056-545454',
            'branch_email'   => 'globalchitwan@gmail.com',
            'created_at'     => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'     => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

    }
}
