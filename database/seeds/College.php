<?php

use Illuminate\Database\Seeder;

class College extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('college')->insert([
            'id'                => '1',
            'college_name'      => 'Kings College',
            'college_address'   => 'Babarmahal, Kathmandu',
            'college_phone'     => '01-4215762',
            'college_country'   => 'Nepal',
            'created_at'        => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'        => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('college')->insert([
            'id'                => '2',
            'college_name'      => 'Informatics College',
            'college_address'   => 'Lions Marga, Pokhara',
            'college_phone'     => '061-538115',
            'college_country'   => 'Nepal',
            'created_at'        => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'        => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('college')->insert([
            'id'                => '3',
            'college_name'      => 'Islington College',
            'college_address'   => 'Kamalpokhari, Kathmandu',
            'college_phone'     => '01-4343434',
            'college_country'   => 'Nepal',
            'created_at'        => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'        => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

    }
}
