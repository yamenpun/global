<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(Branch::class);
        $this->call(Users::class);
        $this->call(College::class);
        $this->call(Course::class);
        $this->call(Enquiry::class);
    }
}
