<?php

use Illuminate\Database\Seeder;

class Enquiry extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $limit = 10;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('student_enquiry')->insert([
                'branch_id'                     => '1',
                'user_id'                       => '1',
                'student_first_name'            => $faker->firstName,
                'student_last_name'             => $faker->lastName,
                'email'                         => $faker->unique()->email,
                'home_phone'                    => $faker->phoneNumber,
                'mobile_phone'                  => $faker->phoneNumber,
                'slc_percentage'                => rand(60, 90),
                'plus_two_percentage'           => rand(60, 90),
                'year_of_completion_slc'        => '2010',
                'year_of_completion_plus_two'   => '2012',
                'language'                      => 'ielts',
                'language_score'                => '7',
                'guardian_first_name'           => $faker->firstName,
                'guardian_last_name'            => $faker->lastName,
                'guardian_email'                => $faker->unique()->email,
                'guardian_phone'                => $faker->phoneNumber,
                'guardian_mobile'               => $faker->phoneNumber,
                'guardian_relationship'         => 'father',
                'interested_course'             => 'BIT',
                'preferred_place'               => 'Australia',
                'how_know_us'                   => 'Internet',
                'enquiry_date'                  => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }

    }
}
