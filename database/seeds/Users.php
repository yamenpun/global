<?php

use Illuminate\Database\Seeder;

class Users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'fullname'      => 'Super Admin',
            'username'      => 'superadmin',
            'email'         => 'superadmin@gmail.com',
            'password'      => bcrypt('superadmin'),
            'role'          => 'superadmin',
            'branch_id'     => '1',
        ]);

        DB::table('users')->insert([
            'fullname'      => 'admin',
            'username'      => 'admin',
            'email'         => 'admin@gmail.com',
            'password'      => bcrypt('admin'),
            'role'          => 'admin',
            'branch_id'     => '1',
        ]);

        DB::table('users')->insert([
            'fullname'      => 'Counselor',
            'username'      => 'counselor',
            'email'         => 'counselor@gmail.com',
            'password'      => bcrypt('counselor'),
            'role'          => 'counselor',
            'branch_id'     => '1'
        ]);

        DB::table('users')->insert([
            'fullname'      => 'Normal',
            'username'      => 'normal',
            'email'         => 'normal@gmail.com',
            'password'      => bcrypt('normal'),
            'role'          => 'normal',
            'branch_id'     => '1'
        ]);


    }
}
