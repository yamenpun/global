<!DOCTYPE html>

<html>

    <head>

        <title>Convert To PDF</title>

        <style>

            table {
                border-collapse: collapse;
                width: 100%;
            }

            th, td {
                text-align: left;
                padding: 8px;
                font-size: 12px;
            }

        </style>

    </head>

    <body>

        <div style="overflow-x:auto;">

            <h2 style="text-align: center">Registered Student List</h2>

            <table>

                <tr>
                    <th style="width: 10px;">S.N.</th>

                    <th style="width: 90px;">Agreement Date</th>

                    <th style="width: 100px;">Student Name</th>

                    <th style="width: 130px;">Full Address</th>

                    <th style="width: 100px;">College Name</th>

                    <th style="width: 80px;">Course Name</th>

                    <th style="width: 80px;">Branch Name</th>

                </tr>

                <?php $i = 0 ?>

                @foreach($data['rows'] as $row)

                    <?php $i++ ?>

                    <tr>

                        <td>{{ $i }}</td>

                        <td>{{ $row->agreement_date }}</td>

                        <td>{{ $row->student_first_name }} {{ $row->student_middle_name }} {{ $row->student_last_name }}</td>

                        <td>{{ $row->vdc_municipality }}-{{ $row->ward_no }}, {{ $row->tole }}, {{ $row->district }}</td>

                        <td>{{ $row->college_name }}</td>

                        <td>{{ $row->course_name }}</td>

                        <td>{{ $row->branch_name }}</td>

                    </tr>

                @endforeach

            </table>

        </div>

    </body>

</html>
