@extends('admin.layouts.master')

@section('title')
    Agreement
@endsection

@section('page_specific_style')

    <link href="{{ asset('assets/admin/css/datepicker.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/admin/css/chosen.css') }}" rel="stylesheet"/>

@endsection

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">Home</a>
                </li>

                <li>
                    <a href="{{ route($base_route.'.list') }}">Agreement List</a>
                </li>

                <li class="active">Add Form</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    <small>
                        <i class="icon-double-angle-right"></i>
                        Agreement Detail
                    </small>

                    <div class="btn-group pull-right">
                        <a href="{{ URL::previous()}}" class="btn btn-pink btn-sm pull-right">
                            <i class="icon-backward bigger-110"></i>
                            Go Back
                        </a>
                    </div>

                </h1>

            </div>

            <div class="row-fluid">

                <div class="col-sm-12">

                    @if (session()->has('message'))
                        {!! session()->get('message') !!}
                    @endif

                    {!! Form::open([
                    'route'     => $base_route.'.store',
                    'method'    => 'post',
                    'class'     => 'form-horizontal',
                    'role'      => "form",
                    'enctype'   => "multipart/form-data"
                    ]) !!}

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <?php $url = Request::fullUrl(); $array = explode('=', $url); $id = array_pop($array); ?>

                    <input id="register_id" name="register_id" type="hidden" value="{{ $id }}">

                    <div class="form-group">

                        <div class="col-sm-11" style="padding-left: 80px; text-align: justify;">

                            <div>

                                <p style="font-size: 14px; text-align: justify;">
                                    लिखितम् का.जि.का.म.न.पा वडा नं. ३१ बागबजार स्थित ग्लोबल एजुकेशन काउन्सिलिङ्ग
                                    सेन्टर प्रा.लि. को तर्फबाट अख्तियार प्राप्त संस्था ( जसलाई यसपछि प्रथम पक्ष भनि
                                    सम्बोधन गरिएको )
                                    र {!! Form::text('grand_father_name', null, [
                                       'id'                                 => 'grand_father_name',
                                       "placeholder"                        => "हजुर बाबाको नाम",
                                    ]) !!} को नाति <b>{{ $data['registration']['0']->father_name }}</b> को छोरा {!! Form::text('district', null, [
                                       'id'                                 => 'district',
                                       "placeholder"                        => "जिल्लाको नाम",
                                    ]) !!} जिल्ला {!! Form::text('vdc_municipality', null, [
                                       "'kl_virtual_keyboard_secure_input"  => "on",
                                       'id'                                 => 'vdc_municipality',
                                       "placeholder"                        => "न.पा./गा.वि.स. को नाम",
                                    ]) !!} न.पा./गा.वि.स. वडा नं {!! Form::text('ward_no', null, [
                                       'id'                                 => 'ward_no',
                                       "placeholder"                        => "वडा नं",
                                    ]) !!} , {!! Form::text('tole', null, [
                                       'id'                                 => 'tole',
                                       "placeholder"                        => "टोलको नाम",
                                    ]) !!} मा बस्ने बर्ष
                                    <?php $dob = strtotime($data['registration']['0']->date_of_birth ); $current_time = time(); ?>
                                    <b>{{ $age_years = date('Y',$current_time) - date('Y',$dob) }}</b> को <b>{{ $data['registration']['0']->student_first_name }} {{ $data['registration']['0']->student_middle_name }} {{ $data['registration']['0']->student_last_name }}</b> ( जसलाई यसपछि दोस्रो पक्ष भनि सम्बोधन गरिएको ) आगे हामी दुवै
                                    पक्षहरुका बीच तरसिलका शर्तहरुमा करार
                                    गर्न मञ्जुर भएकाले करार ऐन, २०५६ को दफा ५ बमोजिम यो करारनामाको कागज तयार गरि
                                    हाम्रा स्वतन्त्र मनोमान राजिखुशीसँग तपसिलका साक्षीका रोहबरमा यो करारनामामा
                                    हस्ताक्षर गरि एक/एक
                                    प्रति लियौं/दियौँ । मुकाम प्रथम पक्षको अफिस ।

                                </p>

                                <p style="font-size: 14px; text-align: justify;">तपसिल</p>
                                <p style="font-size: 14px; text-align: justify;">शर्तहरु :</p>


                            </div>

                            <div>

                                <ul style="font-size: 14px; text-align: justify; list-style: decimal;">
                                    <li>प्रथम पक्षले <b>{{ $data['registration']['0']->college_country }}</b> स्थित <b>{{ $data['registration']['0']->college_name }}</b> मा <b>{{ $data['registration']['0']->course_name }}</b> विषयको
                                        कोर्ष अध्ययन गर्नका लागि म दोस्रो पक्षलाई दिइएको परामर्श सेवा शुल्क तथा
                                        शिक्षण संस्थामा तिर्नुपर्ने अन्य शुल्कमा म दोस्रो पक्षको चित्त बुझ्यो ।
                                    </li>
                                    <li>प्रथम पक्षले परामर्श दिएको <b>{{ $data['registration']['0']->college_country }}</b> स्थित <b>{{ $data['registration']['0']->college_name }}</b> मा <b>{{ $data['registration']['0']->course_name }}</b> कोर्षको लागि प्रकरण नं. १
                                        बमोजिम लाग्ने शुल्क तिरी अध्ययन गर्न म दोस्रो पक्ष मञ्जुर गर्दछु ।
                                    </li>
                                    <li>प्रकरण नं. १ को शिक्षण संस्थामा अध्ययन गर्दा लाग्ने शुल्क, छात्राबास शुल्क,
                                        परिक्षा शूल्क लगायतका नियम अनुसार लाग्ने शुल्क तथा दोस्रो पक्षले शिक्षण
                                        संस्थामा जाँदा आउँदा लाग्ने हवाई तथा यातायात खर्च, होटल-लज खर्च, प्रथम
                                        पक्षले भर्ना प्रकृयाको लागि गरेको पत्राचार खर्च दोस्रो पक्षले नै
                                        तिर्नुपर्नेछ ।
                                    </li>
                                    <li>प्रकरण नं. १ को शिक्षण संस्थामा अध्ययन गर्ने अनुमति पाएपछि वा भर्ना भई
                                        सकेपछि दोस्रो पक्षले उक्त शिक्षण संस्थामा अध्ययन नगरेमा वा अन्य शिक्षण
                                        संस्थामा गई अध्ययन गरेमा अध्ययन अनुमति वा भर्ना गर्दाको अवधिसम्म प्रथम
                                        पक्षले बुझि लिएको परामर्श शुल्क, सेवा शुल्क, धरौटी लगायतका कुनै पनि रुपैयाको
                                        दाबी दोस्रो पक्षले प्रथम पक्षसँग गर्न पाउने छैन ।
                                    </li>
                                    <li>करारमा उल्लेखित शिक्षण संस्थामा भर्ना भई अध्ययनका क्रममा शैक्षिक प्रमाणपत्र
                                        पेश गरेको कारण शिक्षण संस्थाले निष्काशित गरेमा, शैक्षिक सत्र समाप्त नहुँदै
                                        बिचैमा अध्ययन छाडेमा वा शिक्षण संस्थाको नियम उल्लंघन गरी निष्काशित भएमा
                                        दोस्रो पक्षले प्रथम पक्ष उपर कुनै पनि प्रकारको आर्थिक दाबी लिन पाउने छैन ।
                                    </li>
                                    <li>प्रथम पक्ष आफ्नो सेवा अन्तर्गत परामर्श, अफर लेटर, सेलेक्सन लेटर, आई
                                        ट्वान्टी, कोटा रिजर्भेशनको काम गरि कलेज भर्ना गराई दिने मात्र कार्य गर्ने छ
                                        । सोका लागि प्रथम पक्षले परामर्श तथा सेवा लगायतको निश्चित रुपैँया सेवा शुल्क
                                        वापत लिने छ ।
                                    </li>
                                    <li>शिक्षण संस्थामा भर्ना भई सकेपछि शिक्षण संस्थाको आन्तरिक समस्या वा सरकार र
                                        शिक्षण संस्थाको आन्तरिक समस्याले गर्दा अध्ययनको समयावधि बढेमा वा परिक्षा दिन
                                        नपाएमा त्यसको जिम्मेवार प्रथम पक्ष हुने छैन ।
                                    </li>
                                    <li>सिट रिजर्भेसनका लागि शिक्षण संस्थालाई रुपैयाँ बुझाई सकेपछि दोस्रो पक्ष
                                        अध्ययन गर्न नगएमा उक्त रुपैयाँ दोस्रो पक्षले फिर्ता पाउने छैन ।
                                    </li>
                                    <li>यो करारनामा मिति
                                        <div class="input-group">

                                            {{ Form::date('agreement_date', \Carbon\Carbon::now(), array('id' => 'id-date-picker-1', 'class' => 'form-control date-picker' )) }}

                                            <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>

                                        </div>
                                        बाट लागु भएको छ, यो करारनामा एक शैक्षिक सत्रको (भर्ना भएको कोर्षको शैक्षिक
                                        अवधि ) सम्म मात्र बैध मानिने छ ।
                                    </li>
                                </ul>

                            </div>

                            <br/>

                            <div>

                                साक्षी:-
                                {!! Form::text('witness1_name', null, [
                                       "'kl_virtual_keyboard_secure_input"  => "on",
                                       'id'                                 => 'witness1_name',
                                       "placeholder"                        => "साक्षी १",
                                       "class"                              => "col-xs-10 col-sm-12",
                                    ]) !!}

                                <div class="space-4"></div>

                                {!! Form::text('witness2_name', null, [
                                       "'kl_virtual_keyboard_secure_input"  => "on",
                                       'id'                                 => 'witness2_name',
                                       "placeholder"                        => "साक्षी २",
                                       "class"                              => "col-xs-10 col-sm-12",
                                    ]) !!}

                            </div>

                            <br/><br/><br/><br/>

                            <div>
                                <table border="0px" width="900px">
                                    <tr>
                                        <td>प्रथम पक्ष</td>
                                        <td>दोस्रो पक्ष</td>
                                    </tr>
                                    <tr>
                                        <td>नाम: {!! Form::text('first_party', null, ['id' => 'first_party', "placeholder" => "पुरा नाम", ]) !!}</td>
                                        <td>नाम: <b>{{ $data['registration']['0']->student_first_name }} {{ $data['registration']['0']->student_middle_name }} {{ $data['registration']['0']->student_last_name }}</b></td>
                                    </tr>
                                </table>
                            </div>

                            <br/>

                            <div>

                                <p>
                                    इति सम्बत् &nbsp;&nbsp;&nbsp;<input name="year" value="{{ date('Y') }}" type="text"
                                                                        style="width:50px;"/> साल
                                    &nbsp;&nbsp;&nbsp;<input name="month" value="{{ date('m') }}" type="text"
                                                             style="width:30px;"/> महिना
                                    &nbsp;&nbsp;&nbsp;<input name="day" value="{{ date('d') }}" type="text"
                                                             style="width:30px;"/> गते
                                    &nbsp;&nbsp;&nbsp; रोज <input name="bar" type="text" value="{{ (date('w')+1) }}"
                                                                  style="width:30px;"/> &nbsp;&nbsp; शुभम्
                                </p>

                            </div>

                        </div>

                    </div>


                    <div class="clearfix form-actions">

                        <div class="col-md-offset-4 col-md-8">

                            <button class="btn btn-primary" type="submit">
                                <i class="icon-ok bigger-110"></i>
                                Save
                            </button>

                            &nbsp; &nbsp; &nbsp;

                            <button class="btn btn-primary" type="reset">
                                <i class="icon-undo bigger-110"></i>
                                Reset
                            </button>

                        </div>

                    </div>

                    {!! Form::close() !!}

                </div>

            </div>

        </div>

    </div>

@endsection
@section('page_specific_scripts')

    <script src="{{ asset('assets/admin/js/bootstrap-datepicker.min.js') }}"></script>

    <script type="text/javascript">
        jQuery(function ($) {
            $('.date-picker').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            }).next().on(ace.click_event, function () {
                $(this).prev().focus();
            });

            $(function () {
                // bind change event to select
                $('#student_id').on('change', function () {
                    var student_id = $(this).val(); // get selected value
                    if (student_id) { // require a URL
                        window.location = "<?php echo route($base_route . '.add'); ?>" + "?id=" + student_id; // redirect
                    }
                    return false;
                });
            });

        });

        $('#college_id').on('change', function (e) {

            console.log(e);

            var college_id = e.target.value;

            // Ajax
            $.get('/ajax-course?college_id=' + college_id, function (data) {

                // Success data
                $('#course_applied_for').empty();

                $.each(data, function (index, courseObj) {

                    $('#course_applied_for').append('<option value="' + courseObj.course_name + '">' + courseObj.course_name + '</option>');

                })

            });
        });

    </script>

@endsection