<!DOCTYPE html>

<html>

    <head>

        <title>Convert To PDF</title>

        <style>

            table {
                border-collapse: collapse;
                width: 100%;
            }

            th, td {
                text-align: left;
                padding: 8px;
                font-size: 12px;
            }

        </style>

    </head>

    <body>

        <div style="overflow-x:auto;">

            <h2 style="text-align: center">Deleted College List</h2>

            <table>

                <tr>
                    <th style="width: 10px;">S.N.</th>

                    <th style="width: 120px;">College Name</th>

                    <th style="width: 120px;">Address</th>

                    <th style="width: 100px;">Phone Number</th>

                    <th style="width: 100px;">Country</th>

                    <th style="width: 100px;">Remarks</th>

                </tr>

                <?php $i = 0 ?>

                @foreach($data['rows'] as $row)

                    <?php $i++ ?>

                    <tr>

                        <td>{{ $i }}</td>

                        <td>{{ $row->college_name }}</td>

                        <td>{{ $row->college_address }}</td>

                        <td>{{ $row->college_phone }}</td>

                        <td>{{ $row->college_country }}</td>

                        <td>
                            @if($row->remarks)
                                {{ $row->remarks }}
                            @else
                                N/A
                            @endif
                        </td>

                    </tr>

                @endforeach

            </table>

        </div>

    </body>

</html>
