<div class="sidebar" id="sidebar">

    <script type="text/javascript">
        try {
            ace.settings.check('sidebar', 'fixed')
        } catch (e) {
        }
    </script>

    <ul class="nav nav-list">

        <li {!! Request::is('admin/dashboard')?'class="active"':'' !!}>

            <a href="{{ route('admin.dashboard') }}">
                <i class="icon-dashboard"></i>
                <span class="menu-text"> Dashboard </span>
            </a>

        </li>

        <li {!! Request::is('admin/enquiry/list*') || Request::is('admin/enquiry/view*') || Request::is('admin/enquiry/deletedView*') || Request::is('admin/enquiry/add*') || Request::is('admin/enquiry/edit*') || Request::is('admin/enquiry/deletedList*')?'class="active open"':'' !!}>

            <a class="dropdown-toggle" href="#">
                <i class="icon-plus-sign"></i>
                <span class="menu-text"> Student Enquiry </span>
                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu" style="display: no">

                <li {!! Request::is('admin/enquiry/list*') || Request::is('admin/enquiry/view*') || Request::is('admin/enquiry/add*') || Request::is('admin/enquiry/edit*')?'class="active"':'' !!}>

                    <a href="{{ route('admin.enquiry.list') }}">
                        <i class="icon-double-angle-right"></i>
                        Student Enquiry List
                    </a>

                </li>

                <li {!! Request::is('admin/enquiry/deletedList*') || Request::is('admin/enquiry/deletedView*')?'class="active"':'' !!}>

                    <a href="{{ route('admin.enquiry.deletedList') }}">
                        <i class="icon-double-angle-right"></i>
                        Deleted Enquiry List
                    </a>

                </li>

            </ul>

        </li>


        <li {!! Request::is('admin/register/list*') || Request::is('admin/register/view*') || Request::is('admin/register/deletedView*') || Request::is('admin/register/add*') || Request::is('admin/register/newPatient*') || Request::is('admin/register/edit*') || Request::is('admin/register/deletedList*')?'class="active open"':'' !!}>

            <a class="dropdown-toggle" href="#">
                <i class="icon-edit"></i>
                <span class="menu-text"> Registration </span>
                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu" style="display: no">

                <li {!! Request::is('admin/register/list*') || Request::is('admin/register/view*') || Request::is('admin/register/add*') || Request::is('admin/register/edit*')?'class="active"':'' !!}>

                    <a href="{{ route('admin.register.list') }}">
                        <i class="icon-double-angle-right"></i>
                        Registration List
                    </a>

                </li>

                <li {!! Request::is('admin/register/deletedList*') || Request::is('admin/register/deletedView*')?'class="active"':'' !!}>

                    <a href="{{ route('admin.register.deletedList') }}">
                        <i class="icon-double-angle-right"></i>
                        Deleted Registration List
                    </a>

                </li>

            </ul>

        </li>

        <li {!! Request::is('admin/agreement/list*') || Request::is('admin/agreement/view*') || Request::is('admin/agreement/deletedView*') || Request::is('admin/agreement/add*') || Request::is('admin/agreement/edit*') || Request::is('admin/agreement/deletedList*')?'class="active open"':'' !!}>

            <a class="dropdown-toggle" href="#">
                <i class="icon-key"></i>
                <span class="menu-text"> Agreement </span>
                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu" style="display: no">

                <li {!! Request::is('admin/agreement/list*') || Request::is('admin/agreement/view*') || Request::is('admin/agreement/add*') || Request::is('admin/agreement/edit*')?'class="active"':'' !!}>

                    <a href="{{ route('admin.agreement.list') }}">
                        <i class="icon-double-angle-right"></i>
                        Agreement List
                    </a>

                </li>

                <li {!! Request::is('admin/agreement/deletedList*') || Request::is('admin/agreement/deletedView*')?'class="active"':'' !!}>

                    <a href="{{ route('admin.agreement.deletedList') }}">
                        <i class="icon-double-angle-right"></i>
                        Deleted Agreement List
                    </a>

                </li>

            </ul>

        </li>

        <li {!! Request::is('admin/college/list*') || Request::is('admin/college/deletedView*') || Request::is('admin/college/add*') || Request::is('admin/college/edit*') || Request::is('admin/college/deletedList*')?'class="active open"':'' !!}>

            <a class="dropdown-toggle" href="#">
                <i class="icon-home"></i>
                <span class="menu-text"> College </span>
                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu" style="display: no">

                <li {!! Request::is('admin/college/list*') || Request::is('admin/college/add*') || Request::is('admin/college/edit*')?'class="active"':'' !!}>

                    <a href="{{ route('admin.college.list') }}">
                        <i class="icon-double-angle-right"></i>
                        College List
                    </a>

                </li>

                <li {!! Request::is('admin/college/deletedList*') ?'class="active"':'' !!}>

                    <a href="{{ route('admin.college.deletedList') }}">
                        <i class="icon-double-angle-right"></i>
                        Disabled College List
                    </a>

                </li>

            </ul>

        </li>

        <li {!! Request::is('admin/course/list*') || Request::is('admin/course/deletedView*') || Request::is('admin/course/add*') || Request::is('admin/course/edit*') || Request::is('admin/course/deletedList*')?'class="active open"':'' !!}>

            <a class="dropdown-toggle" href="#">
                <i class="icon-certificate"></i>
                <span class="menu-text"> Course </span>
                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu" style="display: no">

                <li {!! Request::is('admin/course/list*') || Request::is('admin/course/add*') || Request::is('admin/course/edit*')?'class="active"':'' !!}>

                    <a href="{{ route('admin.course.list') }}">
                        <i class="icon-double-angle-right"></i>
                        Course List
                    </a>

                </li>

                <li {!! Request::is('admin/course/deletedList*') ?'class="active"':'' !!}>

                    <a href="{{ route('admin.course.deletedList') }}">
                        <i class="icon-double-angle-right"></i>
                        Disabled Course List
                    </a>

                </li>

            </ul>

        </li>

        <li {!! Request::is('admin/log/list*')?'class="active"':'' !!}>

            <a href="{{ route('admin.log.list') }}">
                <i class="icon-exclamation-sign"></i>
                <span class="menu-text"> Activity Log List </span>
            </a>

        </li>

        <li {!! Request::is('admin/user*')?'class="active"':'' !!}>

            <a href="{{ route('admin.user') }}">
                <i class="icon-user"></i>
                <span class="menu-text"> User </span>
            </a>

        </li>

        <li>

            <a href="{{ url('logout') }}">
                <i class="icon-signout"></i>
                <span class="menu-text"> LogOut </span>
            </a>

        </li>

    </ul>

    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>

    <script type="text/javascript">

        try {
            ace.settings.check('sidebar', 'collapsed')
        } catch (e) {
        }

    </script>

</div>

