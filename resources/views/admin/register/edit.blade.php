@extends('admin.layouts.master')

@section('title')
    Update Registered Student
@endsection

@section('page_specific_style')

    <link href="{{ asset('assets/admin/css/datepicker.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/admin/css/chosen.css') }}" rel="stylesheet"/>

@endsection

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">Home</a>
                </li>

                <li>
                    <a href="{{ route($base_route.'.list') }}">Registered Student List</a>
                </li>

                <li class="active">Update Form</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    <small>
                        <i class="icon-double-angle-right"></i>
                        Update Register Student Info
                    </small>

                    <div class="btn-group pull-right">
                        <a href="{{ URL::previous()}}" class="btn btn-pink btn-sm pull-right">
                            <i class="icon-backward bigger-110"></i>
                            Go Back
                        </a>
                    </div>

                </h1>

            </div>

            <div class="row-fluid">

                <div class="col-sm-12">

                    @if (session()->has('message'))
                        {!! session()->get('message') !!}
                    @endif

                        {!! Form::model($data['row'], [
                        'route'     => [$base_route.'.update', $data['row']->id],
                        'method'    => 'post',
                        'class'     => 'form-horizontal',
                        'role'      => "form",
                        'enctype'   => "multipart/form-data"
                        ]) !!}

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="form-group">

                            <label class="col-sm-2 control-label no-padding-right" for="student_id">Student Name</label>

                            <div class="col-sm-3">

                                <select readonly name="student_id" id="student_id" class="col-xs-10 col-sm-12">

                                    @foreach($data['student'] as $student)
                                        <?php
                                        $selected = false;

                                        if ($data['row']->student_id == $student->id) {
                                            $selected = true;
                                        }
                                        if (old('student_id')) {
                                            if (old('student_id') == $student->id) {
                                                $selected = true;
                                            } else {
                                                $selected = false;
                                            }
                                        }
                                        ?>
                                        <option value="{{ $student->id }}" {!! $selected?'selected=selected':'' !!}>{{ $student->student_first_name }} {{ $student->student_middle_name }}{{ $student->student_last_name }}</option>
                                    @endforeach

                                </select>

                            </div>

                            <label class="col-sm-2 control-label no-padding-right" for="register_date">Registration Date</label>

                            <div class="col-sm-3">

                                <div class="input-group">

                                    {{ Form::date('register_date', \Carbon\Carbon::now(), array('id' => 'id-date-picker-1', 'class' => 'form-control date-picker' )) }}

                                    <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>

                                </div>

                            </div>

                        </div>

                        <div class="space-4"></div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label no-padding-right" for="college_id">College Name</label>

                            <div class="col-sm-3">

                                <select kl_virtual_keyboard_secure_input="on" class="col-xs-10 col-sm-12" name="college_id" id="college_id" class="form-control">

                                    @foreach($data['college'] as $college)
                                        @if($data['row']->college_id === $college->id)
                                            <option value="{{ $college->id }}" selected="selected">{{ $college->college_name }}</option>
                                        @else
                                            <option value="{{ $college->id }}">{{ $college->college_name }}</option>
                                        @endif
                                    @endforeach

                                </select>

                            </div>

                            <label class="col-sm-2 control-label no-padding-right" for="course_applied_for">Preferred Course</label>

                            <div class="col-sm-3">

                                <select kl_virtual_keyboard_secure_input="on" class="col-xs-10 col-sm-12" name="course_applied_for"
                                        id="course_applied_for" class="form-control">

                                    @foreach($data['course'] as $course)
                                        @if($data['row']->course_id === $course->id)
                                            <option value="{{ $course->id }}" selected="selected">{{ $course->course_name }}</option>
                                        @else
                                            <option value="{{ $course->id }}">{{ $course->course_name }}</option>
                                        @endif
                                    @endforeach

                                </select>

                            </div>

                        </div>

                        <div class="space-4"></div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label no-padding-right" for="date_of_birth">Date Of Birth</label>

                            <div class="col-sm-3">

                                <div class="input-group">

                                    {{ Form::date('date_of_birth', \Carbon\Carbon::now(), array('id' => 'id-date-picker-1', 'class' => 'form-control date-picker' )) }}

                                    <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>

                                </div>

                            </div>

                            <label class="col-sm-2 control-label no-padding-right" for="intake">Preferred Intake</label>

                            <div class="col-sm-3">

                                {!! Form::text('intake', null, [
                                   "'kl_virtual_keyboard_secure_input"  => "on",
                                   'id'                                 => 'intake',
                                   "placeholder"                        => "Preferred Intake",
                                   "class"                              => "col-xs-10 col-sm-12",
                                ]) !!}

                                {!! AppHelper::getValidationErrorMsg($errors, 'intake') !!}

                            </div>

                        </div>

                        <div class="space-4"></div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label no-padding-right" for="father_name">Father's Name</label>

                            <div class="col-sm-3">

                                {!! Form::text('father_name', null, [
                                   "'kl_virtual_keyboard_secure_input"  => "on",
                                   'id'                                 => 'father_name',
                                   "placeholder"                        => "Father's Name",
                                   "class"                              => "col-xs-10 col-sm-12",
                                ]) !!}

                                {!! AppHelper::getValidationErrorMsg($errors, 'father_name') !!}

                            </div>

                            <label class="col-sm-2 control-label no-padding-right" for="father_occupation">Father's Occupation</label>

                            <div class="col-sm-3">

                                {!! Form::text('father_occupation', null, [
                                   "'kl_virtual_keyboard_secure_input"  => "on",
                                   'id'                                 => 'father_occupation',
                                   "placeholder"                        => "Father's Occupation",
                                   "class"                              => "col-xs-10 col-sm-12",
                                ]) !!}

                                {!! AppHelper::getValidationErrorMsg($errors, 'father_occupation') !!}

                            </div>

                        </div>

                        <div class="space-4"></div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label no-padding-right" for="mother_name">Mother's Name</label>

                            <div class="col-sm-3">

                                {!! Form::text('mother_name', null, [
                                   "'kl_virtual_keyboard_secure_input"  => "on",
                                   'id'                                 => 'mother_name',
                                   "placeholder"                        => "Mother's Name",
                                   "class"                              => "col-xs-10 col-sm-12",
                                ]) !!}

                                {!! AppHelper::getValidationErrorMsg($errors, 'mother_name') !!}

                            </div>

                            <label class="col-sm-2 control-label no-padding-right" for="mother_occupation">Mother's Occupation</label>

                            <div class="col-sm-3">

                                {!! Form::text('mother_occupation', null, [
                                   "'kl_virtual_keyboard_secure_input"  => "on",
                                   'id'                                 => 'mother_occupation',
                                   "placeholder"                        => "Mother's Occupation",
                                   "class"                              => "col-xs-10 col-sm-12",
                                ]) !!}

                                {!! AppHelper::getValidationErrorMsg($errors, 'mother_occupation') !!}

                            </div>

                        </div>

                        <div class="space-4"></div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label no-padding-right" for="permanent_address">Permanent Address</label>

                            <div class="col-sm-3">

                                {!! Form::text('permanent_address', null, [
                                   "'kl_virtual_keyboard_secure_input"  => "on",
                                   'id'                                 => 'permanent_address',
                                   "placeholder"                        => "Permanent Address",
                                   "class"                              => "col-xs-10 col-sm-12",
                                ]) !!}

                                {!! AppHelper::getValidationErrorMsg($errors, 'permanent_address') !!}

                            </div>

                            <label class="col-sm-2 control-label no-padding-right" for="temporary_address">Temporary Address</label>

                            <div class="col-sm-3">

                                {!! Form::text('temporary_address', null, [
                                   "'kl_virtual_keyboard_secure_input"  => "on",
                                   'id'                                 => 'temporary_address',
                                   "placeholder"                        => "Temporary Address",
                                   "class"                              => "col-xs-10 col-sm-12",
                                ]) !!}

                                {!! AppHelper::getValidationErrorMsg($errors, 'temporary_address') !!}

                            </div>

                        </div>

                        <div class="space-4"></div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label no-padding-right" for="language_score">Toefl Score</label>

                            <div class="col-sm-2">

                                {!! Form::text('toefl_score', null, [
                                   "'kl_virtual_keyboard_secure_input"  => "on",
                                   'id'                                 => 'toefl_score',
                                   "placeholder"                        => "TOEFL Score",
                                   "class"                              => "col-xs-10 col-sm-12",
                                ]) !!}

                                {!! AppHelper::getValidationErrorMsg($errors, 'toefl_score') !!}

                            </div>

                            <label class="col-sm-1 control-label no-padding-right" for="language_score">Ielts Score</label>

                            <div class="col-sm-2">

                                {!! Form::text('ielts_score', null, [
                                   "'kl_virtual_keyboard_secure_input"  => "on",
                                   'id'                                 => 'ielts_score',
                                   "placeholder"                        => "IELTS Score",
                                   "class"                              => "col-xs-10 col-sm-12",
                                ]) !!}

                                {!! AppHelper::getValidationErrorMsg($errors, 'ielts_score') !!}

                            </div>

                            <label class="col-sm-1 control-label no-padding-right" for="language_score">Gre Score</label>

                            <div class="col-sm-2">

                                {!! Form::text('gre_score', null, [
                                   "'kl_virtual_keyboard_secure_input"  => "on",
                                   'id'                                 => 'gre_score',
                                   "placeholder"                        => "GRE Score",
                                   "class"                              => "col-xs-10 col-sm-12",
                                ]) !!}

                                {!! AppHelper::getValidationErrorMsg($errors, 'gre_score') !!}

                            </div>

                        </div>

                        <div class="space-4"></div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label no-padding-right" for="language_score">Gmat Score</label>

                            <div class="col-sm-3">

                                {!! Form::text('gmat_score', null, [
                                   "'kl_virtual_keyboard_secure_input"  => "on",
                                   'id'                                 => 'gmat_score',
                                   "placeholder"                        => "GMAT Score",
                                   "class"                              => "col-xs-10 col-sm-12",
                                ]) !!}

                                {!! AppHelper::getValidationErrorMsg($errors, 'gmat_score') !!}

                            </div>

                            <label class="col-sm-2 control-label no-padding-right" for="language_score">Sat Score</label>

                            <div class="col-sm-3">

                                {!! Form::text('sat_score', null, [
                                   "'kl_virtual_keyboard_secure_input"  => "on",
                                   'id'                                 => 'sat_score',
                                   "placeholder"                        => "SAT Score",
                                   "class"                              => "col-xs-10 col-sm-12",
                                ]) !!}

                                {!! AppHelper::getValidationErrorMsg($errors, 'sat_score') !!}

                            </div>

                        </div>

                        <div class="space-4"></div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label no-padding-right" for="signature_date">Signature Date</label>

                            <div class="col-sm-3">

                                <div class="input-group">

                                    {{ Form::date('signature_date', \Carbon\Carbon::now(), array('id' => 'id-date-picker-1', 'class' => 'form-control date-picker' )) }}

                                    <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>

                                </div>

                            </div>

                            <label class="col-sm-2 control-label no-padding-right" for="emergency_contact_number">Emergency Contact</label>

                            <div class="col-sm-3">

                                {!! Form::text('emergency_contact_number', null, [
                                   "'kl_virtual_keyboard_secure_input"  => "on",
                                   'id'                                 => 'emergency_contact_number',
                                   "placeholder"                        => "Emergency Contact Number",
                                   "class"                              => "col-xs-10 col-sm-12",
                                ]) !!}

                                {!! AppHelper::getValidationErrorMsg($errors, 'emergency_contact_number') !!}

                            </div>

                        </div>

                        <div class="space-4"></div>



                    <div class="clearfix form-actions">

                        <div class="col-md-offset-4 col-md-8">

                            <button class="btn btn-primary" type="submit">
                                <i class="icon-ok bigger-110"></i>
                                Update
                            </button>

                            &nbsp; &nbsp; &nbsp;

                            <button class="btn btn-primary" type="reset">
                                <i class="icon-undo bigger-110"></i>
                                Reset
                            </button>

                        </div>

                    </div>

                    {!! Form::close() !!}

                </div>

            </div>

        </div>

    </div>

@endsection
@section('page_specific_scripts')

    <script src="{{ asset('assets/admin/js/bootstrap-datepicker.min.js') }}"></script>

    <script type="text/javascript">
        jQuery(function ($) {
            $('.date-picker').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            }).next().on(ace.click_event, function () {
                $(this).prev().focus();
            });

        });

        $('#college_id').on('change', function (e) {

            console.log(e);

            var college_id = e.target.value;

            console.log(college_id);

            // Ajax
            $.get('/ajax-course?college_id=' + college_id, function (data) {

                // Success data
                $('#course_applied_for').empty();

                $.each(data, function (index, courseObj) {

                    $('#course_applied_for').append('<option value="' + courseObj.course_name + '">' + courseObj.course_name + '</option>');

                })

            });
        });

    </script>

@endsection