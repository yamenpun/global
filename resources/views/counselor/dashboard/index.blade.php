@extends('counselor.layouts.master')

@section('page_specific_style')

    <style>
        .acb {
            display: block;
        }
    </style>

@endsection

@section('title')
    Dashboard
@endsection

@section('content')


    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('counselor.dashboard') }}">Home</a>
                </li>

                <li class="active">Dashboard</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    <small>
                        <i class="icon-double-angle-right"></i>
                        Welcome To Dashboard Manager
                    </small>

                </h1>

            </div>


            <div class="col-xs-12">

                <div class="alert alert-block alert-success">

                    <button type="button" class="close" data-dismiss="alert">
                        <i class="icon-remove"></i>
                    </button>

                    <i class="icon-ok green"></i>

                    Hi, <b>{{ ucfirst($logged_in_user->fullname) }}</b> welcome to the dashboard of Global Education Counselling Pvt. Ltd.

                </div>

            </div>

        </div>

    </div>

@endsection

