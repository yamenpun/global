@extends('counselor.layouts.master')

@section('title')
    View Student Enquiry
@endsection

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('counselor.dashboard') }}">Home</a>
                </li>

                <li>
                    <a href="{{ route($base_route.'.list') }}">Student Enquiry List</a>
                </li>

                <li class="active">View Student Enquiry</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    <small>
                        <i class="icon-double-angle-right"></i>
                        Student Enquiry Detail Info
                    </small>

                    <div class="btn-group pull-right">

                        <a href="{{ URL::previous()}}" class="btn btn-pink btn-sm">
                            <i class="icon-backward bigger-110"></i>
                            Go Back
                        </a>

                    </div>

                </h1>

            </div>

            <div class="row">

                <div class="col-xs-12">

                    <div class="row">

                        <div class="col-xs-8">

                            <div class="table-responsive">

                                @if (session()->has('message'))
                                    {!! session()->get('message') !!}
                                @endif

                                <table class="table table-striped table-bordered table-hover "
                                       id="" aria-describedby="sample-table-2_info">

                                    <tbody role="alert" aria-live="polite" aria-relevant="all">

                                    <tr class="odd">

                                        <td>Student Full Name</td>
                                        <td>{{ $data['row']['0']->student_first_name }} {{ $data['row']['0']->student_middle_name }} {{ $data['row']['0']->student_last_name }} </td>

                                    </tr>

                                    <tr class="even">

                                        <td>Student Email</td>
                                        <td>{{ $data['row']['0']->email }} </td>

                                    </tr>

                                    <tr class="odd">

                                        <td>Phone Number</td>
                                        <td>HOME: @if($data['row']['0']->home_phone){{ $data['row']['0']->home_phone }}@else N/A @endif | MOBILE: {{ $data['row']['0']->mobile_phone }}</td>

                                    </tr>

                                    <tr class="even">

                                        <td>SLC Details</td>
                                        <td>YEAR: {{ $data['row']['0']->year_of_completion_slc }} A.D. | Percentage: {{ $data['row']['0']->slc_percentage }} % </td>

                                    </tr>

                                    <tr class="odd">

                                        <td>+2 Details</td>
                                        <td>YEAR: {{ $data['row']['0']->year_of_completion_plus_two }} A.D. | Percentage: {{ $data['row']['0']->plus_two_percentage }} % </td>

                                    </tr>

                                    <tr class="even">

                                        <td>@if($data['row']['0']->language){{ strtoupper($data['row']['0']->language) }} @else NO Language Test @endif</td>

                                        <td>@if($data['row']['0']->language){{ $data['row']['0']->language_score }} @else NO Language Score @endif </td>

                                    </tr>

                                    <tr class="odd">

                                        <td>Guardian Full Name</td>

                                        <td>{{ $data['row']['0']->guardian_first_name }} {{ $data['row']['0']->guardian_middle_name }} {{ $data['row']['0']->guardian_last_name }} </td>

                                    </tr>

                                    <tr class="odd">

                                        <td>Guardian Email</td>
                                        <td>
                                            @if($data['row']['0']->email)
                                                {{ $data['row']['0']->email }}
                                            @else
                                                N/A
                                            @endif
                                        </td>

                                    </tr>

                                    <tr class="even">

                                        <td>Guardian Phone Number</td>
                                        <td>HOME: @if($data['row']['0']->guardian_phone){{ $data['row']['0']->guardian_phone }} @else N/A @endif | MOBILE: {{ $data['row']['0']->guardian_mobile }}</td>

                                    </tr>

                                    <tr class="even">

                                        <td>Relationship with Guardian</td>

                                        <td>
                                            @if($data['row']['0']->guardian_relationship)
                                                {{ $data['row']['0']->guardian_relationship }}
                                            @else
                                                N/A
                                            @endif
                                        </td>

                                    </tr>

                                    <tr class="even">

                                        <td>Interested Course</td>

                                        <td>
                                            @if($data['row']['0']->interested_course)
                                                {{ $data['row']['0']->interested_course }}
                                            @else
                                                N/A
                                            @endif
                                        </td>

                                    </tr>

                                    <tr class="odd">

                                        <td>Preferred Place</td>

                                        <td>
                                            @if($data['row']['0']->preferred_place)
                                                {{ $data['row']['0']->preferred_place }}
                                            @else
                                                N/A
                                            @endif
                                        </td>

                                    </tr>

                                    <tr class="even">
                                        <td>Is Registered?</td>
                                        <td>
                                            @if($data['row']['0']->is_registered == '0')
                                                NO
                                            @else
                                                Yes
                                            @endif
                                        </td>
                                    </tr>

                                    <tr class="odd">
                                        <td>Is Counselled?</td>
                                        <td>
                                            @if($data['row']['0']->is_counselled == '0')
                                                NO
                                            @else
                                                Yes
                                            @endif
                                        </td>
                                    </tr>

                                    <tr class="odd">
                                        <td>Counselled Date</td>
                                        <td>
                                            @if($data['row']['0']->is_counselled == '0')
                                                N/A
                                            @else
                                                {{ $data['row']['0']->counselling_date }}
                                            @endif
                                        </td>
                                    </tr>

                                    <tr class="even">
                                        <td>Counselor Remarks</td>
                                        <td>
                                            @if($data['row']['0']->counsellor_remarks)
                                                {{ $data['row']['0']->counsellor_remarks }}
                                            @else
                                                N/A
                                            @endif
                                        </td>
                                    </tr>

                                    <tr class="odd">

                                        <td>Branch Name</td>

                                        <td>{{ $data['row']['0']->branch_name }} </td>

                                    </tr>

                                    <tr class="even">

                                        <td>Created By</td>

                                        <td>{{ $data['row']['0']->fullname }} </td>

                                    </tr>

                                    </tbody>

                                </table>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

@endsection
