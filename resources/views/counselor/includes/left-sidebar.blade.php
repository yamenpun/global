<div class="sidebar" id="sidebar">

    <script type="text/javascript">
        try {
            ace.settings.check('sidebar', 'fixed')
        } catch (e) {
        }
    </script>

    <ul class="nav nav-list">

        <li {!! Request::is('counselor/dashboard')?'class="active"':'' !!}>

            <a href="{{ route('counselor.dashboard') }}">
                <i class="icon-dashboard"></i>
                <span class="menu-text"> Dashboard </span>
            </a>

        </li>

        <li {!! Request::is('counselor/enquiry/list*') || Request::is('counselor/enquiry/view*')?'class="active"':'' !!}>

            <a href="{{ route('counselor.enquiry.list') }}">
                <i class="icon-home"></i>
                <span class="menu-text"> Student Enquiry </span>
            </a>

        </li>

        <li {!! Request::is('counselor/register/list*') || Request::is('counselor/register/view*') || Request::is('counselor/register/add*') ||  Request::is('counselor/register/edit*')?'class="active"':'' !!}>

            <a href="{{ route('counselor.register.list') }}">
                <i class="icon-edit"></i>
                <span class="menu-text"> Registration List </span>
            </a>

        </li>

        <li {!! Request::is('counselor/agreement/list*') || Request::is('counselor/agreement/view*') ?'class="active"':'' !!}>

            <a href="{{ route('counselor.agreement.list') }}">
                <i class="icon-key"></i>
                <span class="menu-text"> Agreement List </span>
            </a>

        </li>

        <li {!! Request::is('counselor/college/list*')?'class="active"':'' !!}>

            <a href="{{ route('counselor.college.list') }}">
                <i class="icon-home"></i>
                <span class="menu-text"> College List </span>
            </a>

        </li>

        <li {!! Request::is('counselor/course/list*')?'class="active"':'' !!}>

            <a href="{{ route('counselor.course.list') }}">
                <i class="icon-certificate"></i>
                <span class="menu-text"> Course List </span>
            </a>

        </li>

        <li>

            <a href="{{ url('logout') }}">
                <i class="icon-signout"></i>
                <span class="menu-text"> LogOut </span>
            </a>

        </li>

    </ul>

    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>

    <script type="text/javascript">

        try {
            ace.settings.check('sidebar', 'collapsed')
        } catch (e) {
        }

    </script>

</div>

