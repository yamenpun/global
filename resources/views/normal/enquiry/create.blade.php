@extends('normal.layouts.master')

@section('title')
    Add Student Enquiry
@endsection

@section('page_specific_style')

    <link href="{{ asset('assets/admin/css/datepicker.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/admin/css/chosen.css') }}" rel="stylesheet"/>

@endsection

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('normal.dashboard') }}">Home</a>
                </li>

                <li>
                    <a href="{{ route($base_route.'.list') }}">Enquiry List</a>
                </li>

                <li class="active">Add Form</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    <small>
                        <i class="icon-double-angle-right"></i>
                        Add Student Enquiry Info
                    </small>

                    <div class="btn-group pull-right">
                        <a href="{{ URL::previous()}}" class="btn btn-pink btn-sm pull-right">
                            <i class="icon-backward bigger-110"></i>
                            Go Back
                        </a>
                    </div>

                </h1>

            </div>

            <div class="row-fluid">

                <div class="col-sm-12">

                    @if (session()->has('message'))
                        {!! session()->get('message') !!}
                    @endif

                    {!! Form::open([
                    'route'     => $base_route.'.store',
                    'method'    => 'post',
                    'class'     => 'form-horizontal',
                    'role'      => "form",
                    'enctype'   => "multipart/form-data"
                    ]) !!}

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="form-group">

                        <label class="col-sm-2 control-label no-padding-right" for="enquiry_date">Enquiry Date</label>

                        <div class="col-sm-8">

                            <div class="input-group">

                                {{ Form::date('enquiry_date', \Carbon\Carbon::now(), array('id' => 'id-date-picker-1', 'class' => 'form-control date-picker' )) }}

                                <span class="input-group-addon"><i class="icon-calendar bigger-110"></i></span>

                            </div>

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-2 control-label no-padding-right" for="student_first_name">Student Full Name</label>

                        <div class="col-sm-3 no-padding-right">

                            {!! Form::text('student_first_name', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'student_first_name',
                               "placeholder"                        => "First Name",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'student_first_name') !!}

                        </div>

                        <div class="col-sm-2">

                            {!! Form::text('student_middle_name', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'student_middle_name',
                               "placeholder"                        => "Middle Name",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'student_middle_name') !!}

                        </div>

                        <div class="col-sm-3">

                            {!! Form::text('student_last_name', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'student_last_name',
                               "placeholder"                        => "Last Name",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'student_last_name') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-2 control-label no-padding-right" for="email">Email Address</label>

                        <div class="col-sm-3 no-padding-right">

                            {!! Form::email('email', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'email',
                               "placeholder"                        => "Email Address",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'email') !!}

                        </div>

                        <label class="col-sm-2 control-label" for="language">Language Test </label>

                        <div class="col-sm-1">

                            <select name="language" id="language" class="col-xs-10 col-sm-12">
                                @foreach(config('global.test') as $key => $language)
                                    <option value="{{ $key }}" {!! $language?'selected=selected':'' !!}>{{ $language }}</option>
                                @endforeach
                            </select>

                        </div>

                        <div class="col-sm-2">
                            {!! Form::text('language_score', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'language_score',
                               "placeholder"                        => "Score",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'language_score') !!}
                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-2 control-label no-padding-right" for="home_phone">Phone Number </label>

                        <div class="col-sm-3 no-padding-right">

                            {!! Form::text('home_phone', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'home_phone',
                               "placeholder"                        => "Phone Number",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'home_phone') !!}

                        </div>

                        <label class="col-sm-2 control-label no-padding-right" for="mobile_phone">Mobile Number </label>

                        <div class="col-sm-3">

                            {!! Form::text('mobile_phone', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'mobile_phone',
                               "placeholder"                        => "Mobile Number",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'mobile_phone') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-2 control-label no-padding-right" for="slc_percentage">SLC Percentage </label>

                        <div class="col-sm-3 no-padding-right">

                            {!! Form::text('slc_percentage', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'slc_percentage',
                               "placeholder"                        => "SLC Percentage",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'slc_percentage') !!}

                        </div>

                        <label class="col-sm-2 control-label no-padding-right" for="year_of_completion_slc">SLC Completed Year</label>

                        <div class="col-sm-3">

                            {!! Form::text('year_of_completion_slc', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'year_of_completion_slc',
                               "placeholder"                        => "SLC Completed Year",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'year_of_completion_slc') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-2 control-label no-padding-right" for="plus_two_percentage">+2 Percentage </label>

                        <div class="col-sm-3 no-padding-right">

                            {!! Form::text('plus_two_percentage', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'plus_two_percentage',
                               "placeholder"                        => "+2 Percentage",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'plus_two_percentage') !!}

                        </div>

                        <label class="col-sm-2 control-label no-padding-right" for="year_of_completion_plus_two">+2 Completed Year</label>

                        <div class="col-sm-3">

                            {!! Form::text('year_of_completion_plus_two', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'year_of_completion_plus_two',
                               "placeholder"                        => "+2 Completed Year",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'year_of_completion_plus_two') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-2 control-label no-padding-right" for="guardian_first_name">Guardian's Name</label>

                        <div class="col-sm-3 no-padding-right">

                            {!! Form::text('guardian_first_name', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'guardian_first_name',
                               "placeholder"                        => "First Name",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'guardian_first_name') !!}

                        </div>

                        <div class="col-sm-2">

                            {!! Form::text('guardian_middle_name', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'guardian_middle_name',
                               "placeholder"                        => "Middle Name",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'guardian_middle_name') !!}

                        </div>

                        <div class="col-sm-3">

                            {!! Form::text('guardian_last_name', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'guardian_last_name',
                               "placeholder"                        => "Last Name",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'guardian_last_name') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-2 control-label no-padding-right" for="guardian_phone">Guardian's Phone </label>

                        <div class="col-sm-3 no-padding-right">

                            {!! Form::text('guardian_phone', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'guardian_phone',
                               "placeholder"                        => "Guardian Phone Number",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'guardian_phone') !!}

                        </div>

                        <label class="col-sm-2 control-label no-padding-right" for="guardian_mobile">Guardian Mobile </label>

                        <div class="col-sm-3">

                            {!! Form::text('guardian_mobile', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'guardian_mobile',
                               "placeholder"                        => "Guardian Mobile Number",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'guardian_mobile') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-2 control-label no-padding-right" for="guardian_email">Guardian Email </label>

                        <div class="col-sm-3 no-padding-right">

                            {!! Form::email('guardian_email', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'guardian_email',
                               "placeholder"                        => "Guardian Email",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'guardian_email') !!}

                        </div>

                        <label class="col-sm-2 control-label no-padding-right" for="guardian_relationship">Guardian Relationship </label>

                        <div class="col-sm-3">

                            {!! Form::text('guardian_relationship', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'guardian_relationship',
                               "placeholder"                        => "Guardian Relationship",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'guardian_relationship') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-2 control-label no-padding-right" for="interested_course">Interested Course </label>

                        <div class="col-sm-3 no-padding-right">

                            {!! Form::text('interested_course', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'interested_course',
                               "placeholder"                        => "Interested Course",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'interested_course') !!}

                        </div>

                        <label class="col-sm-2 control-label no-padding-right" for="preferred_place">Preferred Place</label>

                        <div class="col-sm-3">

                            {!! Form::text('preferred_place', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'preferred_place',
                               "placeholder"                        => "Preferred Place",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'preferred_place') !!}

                        </div>

                    </div>

                    <div class="space-4"></div>

                    <div class="form-group">

                        <label class="col-sm-2 control-label no-padding-right" for="how_know_us">How Know US</label>

                        <div class="col-sm-8 no-padding-right">

                            {!! Form::text('how_know_us', null, [
                               "'kl_virtual_keyboard_secure_input"  => "on",
                               'id'                                 => 'how_know_us',
                               "placeholder"                        => "How Know Us",
                               "class"                              => "col-xs-10 col-sm-12",
                            ]) !!}

                            {!! AppHelper::getValidationErrorMsg($errors, 'how_know_us') !!}

                        </div>

                    </div>

                    <div class="clearfix form-actions">

                        <div class="col-md-offset-4 col-md-8">

                            <button class="btn btn-primary" type="submit">
                                <i class="icon-ok bigger-110"></i>
                                Save
                            </button>

                            &nbsp; &nbsp; &nbsp;

                            <button class="btn btn-primary" type="reset">
                                <i class="icon-undo bigger-110"></i>
                                Reset
                            </button>

                        </div>

                    </div>

                    {!! Form::close() !!}

                </div>

            </div>

        </div>

    </div>

@endsection
@section('page_specific_scripts')

    <script src="{{ asset('assets/admin/js/bootstrap-datepicker.min.js') }}"></script>

    <script type="text/javascript">
        jQuery(function($) {
            $('.date-picker').datepicker({format: 'yyyy-mm-dd', autoclose:true}).next().on(ace.click_event, function(){
                $(this).prev().focus();
            });
        });

        $(".chosen-select").chosen();
        $('#chosen-multiple-style').on('click', function(e){
            var target = $(e.target).find('input[type=radio]');
            var which = parseInt(target.val());
            if(which == 2) $('#form-field-select-4').addClass('tag-input-style');
            else $('#form-field-select-4').removeClass('tag-input-style');
        });

    </script>

@endsection