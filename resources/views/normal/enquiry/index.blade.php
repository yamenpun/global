@extends('normal.layouts.master')

@section('title')
    Student Enquiry List
@endsection

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('normal.dashboard') }}">Home</a>
                </li>

                <li class="active">Student Enquiry List</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    <small>
                        <i class="icon-double-angle-right"></i>
                        Student Enquiry List
                    </small>

                    <div class="btn-group">

                        <a href="{{ route($base_route.'.add') }}" class="btn btn-primary btn-sm">
                            <i class="icon-plus-sign bigger-110"></i>
                            Add New
                        </a>

                    </div>

                    <div class="btn-group pull-right">

                        <a target="_blank" href={{ route($base_route.'.print', ['country' => AppHelper::getCountryFilter(), 'year' => AppHelper::getYearFilter(), 'month' => AppHelper::getMonthFilter() ] ) }}" class="btn btn-pink btn-sm">
                        <i class="icon-print bigger-110"></i>
                        Print
                        </a>

                    </div>

                    <div class="btn-group pull-right">

                        <button data-toggle="dropdown" class="btn btn-primary btn-sm dropdown-toggle">
                            Export As
                            <i class="icon-angle-down icon-on-right"></i>
                        </button>

                        <ul class="dropdown-menu">

                            <li><a href="{{ route($base_route.'.pdf', ['country' => AppHelper::getCountryFilter(), 'year' => AppHelper::getYearFilter(), 'month' => AppHelper::getMonthFilter() ] ) }}">PDF</a></li>

                            <li class="divider"></li>

                            <li><a href="{{ route($base_route.'.excel', ['country' => AppHelper::getCountryFilter(), 'year' => AppHelper::getYearFilter(), 'month' => AppHelper::getMonthFilter() ] ) }}">Excel</a></li>

                        </ul>

                    </div>

                </h1>

            </div>

            <div class="page-header">

                <h6>

                    {!! Form::open(['method'  => 'GET']) !!}

                    <label>Country</label>

                    <select name="country">

                        <option value="all" selected="selected">-- Select Country --</option>

                        @foreach($data['college'] as $college)

                            <option value="{{ $college->college_country }}" {!! Request()->get('country')== $college->college_country?'selected=selected':'' !!} >{{ $college->college_country }}</option>

                        @endforeach

                    </select>

                    <label> Year </label>

                    <select name="year">

                        <option value="all" selected="selected">-- Select Year --</option>
                        <option value="2016"> 2016 </option>
                        <option value="2017"> 2017 </option>
                        <option value="2018"> 2018 </option>
                        <option value="2019"> 2019 </option>
                        <option value="2020"> 2020 </option>

                    </select>

                    <label> Month </label>

                    <select name="month">

                        <option value="all" selected="selected">-- Select Month --</option>
                        <option value="01"> January </option>
                        <option value="02"> February </option>
                        <option value="03"> March </option>
                        <option value="04"> April </option>
                        <option value="05"> May </option>
                        <option value="06"> June </option>
                        <option value="07"> July </option>
                        <option value="08"> August </option>
                        <option value="09"> September </option>
                        <option value="10"> October </option>
                        <option value="11"> November </option>
                        <option value="12"> December </option>

                    </select>

                    <button class="btn btn-pink btn-sm" type="submit">

                        <i class="icon-filter bigger-110"></i>

                        Filter

                    </button>

                    {!! Form::close() !!}

                </h6>

            </div>

            <div class="row">

                <div class="col-xs-12">

                    <div class="row">

                        <div class="col-xs-12">

                            <div class="table-responsive">

                                @if (session()->has('message'))
                                    {!! session()->get('message') !!}
                                @endif

                                <table id="sample-table-2" class="table table-striped table-bordered table-hover">

                                    <thead>
                                    <tr>
                                        <th style="width: 10px;" class="center">
                                            <label>
                                                <input type="checkbox" class="ace"/>
                                                <span class="lbl"></span>
                                            </label>
                                        </th>

                                        <th style="width: 70px;">Enquiry Date</th>

                                        <th style="width: 80px;">Full Name</th>

                                        <th style="width: 160px;">Education History</th>

                                        <th style="width: 100px;">Preferred Place</th>

                                        <th style="width: 20px;">Counselled?</th>

                                        <th style="width: 20px;">Registered?</th>

                                        <th style="width: 70px;">Entry By</th>

                                        <th style="width: 70px;"></th>

                                    </tr>

                                    </thead>

                                    <tbody>
                                    @foreach($data['rows'] as $row)

                                        <tr>

                                            <td class="center">
                                                <label>
                                                    <input type="checkbox" class="ace"/>
                                                    <span class="lbl"></span>
                                                </label>
                                            </td>

                                            <td>{{ $row->enquiry_date }}</td>

                                            <td>{{ $row->student_first_name }} {{ $row->student_middle_name }} {{ $row->student_last_name }}</td>

                                            <td>
                                                <p>SLC :- {{ $row->year_of_completion_slc }} A.D.
                                                    Percentage:- {{ $row->slc_percentage }} %</p>
                                                <p>+2 :- {{ $row->year_of_completion_plus_two }} A.D.
                                                    Percentage:- {{ $row->plus_two_percentage }} %</p>
                                            </td>

                                            <td>{{ $row->preferred_place }}</td>

                                            <td>
                                                @if($row->is_counselled == '0')
                                                    NO
                                                @else
                                                    YES
                                                @endif
                                            </td>

                                            <td>
                                                @if($row->is_registered == '0')
                                                    NO
                                                @else
                                                    YES
                                                @endif
                                            </td>

                                            <td>{{ $row->fullname }}</td>

                                            <td>
                                                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">

                                                    <a class="blue"
                                                       href="{{ route($base_route.'.view', ['id' => $row->id]) }}">
                                                        <i class="icon-zoom-in bigger-130" title="View Student Enquiry"></i>
                                                    </a>

                                                    <a class="green"
                                                       href="{{ route($base_route.'.edit', ['id' => $row->id]) }}">
                                                        <i class="icon-pencil bigger-130" title="Update Student Enquiry"></i>
                                                    </a>

                                                    <a class="red disableConfirm"
                                                       href="{{ route($base_route.'.delete', ['id' => $row->id]) }}">
                                                        <i class="icon-trash bigger-130" title="Delete Student Enquiry"></i>
                                                    </a>

                                                </div>

                                                <div class="visible-xs visible-sm hidden-md hidden-lg">

                                                    <div class="inline position-relative">

                                                        <button class="btn btn-minier btn-yellow dropdown-toggle"
                                                                data-toggle="dropdown">
                                                            <i class="icon-caret-down icon-only bigger-120"></i>
                                                        </button>

                                                        <ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">

                                                            <li>
                                                                <a href="#" class="tooltip-info" data-rel="tooltip"
                                                                   title="View">
                                                                    <span class="blue">
                                                                        <i class="icon-zoom-in bigger-120"></i>
                                                                    </span>
                                                                </a>
                                                            </li>

                                                            <li>
                                                                <a href="#" class="tooltip-success" data-rel="tooltip"
                                                                   title="Edit">
                                                                        <span class="green">
                                                                            <i class="icon-edit bigger-120"></i>
                                                                        </span>
                                                                </a>
                                                            </li>

                                                            <li>
                                                                <a href="#" class="tooltip-error" data-rel="tooltip"
                                                                   title="Delete">
                                                                        <span class="red">
                                                                            <i class="icon-trash bigger-120"></i>
                                                                        </span>
                                                                </a>
                                                            </li>

                                                        </ul>

                                                    </div>

                                                </div>

                                            </td>

                                        </tr>

                                    @endforeach

                                    </tbody>

                                </table>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- page specific plugin scripts -->

    <script src="{{ asset('assets/admin/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/jquery.dataTables.bootstrap.js') }}"></script>

    <script type="text/javascript">

        jQuery(function ($) {
            var oTable1 = $('#sample-table-2').dataTable({
                "aoColumns": [
                    {"bSortable": false},
                    null, null, null, null, null, null, null,
                    {"bSortable": false}
                ]
            });


            $('table th input:checkbox').on('click', function () {
                var that = this;
                $(this).closest('table').find('tr > td:first-child input:checkbox')
                        .each(function () {
                            this.checked = that.checked;
                            $(this).closest('tr').toggleClass('selected');
                        });

            });


            $('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
            function tooltip_placement(context, source) {
                var $source = $(source);
                var $parent = $source.closest('table')
                var off1 = $parent.offset();
                var w1 = $parent.width();

                var off2 = $source.offset();
                var w2 = $source.width();

                if (parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2)) return 'right';
                return 'left';
            }
        })
    </script>

@endsection
