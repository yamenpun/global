<div class="sidebar" id="sidebar">

    <script type="text/javascript">
        try {
            ace.settings.check('sidebar', 'fixed')
        } catch (e) {
        }
    </script>

    <ul class="nav nav-list">

        <li {!! Request::is('normal/dashboard')?'class="active"':'' !!}>

            <a href="{{ route('normal.dashboard') }}">
                <i class="icon-dashboard"></i>
                <span class="menu-text"> Dashboard </span>
            </a>

        </li>

        <li {!! Request::is('normal/enquiry/list*') || Request::is('normal/enquiry/view*') || Request::is('normal/enquiry/add*') || Request::is('normal/enquiry/edit*')?'class="active"':'' !!}>

            <a href="{{ route('normal.enquiry.list') }}">
                <i class="icon-plus-sign"></i>
                <span class="menu-text"> Student Enquiry </span>
            </a>

        </li>

        <li {!! Request::is('normal/register/list*') || Request::is('normal/register/view*')?'class="active"':'' !!}>

            <a href="{{ route('normal.register.list') }}">
                <i class="icon-edit"></i>
                <span class="menu-text"> Registration </span>
            </a>

        </li>

        <li {!! Request::is('normal/agreement/list*') || Request::is('normal/agreement/view*')?'class="active"':'' !!}>

            <a href="{{ route('normal.agreement.list') }}">
                <i class="icon-key"></i>
                <span class="menu-text"> Agreement </span>
            </a>

        </li>

        <li {!! Request::is('normal/college/list*')?'class="active"':'' !!}>

            <a href="{{ route('normal.college.list') }}">
                <i class="icon-home"></i>
                <span class="menu-text"> College List </span>
            </a>

        </li>

        <li {!! Request::is('normal/course/list*')?'class="active"':'' !!}>

            <a href="{{ route('normal.course.list') }}">
                <i class="icon-certificate"></i>
                <span class="menu-text"> Course List </span>
            </a>

        </li>

        <li>

            <a href="{{ url('logout') }}">
                <i class="icon-signout"></i>
                <span class="menu-text"> LogOut </span>
            </a>

        </li>

    </ul>

    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>

    <script type="text/javascript">

        try {
            ace.settings.check('sidebar', 'collapsed')
        } catch (e) {
        }

    </script>

</div>

