@extends('superadmin.layouts.master')

@section('title')
    View Agreement
@endsection

@section('content')

    <div class="main-content">

        <div class="breadcrumbs no-print" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">Home</a>
                </li>

                <li>
                    <a href="{{ route($base_route.'.list') }}">Student Agreement List</a>
                </li>

                <li class="active">View Student Agreement</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    <small>
                        <i class="icon-double-angle-right"></i>
                        View Student Agreement Detail Info
                    </small>

                    <div class="btn-group pull-right">

                        <a href="{{ URL::previous()}}" class="btn btn-pink btn-sm">
                            <i class="icon-backward bigger-110"></i>
                            Go Back
                        </a>

                    </div>

                    <div class="btn-group pull-right">

                        <a href="{{ route($base_route.'.agreementView' ) }}" class="btn btn-primary btn-sm">
                            <i class="icon-print bigger-110"></i>
                            Print
                        </a>

                    </div>

                </h1>

            </div>

            <div class="row">

                <div class="col-xs-11" style="padding-left: 80px; text-align: justify">

                    <div>

                        <p style="font-size: 14px;">
                            लिखितम् का.जि.का.म.न.पा वडा नं. ३१ बागबजार स्थित ग्लोबल एजुकेशन काउन्सिलिङ्ग
                            सेन्टर प्रा.लि. को तर्फबाट अख्तियार प्राप्त संस्था ( जसलाई यसपछि प्रथम पक्ष भनि
                            सम्बोधन गरिएको ) र <b>{{ $data['row']['0']->grand_father_name }}</b> को नाति <b>{{ $data['row']['0']->father_name }}</b> को छोरा
                            <b>{{ $data['row']['0']->district }}</b> जिल्ला <b>{{ $data['row']['0']->vdc_municipality }}</b> न.पा./गा.वि.स. वडा नं
                            <b>{{ $data['row']['0']->ward_no }}</b> , <b>{{ $data['row']['0']->tole }}</b> मा बस्ने बर्ष
                            <?php $dob = strtotime($data['row']['0']->date_of_birth ); $current_time = time(); ?>
                            <b>{{ $age_years = date('Y',$current_time) - date('Y',$dob) }}</b> को <b>{{ $data['row']['0']->student_first_name }} {{ $data['row']['0']->student_middle_name }} {{ $data['row']['0']->student_last_name }}</b> ( जसलाई यसपछि दोस्रो पक्ष भनि सम्बोधन गरिएको ) आगे हामी दुवै
                            पक्षहरुका बीच तरसिलका शर्तहरुमा करार
                            गर्न मञ्जुर भएकाले करार ऐन, २०५६ को दफा ५ बमोजिम यो करारनामाको कागज तयार गरि
                            हाम्रा स्वतन्त्र मनोमान राजिखुशीसँग तपसिलका साक्षीका रोहबरमा यो करारनामामा
                            हस्ताक्षर गरि एक/एक
                            प्रति लियौं/दियौँ । मुकाम प्रथम पक्षको अफिस ।

                        </p>

                        <p style="font-size: 14px; text-align: justify;">तपसिल</p>
                        <p style="font-size: 14px; text-align: justify;">शर्तहरु :</p>


                    </div>

                    <div>

                        <ul style="font-size: 14px; text-align: justify; list-style: decimal;">

                            <li>प्रथम पक्षले <b>{{ $data['row']['0']->college_country }}</b> स्थित <b>{{ $data['row']['0']->college_name }}</b> मा <b>{{ $data['row']['0']->course_name }}</b> विषयको
                                कोर्ष अध्ययन गर्नका लागि म दोस्रो पक्षलाई दिइएको परामर्श सेवा शुल्क तथा
                                शिक्षण संस्थामा तिर्नुपर्ने अन्य शुल्कमा म दोस्रो पक्षको चित्त बुझ्यो ।
                            </li>
                            <li>प्रथम पक्षले परामर्श दिएको <b>{{ $data['row']['0']->college_country }}</b> स्थित <b>{{ $data['row']['0']->college_name }}</b> मा <b>{{ $data['row']['0']->course_name }}</b> कोर्षको लागि प्रकरण नं. १
                                बमोजिम लाग्ने शुल्क तिरी अध्ययन गर्न म दोस्रो पक्ष मञ्जुर गर्दछु ।
                            </li>
                            <li>प्रकरण नं. १ को शिक्षण संस्थामा अध्ययन गर्दा लाग्ने शुल्क, छात्राबास शुल्क,
                                परिक्षा शूल्क लगायतका नियम अनुसार लाग्ने शुल्क तथा दोस्रो पक्षले शिक्षण
                                संस्थामा जाँदा आउँदा लाग्ने हवाई तथा यातायात खर्च, होटल-लज खर्च, प्रथम
                                पक्षले भर्ना प्रकृयाको लागि गरेको पत्राचार खर्च दोस्रो पक्षले नै
                                तिर्नुपर्नेछ ।
                            </li>
                            <li>प्रकरण नं. १ को शिक्षण संस्थामा अध्ययन गर्ने अनुमति पाएपछि वा भर्ना भई
                                सकेपछि दोस्रो पक्षले उक्त शिक्षण संस्थामा अध्ययन नगरेमा वा अन्य शिक्षण
                                संस्थामा गई अध्ययन गरेमा अध्ययन अनुमति वा भर्ना गर्दाको अवधिसम्म प्रथम
                                पक्षले बुझि लिएको परामर्श शुल्क, सेवा शुल्क, धरौटी लगायतका कुनै पनि रुपैयाको
                                दाबी दोस्रो पक्षले प्रथम पक्षसँग गर्न पाउने छैन ।
                            </li>
                            <li>करारमा उल्लेखित शिक्षण संस्थामा भर्ना भई अध्ययनका क्रममा शैक्षिक प्रमाणपत्र
                                पेश गरेको कारण शिक्षण संस्थाले निष्काशित गरेमा, शैक्षिक सत्र समाप्त नहुँदै
                                बिचैमा अध्ययन छाडेमा वा शिक्षण संस्थाको नियम उल्लंघन गरी निष्काशित भएमा
                                दोस्रो पक्षले प्रथम पक्ष उपर कुनै पनि प्रकारको आर्थिक दाबी लिन पाउने छैन ।
                            </li>
                            <li>प्रथम पक्ष आफ्नो सेवा अन्तर्गत परामर्श, अफर लेटर, सेलेक्सन लेटर, आई
                                ट्वान्टी, कोटा रिजर्भेशनको काम गरि कलेज भर्ना गराई दिने मात्र कार्य गर्ने छ
                                । सोका लागि प्रथम पक्षले परामर्श तथा सेवा लगायतको निश्चित रुपैँया सेवा शुल्क
                                वापत लिने छ ।
                            </li>
                            <li>शिक्षण संस्थामा भर्ना भई सकेपछि शिक्षण संस्थाको आन्तरिक समस्या वा सरकार र
                                शिक्षण संस्थाको आन्तरिक समस्याले गर्दा अध्ययनको समयावधि बढेमा वा परिक्षा दिन
                                नपाएमा त्यसको जिम्मेवार प्रथम पक्ष हुने छैन ।
                            </li>
                            <li>सिट रिजर्भेसनका लागि शिक्षण संस्थालाई रुपैयाँ बुझाई सकेपछि दोस्रो पक्ष
                                अध्ययन गर्न नगएमा उक्त रुपैयाँ दोस्रो पक्षले फिर्ता पाउने छैन ।
                            </li>
                            <li>यो करारनामा मिति <b>{{ $data['row']['0']->agreement_date }}</b></td> बाट लागु भएको छ, यो करारनामा एक शैक्षिक सत्रको (भर्ना भएको कोर्षको शैक्षिक
                                अवधि ) सम्म मात्र बैध मानिने छ ।
                            </li>
                        </ul>

                    </div>
                    <br/>

                    <div>

                        <p>साक्षी १ :-  <b>{{ $data['row']['0']->witness1_name }}</b></p>

                        <p>साक्षी २ :-  <b>{{ $data['row']['0']->witness2_name }}</b></p>

                    </div>

                    <div style="margin-top: 10px;">
                        <table border="0px" width="1000px">
                            <tr>
                                <td>प्रथम पक्ष:</td>
                                <td>दोस्रो पक्ष:</td>
                            </tr>
                            <tr>
                                <td>
                                    नाम: <b>{{ $data['row']['0']->first_party }}</b></td>
                                <td>नाम: <b>{{ $data['row']['0']->student_first_name }} {{ $data['row']['0']->student_middle_name }} {{ $data['row']['0']->student_last_name }}</b></td>
                            </tr>
                        </table>
                    </div>

                    <div>
                        <br/>
                        <p>
                            इति सम्बत्&nbsp;&nbsp;&nbsp;<b>{{ $data['row']['0']->year }}</b></td> साल
                            &nbsp;&nbsp;&nbsp;<b>{{ $data['row']['0']->month }}</b></td> महिना
                            &nbsp;&nbsp;&nbsp;<b>{{ $data['row']['0']->day }}</b></td> गते
                            &nbsp;&nbsp;&nbsp; रोज <b>{{ $data['row']['0']->bar }}</b></td> &nbsp;&nbsp; शुभम् !!!
                        </p>

                    </div>

                </div>

            </div>

        </div>

    </div>

@endsection
