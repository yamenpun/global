<!DOCTYPE html>

<html>

    <head>

        <title>Convert To PDF</title>

        <style>

            table {
                border-collapse: collapse;
                width: 100%;
            }

            th, td {
                text-align: left;
                padding: 8px;
                font-size: 12px;
            }

        </style>

    </head>

    <body>

        <div style="overflow-x:auto;">

            <h2 style="text-align: center">Deleted Branch List</h2>

            <table>

                <tr>
                    <th style="width: 10px;">S.N.</th>

                    <th style="width: 120px;">Branch Name</th>

                    <th style="width: 120px;">Address</th>

                    <th style="width: 100px;">Phone Number</th>

                    <th style="width: 100px;">Email</th>

                </tr>

                <?php $i = 0 ?>

                @foreach($data['rows'] as $row)

                    <?php $i++ ?>

                    <tr>

                        <td>{{ $i }}</td>

                        <td>{{ $row->branch_name }}</td>

                        <td>{{ $row->branch_address }}</td>

                        <td>{{ $row->branch_phone }}</td>

                        <td>{{ $row->branch_email }}</td>

                    </tr>

                @endforeach

            </table>

        </div>

    </body>

</html>
