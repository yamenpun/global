<!DOCTYPE html>

<html moznomarginboxes mozdisallowselectionprint>

    <head>

        <title>Print Disabled College List</title>

        <style>

            @page { size: auto;  margin: 0mm; }

            table {
                border-collapse: collapse;
                width: 100%;
                padding-left:10px;
            }

            th, td {
                text-align: left;
                padding: 8px;
                font-size: 12px;
            }

            #printpagebutton{
                background-color: #008CBA; /* Green */
                border: none;
                color: white;
                padding: 5px 8px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 12px;
            }

        </style>

    </head>

    <body>
        <div style="width: 900px; margin: 0 auto;">

            <div style="overflow-x:auto;">

                <h2 style="text-align: center">Disabled College List &nbsp;<input id="printpagebutton" type="button" value="Print" onclick="printpage()"/></h2>

                <table>

                    <tr>

                        <th style="width: 10px;">S.N.</th>

                        <th style="width: 150px;">College Name</th>

                        <th style="width: 150px;">Address</th>

                        <th style="width: 80px;">Phone Number</th>

                        <th style="width: 50px;">Country</th>

                        <th style="width: 100px;">Remarks</th>

                    </tr>

                    <?php $i = 0 ?>

                    @foreach($data['rows'] as $row)

                        <?php $i++ ?>

                        <tr>

                            <td>{{ $i }}</td>

                            <td>{{ $row->college_name }}</td>

                            <td>{{ $row->college_address }}</td>

                            <td>{{ $row->college_phone }}</td>

                            <td>{{ $row->college_country }}</td>

                            <td>
                                @if($row->remarks)
                                    {{ $row->remarks }}
                                @else
                                    N/A
                                @endif
                            </td>

                        </tr>

                    @endforeach

                </table>

            </div>

        </div>

        <script type="text/javascript">

            function printpage() {

                //Get the print button and put it into a variable
                var printButton = document.getElementById("printpagebutton");

                //Set the print button visibility to 'hidden'
                printButton.style.visibility = 'hidden';

                //Print the page content
                window.print()

                //Set the print button to 'visible' again
                //[Delete this line if you want it to stay hidden after printing]
                printButton.style.visibility = 'visible';
            }

        </script>

    </body>

</html>
