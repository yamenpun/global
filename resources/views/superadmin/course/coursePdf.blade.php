<!DOCTYPE html>

<html>

    <head>

        <title>Convert To PDF</title>

        <style>

            table {
                border-collapse: collapse;
                width: 100%;
            }

            th, td {
                text-align: left;
                padding: 8px;
                font-size: 12px;
            }

        </style>

    </head>

    <body>

        <div style="overflow-x:auto;">

            <h2 style="text-align: center">Course List</h2>

            <table>

                <tr>
                    <th style="width: 10px;">S.N.</th>

                    <th style="width: 120px;">Course Name</th>

                    <th style="width: 100px;">Course Remarks</th>

                </tr>

                <?php $i = 0 ?>

                @foreach($data['rows'] as $row)

                    <?php $i++ ?>

                    <tr>

                        <td>{{ $i }}</td>

                        <td>{{ $row->course_name }}</td>

                        <td>
                            @if($row->course_remarks)
                                {{ $row->course_remarks }}
                            @else
                                N/A
                            @endif
                        </td>

                    </tr>

                @endforeach

            </table>

        </div>

    </body>

</html>
