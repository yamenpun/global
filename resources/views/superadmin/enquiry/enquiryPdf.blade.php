<!DOCTYPE html>

<html>

    <head>

        <title>Convert To PDF</title>

        <style>

            table {
                border-collapse: collapse;
                width: 100%;
            }

            th, td {
                text-align: left;
                padding: 8px;
                font-size: 12px;
            }

        </style>

    </head>

    <body>

        <div style="overflow-x:auto;">

            <h2 style="text-align: center">Student Enquiry List</h2>

            <table>

                <tr>
                    <th style="width: 10px;">S.N.</th>

                    <th style="width: 20px;">Enquiry Date</th>

                    <th style="width: 80px;">Student Name</th>

                    <th style="width: 30px;">Mobile No.</th>

                    <th style="width: 80px;">Interested Course</th>

                    <th style="width: 80px;">Preferred Place</th>

                    <th style="width: 80px;">Branch</th>

                </tr>

                <?php $i = 0 ?>

                @foreach($data['rows'] as $row)

                    <?php $i++ ?>

                    <tr>

                        <td>{{ $i }}</td>

                        <td>{{ $row->enquiry_date }}</td>

                        <td>{{ $row->student_first_name }} {{ $row->student_middle_name }} {{ $row->student_last_name }}</td>

                        <td>{{ $row->mobile_phone }}</td>

                        <td>{{ $row->interested_course }}</td>

                        <td>{{ $row->preferred_place }}</td>

                        <td>{{ $row->branch_name }}</td>

                    </tr>

                @endforeach

            </table>

        </div>

    </body>

</html>
