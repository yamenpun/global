<div class="sidebar" id="sidebar">

    <script type="text/javascript">
        try {
            ace.settings.check('sidebar', 'fixed')
        } catch (e) {
        }
    </script>

    <ul class="nav nav-list">

        <li {!! Request::is('superadmin/dashboard')?'class="active"':'' !!}>

            <a href="{{ route('superadmin.dashboard') }}">
                <i class="icon-dashboard"></i>
                <span class="menu-text"> Dashboard </span>
            </a>

        </li>

        <li {!! Request::is('superadmin/enquiry/list*') || Request::is('superadmin/enquiry/view*') || Request::is('superadmin/enquiry/deletedView*') || Request::is('superadmin/enquiry/add*') || Request::is('superadmin/enquiry/edit*') || Request::is('superadmin/enquiry/deletedList*')?'class="active open"':'' !!}>

            <a class="dropdown-toggle" href="#">
                <i class="icon-plus-sign"></i>
                <span class="menu-text"> Student Enquiry </span>
                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu" style="display: no">

                <li {!! Request::is('superadmin/enquiry/list*') || Request::is('superadmin/enquiry/view*') || Request::is('superadmin/enquiry/add*') || Request::is('superadmin/enquiry/edit*')?'class="active"':'' !!}>

                    <a href="{{ route('superadmin.enquiry.list') }}">
                        <i class="icon-double-angle-right"></i>
                        Student Enquiry List
                    </a>

                </li>

                <li {!! Request::is('superadmin/enquiry/deletedList*') || Request::is('superadmin/enquiry/deletedView*')?'class="active"':'' !!}>

                    <a href="{{ route('superadmin.enquiry.deletedList') }}">
                        <i class="icon-double-angle-right"></i>
                        Deleted Enquiry List
                    </a>

                </li>

            </ul>

        </li>

        <li {!! Request::is('superadmin/register/list*') || Request::is('superadmin/register/view*') || Request::is('superadmin/register/deletedView*') || Request::is('superadmin/register/add*') || Request::is('superadmin/register/newPatient*') || Request::is('superadmin/register/edit*') || Request::is('superadmin/register/deletedList*')?'class="active open"':'' !!}>

            <a class="dropdown-toggle" href="#">
                <i class="icon-edit"></i>
                <span class="menu-text"> Registration </span>
                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu" style="display: no">

                <li {!! Request::is('superadmin/register/list*') || Request::is('superadmin/register/view*') || Request::is('superadmin/register/add*') || Request::is('superadmin/register/edit*')?'class="active"':'' !!}>

                    <a href="{{ route('superadmin.register.list') }}">
                        <i class="icon-double-angle-right"></i>
                        Registration List
                    </a>

                </li>

                <li {!! Request::is('superadmin/register/deletedList*') || Request::is('superadmin/register/deletedView*')?'class="active"':'' !!}>

                    <a href="{{ route('superadmin.register.deletedList') }}">
                        <i class="icon-double-angle-right"></i>
                        Deleted Registration List
                    </a>

                </li>

            </ul>

        </li>

        <li {!! Request::is('superadmin/agreement/list*') || Request::is('superadmin/agreement/view*') || Request::is('superadmin/agreement/deletedView*') || Request::is('superadmin/agreement/add*') || Request::is('superadmin/agreement/edit*') || Request::is('superadmin/agreement/deletedList*')?'class="active open"':'' !!}>

            <a class="dropdown-toggle" href="#">
                <i class="icon-key"></i>
                <span class="menu-text"> Agreement </span>
                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu" style="display: no">

                <li {!! Request::is('superadmin/agreement/list*') || Request::is('superadmin/agreement/view*') || Request::is('superadmin/agreement/add*') || Request::is('superadmin/agreement/edit*')?'class="active"':'' !!}>

                    <a href="{{ route('superadmin.agreement.list') }}">
                        <i class="icon-double-angle-right"></i>
                        Agreement List
                    </a>

                </li>

                <li {!! Request::is('superadmin/agreement/deletedList*') || Request::is('superadmin/agreement/deletedView*')?'class="active"':'' !!}>

                    <a href="{{ route('superadmin.agreement.deletedList') }}">
                        <i class="icon-double-angle-right"></i>
                        Deleted Agreement List
                    </a>

                </li>

            </ul>

        </li>

        <li {!! Request::is('superadmin/branch/list*') || Request::is('superadmin/branch/view*') || Request::is('superadmin/branch/deletedView*') || Request::is('superadmin/branch/add*') || Request::is('superadmin/branch/newPatient*') || Request::is('superadmin/branch/edit*') || Request::is('superadmin/branch/deletedList*')?'class="active open"':'' !!}>

            <a class="dropdown-toggle" href="#">
                <i class="icon-group"></i>
                <span class="menu-text"> Branch </span>
                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu" style="display: no">

                <li {!! Request::is('superadmin/branch/list*') || Request::is('superadmin/branch/view*') || Request::is('superadmin/branch/add*') || Request::is('superadmin/branch/edit*')?'class="active"':'' !!}>

                    <a href="{{ route('superadmin.branch.list') }}">
                        <i class="icon-double-angle-right"></i>
                        Branch List
                    </a>

                </li>

                <li {!! Request::is('superadmin/branch/deletedList*') || Request::is('superadmin/branch/deletedView*')?'class="active"':'' !!}>

                    <a href="{{ route('superadmin.branch.deletedList') }}">
                        <i class="icon-double-angle-right"></i>
                        Disabled Branch List
                    </a>

                </li>

            </ul>

        </li>

        <li {!! Request::is('superadmin/college/list*') || Request::is('superadmin/college/deletedView*') || Request::is('superadmin/college/add*') || Request::is('superadmin/college/edit*') || Request::is('superadmin/college/deletedList*')?'class="active open"':'' !!}>

            <a class="dropdown-toggle" href="#">
                <i class="icon-home"></i>
                <span class="menu-text"> College </span>
                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu" style="display: no">

                <li {!! Request::is('superadmin/college/list*') || Request::is('superadmin/college/add*') || Request::is('superadmin/college/edit*')?'class="active"':'' !!}>

                    <a href="{{ route('superadmin.college.list') }}">
                        <i class="icon-double-angle-right"></i>
                        College List
                    </a>

                </li>

                <li {!! Request::is('superadmin/college/deletedList*') ?'class="active"':'' !!}>

                    <a href="{{ route('superadmin.college.deletedList') }}">
                        <i class="icon-double-angle-right"></i>
                        Disabled College List
                    </a>

                </li>

            </ul>

        </li>

        <li {!! Request::is('superadmin/course/list*') || Request::is('superadmin/course/deletedView*') || Request::is('superadmin/course/add*') || Request::is('superadmin/course/edit*') || Request::is('superadmin/course/deletedList*')?'class="active open"':'' !!}>

            <a class="dropdown-toggle" href="#">
                <i class="icon-certificate"></i>
                <span class="menu-text"> Course </span>
                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu" style="display: no">

                <li {!! Request::is('superadmin/course/list*') || Request::is('superadmin/course/add*') || Request::is('superadmin/course/edit*')?'class="active"':'' !!}>

                    <a href="{{ route('superadmin.course.list') }}">
                        <i class="icon-double-angle-right"></i>
                        Course List
                    </a>

                </li>

                <li {!! Request::is('superadmin/course/deletedList*') ?'class="active"':'' !!}>

                    <a href="{{ route('superadmin.course.deletedList') }}">
                        <i class="icon-double-angle-right"></i>
                        Disabled Course List
                    </a>

                </li>

            </ul>

        </li>

        <li {!! Request::is('superadmin/log/list*') || Request::is('superadmin/log/deleteForm*')?'class="active open"':'' !!}>

            <a class="dropdown-toggle" href="#">
                <i class="icon-exclamation-sign"></i>
                <span class="menu-text"> Activity Log </span>
                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu" style="display: no">

                <li {!! Request::is('superadmin/log/list*') ?'class="active"':'' !!}>

                    <a href="{{ route('superadmin.log.list') }}">
                        <i class="icon-double-angle-right"></i>
                        Activity Log List
                    </a>

                </li>

                <li {!! Request::is('superadmin/log/deleteForm*') ?'class="active"':'' !!}>

                    <a href="{{ route('superadmin.log.deleteForm') }}">
                        <i class="icon-double-angle-right"></i>
                        Delete Form
                    </a>

                </li>

            </ul>

        </li>

        <li {!! Request::is('superadmin/user*')?'class="active"':'' !!}>

            <a href="{{ route('superadmin.user') }}">
                <i class="icon-user"></i>
                <span class="menu-text"> User </span>
            </a>

        </li>

        <li>

            <a href="{{ url('logout') }}">
                <i class="icon-signout"></i>
                <span class="menu-text"> LogOut </span>
            </a>

        </li>

    </ul>

    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>

    <script type="text/javascript">

        try {
            ace.settings.check('sidebar', 'collapsed')
        } catch (e) {
        }

    </script>

</div>

