@extends('superadmin.layouts.master')

@section('title')
    View Registered Student
@endsection

@section('content')

    <div class="main-content">

        <div class="breadcrumbs" id="breadcrumbs">

            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">

                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">Home</a>
                </li>

                <li>
                    <a href="{{ route($base_route.'.list') }}">Registered Student List</a>
                </li>

                <li class="active">View Registered Student</li>

            </ul>

        </div>

        <div class="page-content">

            <div class="page-header">

                <h1>

                    <small>
                        <i class="icon-double-angle-right"></i>
                        Registered Student Detail Info
                    </small>

                    <div class="btn-group pull-right">

                        <a href="{{ URL::previous()}}" class="btn btn-pink btn-sm">
                            <i class="icon-backward bigger-110"></i>
                            Go Back
                        </a>

                    </div>

                </h1>

            </div>

            <div class="row">

                <div class="col-xs-12">

                    <div class="row">

                        <div class="col-xs-8">

                            <div class="table-responsive">

                                @if (session()->has('message'))
                                    {!! session()->get('message') !!}
                                @endif

                                <table class="table table-striped table-bordered table-hover" id="" aria-describedby="sample-table-2_info">

                                    <tbody role="alert" aria-live="polite" aria-relevant="all">

                                        <tr class="even">

                                            <td>Registered Date</td>

                                            <td>{{ $data['row']['0']->register_date }}</td>

                                        </tr>

                                        <tr class="odd">

                                            <td>Student Full Name</td>
                                            <td>{{ $data['row']['0']->student_first_name }} {{ $data['row']['0']->student_middle_name }} {{ $data['row']['0']->student_last_name }} </td>

                                        </tr>

                                        <tr class="even">

                                            <td>College Name</td>
                                            <td>{{ $data['row']['0']->college_name }} </td>

                                        </tr>

                                        <tr class="odd">

                                            <td>Interested Course</td>
                                            <td>{{ $data['row']['0']->course_name }}</td>

                                        </tr>

                                        <tr class="even">

                                            <td>Intake</td>
                                            <td>{{ $data['row']['0']->intake }}</td>

                                        </tr>

                                        <tr class="odd">

                                            <td>Date Of Birth</td>
                                            <td>{{ $data['row']['0']->date_of_birth }}</td>

                                        </tr>

                                        <tr class="even">

                                            <td>Father's Name</td>
                                            <td>{{ $data['row']['0']->father_name }}</td>

                                        </tr>

                                        <tr class="odd">

                                            <td>Father's Occupation</td>
                                            <td>{{ $data['row']['0']->father_occupation }}</td>

                                        </tr>

                                        <tr class="even">

                                            <td>Mother's Name</td>
                                            <td>{{ $data['row']['0']->mother_name }}</td>

                                        </tr>

                                        <tr class="odd">

                                            <td>Mother's Occupation</td>
                                            <td>{{ $data['row']['0']->mother_occupation }}</td>

                                        </tr>

                                        <tr class="even">

                                            <td>Permanent Address</td>
                                            <td>{{ $data['row']['0']->permanent_address }}</td>

                                        </tr>

                                        <tr class="odd">

                                            <td>Temporary Address</td>
                                            <td>{{ $data['row']['0']->temporary_address }}</td>

                                        </tr>

                                        <tr class="even">

                                            <td>Emergency Contact No.</td>
                                            <td>{{ $data['row']['0']->emergency_contact_number }}</td>

                                        </tr>

                                        <tr class="odd">

                                            <td>Language Score</td>

                                            <td>IELTS: {{ $data['row']['0']->ielts_score }} TOEFL: {{ $data['row']['0']->toefl_score }} SAT: {{ $data['row']['0']->sat_score }}
                                                GRE: {{ $data['row']['0']->gre_score }} GMAT: {{ $data['row']['0']->gmat_score }}</td>

                                        </tr>

                                        <tr class="even">

                                            <td>Signature Date</td>
                                            <td>{{ $data['row']['0']->signature_date }}</td>

                                        </tr>

                                        <tr class="odd">
                                            <td>Is Signature Student?</td>
                                            <td>
                                                @if($data['row']['0']->is_signature_student == '0')
                                                    NO
                                                @else
                                                    Yes
                                                @endif
                                            </td>
                                        </tr>

                                        <tr class="even">
                                            <td>Is Signature Parent?</td>
                                            <td>
                                                @if($data['row']['0']->is_signature_parent == '0')
                                                    NO
                                                @else
                                                    Yes
                                                @endif
                                            </td>
                                        </tr>

                                        <tr class="odd">

                                            <td>Branch Name</td>

                                            <td>{{ $data['row']['0']->branch_name }} </td>

                                        </tr>

                                        <tr class="even">

                                            <td>Registered By</td>

                                            <td>{{ $data['row']['0']->fullname }} </td>

                                        </tr>

                                    </tbody>

                                </table>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

@endsection
